from django.contrib import admin

from .models import Group, Structure


@admin.register(Structure)
class StructureModelAdmin(admin.ModelAdmin):
    list_display = ('title_id', 'parent_id', 'group_id')
    search_fields = ['title_id', 'parent_id', 'group_id']


@admin.register(Group)
class GroupModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
