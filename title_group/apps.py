from django.apps import AppConfig


class TitleGroupConfig(AppConfig):
    name = 'title_group'
