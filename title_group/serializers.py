from rest_framework import exceptions, serializers

from .models import Group, Structure


class StructureListSerializer(serializers.ModelSerializer):
    count = serializers.SerializerMethodField()

    class Meta:
        model = Structure
        fields = (
            'id',
            'name',
            'title_id',
            'group_id',
            'count',
        )

    def get_count(self, structure):
        return structure.get_child().count()
