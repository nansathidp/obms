from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views_group import GroupView
from .views_structure import StructureView

router = DefaultRouter()

router.register(r'structure', StructureView)
router.register(r'group', GroupView)
app_name = 'country'
urlpatterns = [
    url(r'^', include(router.urls)),
]
