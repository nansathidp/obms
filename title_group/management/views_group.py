from rest_framework import viewsets

from ..models import Group
from .serializers import GroupSerializer


class GroupView(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    pagination_class = None
