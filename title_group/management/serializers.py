from rest_framework import exceptions, serializers

from ..models import Group, Structure


class StructureListSerializer(serializers.ModelSerializer):
    count = serializers.SerializerMethodField()

    class Meta:
        model = Structure
        fields = (
            'id',
            'name',
            'title_id',
            'group_id',
            'count',
        )

    def get_count(self, structure):
        return structure.get_child().count()


class StructureSerializer(serializers.ModelSerializer):
    account_created_id = serializers.CharField(required=False, allow_blank=True, allow_null=True, default=None)

    class Meta:
        model = Structure
        fields = (
            'id',
            'title',
            'parent',
            'group',
            'account_created_id',
        )
        read_only_fields = ['id', ]

    def create(self, validated_data):
        request = self.context['request']
        validated_data.update({'account_created_id': request.user.id})
        return super().create(validated_data)

    def update(self, instance, validated_data):
        parent = validated_data.get('parent', None)
        if parent == instance:
            raise exceptions.ValidationError('parent is self conflict')
        if parent:
            if instance in parent.get_parent_all():
                raise exceptions.ValidationError('parent is conflict')
        if parent in instance.get_parent_all():
            raise exceptions.ValidationError('parent is conflict')
        return super().update(instance, validated_data)

    def validate(self, attrs):
        title = attrs.get('title', None)
        parent = attrs.get('parent', None)
        group = attrs.get('group', None)
        if not (title or group):
            raise exceptions.ValidationError('title and group is none')
        if parent:
            if parent in parent.get_parent_all():
                raise exceptions.ValidationError('parent is conflict')
        return attrs


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = (
            'id',
            'name',
            'account_created_id',
        )
        read_only_fields = ['id', ]

    def create(self, validated_data):
        request = self.context['request']
        validated_data.update({'account_created_id': request.user.id})
        return super().create(validated_data)

    def validate_name(self, name):
        if Group.objects.filter(name=name).exists():
            raise exceptions.ValidationError('%s duplicate' % name)
        return name
