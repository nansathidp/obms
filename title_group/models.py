from django.conf import settings
from django.db import models

from utils.fields import ObmModel, SaveMixin


class Group(ObmModel):
    id = models.CharField(primary_key=True, max_length=15, db_column="TITLE_GROUP_ID")
    name = models.CharField(max_length=50, db_column="TITLE_GROUP_NAME", unique=True)
    primary_key = 'id'
    prefix = "GT"

    class Meta:
        ordering = ('name', )
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE_GROUP')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)


class Structure(SaveMixin, models.Model):
    id = models.CharField(primary_key=True, max_length=15, db_column="STRUCTURE_ID")
    parent = models.ForeignKey(
        'self',
        related_name="parent_set",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        default=None,
        db_column="PARENT_ID"
    )
    title = models.ForeignKey(
        'title.Title',
        related_name="structure_title_set",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        default=None,
        db_column="TITLE_ID"
    )
    group = models.ForeignKey(
        Group,
        related_name="title_group_set",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        default=None,
        db_column="TITLE_GROUP_ID"
    )
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")

    primary_key = "id"
    prefix = "GS"

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE_GROUP_STRUCTURE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    @property
    def name(self):
        if self.title:
            return getattr(self.title, 'name', '')
        return getattr(self.group, 'name', '')

    def get_child(self):
        return Structure.objects.filter(parent=self)

    def get_child_all(self):
        child_list = []
        for child in Structure.objects.filter(parent=self):
            child_list.append(child)
            child_list += child.get_child_all()
        return child_list

    def get_parent_all(self):
        if self.parent:
            parent_list = [self.parent]
            for structure in Structure.objects.filter(id=self.parent.id):
                parent_list += structure.get_parent_all()
            return parent_list
        else:
            return []
