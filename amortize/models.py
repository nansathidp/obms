from django.conf import settings
from django.db import models

from utils.fields import ObmModel


class Category(ObmModel):
    id = models.CharField(max_length=15, primary_key=True, db_column="AMORTIZE_CATEGORY_ID")
    name = models.CharField(max_length=50, db_column="CATEGORY_NAME", )

    primary_key = 'id'
    prefix = "AMC"

    class Meta:
        ordering = ['name']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_AMORTIZE_CATEGORY')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)
