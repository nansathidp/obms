from django_filters import filters
from django_filters.rest_framework import FilterSet

from ..models import Category


class CategoryFilter(FilterSet):
    category_name = filters.CharFilter(
        method='get_category_name',
        field_name='category_name'
    )

    class Meta:
        model = Category
        fields = (
            'id',
            'name'            
        )

    def get_category_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
