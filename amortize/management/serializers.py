from rest_framework import serializers

from ..models import Category


class AmortizeCategoryListSerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)    
    datetime_updated = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    update_by = serializers.CharField(source="account_updated.email", required=False, read_only=True)

    class Meta:
        model = Category
        fields = (
            'id',
            'name',
            'datetime_created',
            'account_created',
            'create_by',
            'datetime_updated',
            'account_updated',
            'update_by'           
        )


class AmortizeCategorySerializer(serializers.ModelSerializer):
    account_created = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)
    account_updated = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)

    class Meta:
        model = Category
        fields = (
            'name',
            'account_created', 
            'account_updated'           
        )

    def validate_account_created(self, account):
        return self.context['request'].user

    def validate_account_updated(self, account):
        return self.context['request'].user
