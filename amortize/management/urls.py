from django.urls import include, path
from rest_framework import routers

from .views import AmortizeView

router = routers.DefaultRouter()
router.register(r'', AmortizeView)

app_name = 'amortize'
urlpatterns = [    
    path('', include(router.urls)),
]
