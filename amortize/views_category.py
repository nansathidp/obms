from rest_framework import mixins, viewsets

from .models import Category
from .serializers import AmortizeCategorySerializer


class CategoryView(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = Category.objects.all()
    serializer_class = AmortizeCategorySerializer
    pagination_class = None
