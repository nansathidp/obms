from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views_category import CategoryView

router = DefaultRouter()
router.register(r'category', CategoryView)

urlpatterns = [
    url(r'^', include(router.urls)),
]