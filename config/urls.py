from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import SystemConfigView

router = DefaultRouter()

router.register(r'', SystemConfigView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
