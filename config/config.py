CONFIG = [

    {
        'group_name': 'VOD_TYPE',
        'value': {
            'type': 'TVOD',
            'is_enable': True,
            'is_mg': True,
            'is_deem_price': True,
            'is_rev_share': True
        },
    },
    {
        'group_name': 'VOD_TYPE',
        'value': {
            'type': 'AVOD',
            'is_enable': True,
            'is_mg': False,
            'is_deem_price': False,
            'is_rev_share': True
        }
    },

]
