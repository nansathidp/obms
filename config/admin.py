from django.contrib import admin

from .models import System


@admin.register(System)
class SystemModelAdmin(admin.ModelAdmin):
    list_display = ('group_name', 'value')
    actions = ['vod_type_enable', 'vod_type_disable', ]

    def vod_type_enable(self, request, queryset):
        for system in queryset:
            system.vod_type_enable()

    def vod_type_disable(self, request, queryset):
        for system in queryset:
            system.vod_type_disable()

# Register your models here.
