from django_filters.rest_framework.backends import DjangoFilterBackend
from rest_framework import status, viewsets
from rest_framework.response import Response

from .models import System
from .serializers import SystemSerializer


class SystemConfigView(viewsets.ReadOnlyModelViewSet):
    queryset = System.objects.all()
    filter_backends = (
        DjangoFilterBackend,
    )
    serializer_class = SystemSerializer

    lookup_url_kwarg = 'group_name'

    def retrieve(self, request, *args, **kwargs):
        queryset = self.get_queryset().filter(**kwargs)
        serializers = self.get_serializer(queryset, many=True)
        return Response(serializers.data, status=status.HTTP_200_OK)

