from rest_framework import serializers

from .models import System


class SystemSerializer(serializers.ModelSerializer):
    value = serializers.SerializerMethodField()

    class Meta:
        model = System
        fields = (
            'group_name',
            'value',
        )

    def get_value(self, config):
        if config.group_name == 'VOD_TYPE':
            return config.get_vod_type_display()
        return config.value
