import ast
import json

from django.conf import settings
from django.db import models


class System(models.Model):
    value = models.CharField(max_length=50, db_column="SYSTEM_VALUE")
    group_name = models.CharField(max_length=20, db_column="GROUP_NAME", primary_key=settings.IS_ORA)
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ['-datetime_created']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_SYSTEM_VALUE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    @staticmethod
    def is_exist(value, group_name):
        return System.objects.filter(value=value, group_name=group_name).exists()

    @staticmethod
    def gen_vod_type():
        from .config import CONFIG
        config_list = System.objects.filter(group_name='VOD_TYPE').values_list('value', flat=True)
        config_list = list(config_list)
        if len(config_list) > 0:
            System.objects.filter(group_name='VOD_TYPE').delete()

        for vod_type in CONFIG:
            value = vod_type['value']
            for config in config_list:
                config = ast.literal_eval(config)
                if config['type'] == value['type']:
                    value = config
            value = json.dumps(value)
            vod_type['value'] = str(value)
            System.objects.create(**vod_type)

    @staticmethod
    def pull_vod_type():
        return System.objects.filter(group_name='VOD_TYPE')

    @staticmethod
    def is_vod_type(vod_type):
        config_list = System.objects.filter(group_name='VOD_TYPE').values_list('value', flat=True)
        config_list = list(config_list)
        for config in config_list:
            config = json.loads(config)
            if config['type'] == vod_type:
                return True
        return vod_type in ['LINEAR']

    def vod_type_enable(self):
        if self.group_name != 'VOD_TYPE':
            return None
        vod_type = json.loads(self.value)
        vod_type['is_enable'] = True
        self.value = str(vod_type)
        self.save(update_fields=['value'])

    def vod_type_disable(self):
        if self.group_name != 'VOD_TYPE':
            return None
        vod_type = ast.literal_eval(self.value)
        vod_type['is_enable'] = False
        self.value = str(vod_type)
        self.save(update_fields=['value'])

    def get_vod_type_display(self):
        try:
            return json.loads(self.value)
        except Exception as err:
            print(err)
            return {}
