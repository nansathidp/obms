from django.contrib import admin

from .models import Prefix


# Register your models.py here.

@admin.register(Prefix)
class PrefixModelAdmin(admin.ModelAdmin):
    list_display = ('prefix', 'code')
