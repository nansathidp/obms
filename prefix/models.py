from datetime import datetime

from django.conf import settings
from django.db import models


class Prefix(models.Model):
    code = models.CharField(max_length=15, db_column="PROG_CODE", primary_key=settings.IS_ORA)
    prefix = models.CharField(max_length=10)
    year = models.IntegerField()
    month = models.IntegerField()
    running = models.IntegerField(default=0)
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="+", db_column="CREATED_BY",
                                        on_delete=models.CASCADE)

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_RUNNING_NO')
        default_permissions = (
            'view',
            'add',
            'change',
            'delete',
        )

    def __str__(self):
        return "{}_{}".format(self.prefix, self.code)

    @staticmethod
    def get_generate_id(content_type, prefix, account):
        code = '{}.{}'.format(content_type.app_label, content_type.model)
        year = datetime.now().year
        month = datetime.now().month
        prefix_list = Prefix.objects.filter(code=code)
        if prefix_list.exists():
            if prefix_list.filter(year=year, month=month).exists():
                self = prefix_list.filter(year=year, month=month).first()
            else:
                instance = prefix_list.first()
                self = instance.clone(account)
        else:
            self = Prefix.objects.create(
                code=code,
                prefix=prefix,
                month=month,
                year=year,
                account_created=account
            )

        return self.next()

    def clone(self, account):
        year = datetime.now().year
        month = datetime.now().month

        return Prefix.objects.create(
            code=self.code,
            prefix=self.prefix,
            year=year,
            month=month,
            account_created=account
        )
    def next(self):
        self.running = self.running + 1
        postfix = str(self.running)
        size_of_str = len(postfix)
        datetime_str_prefix = datetime.now().strftime("%Y%m")
        prefix = '{}{}'.format(self.prefix, datetime_str_prefix)
        suffix = "".join(["0" for index in range(settings.SIZE_PREFIX - size_of_str)])
        self.save(update_fields=['running'])
        return '{}{}{}'.format(prefix, suffix, postfix)
