from django_filters.rest_framework import FilterSet
from django_filters import filters

from ..models import Role


class RoleFilter(FilterSet):
    role_name = filters.CharFilter(
        method='get_role_name',
        field_name='role_name'
    )

    class Meta:
        model = Role
        fields = (
            'id',
            'name',
        )

    def get_role_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
