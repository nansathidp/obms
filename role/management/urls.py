from django.urls import include, path
from rest_framework import routers

from utils.rest_framework.routers import ListDetailRouter
from .views import RoleView
from .views_aka import AKAView

router = routers.DefaultRouter()
router.register(r'', RoleView)

router_aka = ListDetailRouter()
router_aka.register(r'', AKAView)

app_name = 'role'
urlpatterns = [
    path('<str:role_id>/aka/', include(router_aka.urls)),
    path('', include(router.urls)),
]
