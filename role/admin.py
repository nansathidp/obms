from django.contrib import admin
from .models import Role, AKA


@admin.register(Role)
class RoleModelAdmin(admin.ModelAdmin):
    list_display = ('id', )

@admin.register(AKA)
class AKAModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'role', 'language')
