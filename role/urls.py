from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import RoleView

router = DefaultRouter()

# router.register(r'aka', AKAView)
router.register(r'', RoleView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
