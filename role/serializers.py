from rest_framework import serializers

from .models import AKA, Role


class RoleListSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Role
        fields = (
            'id',
            'name'            
        )

class RoleMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = (
            'id',
            'name'            
        )

class AKASerializer(serializers.ModelSerializer):

    class Meta:
        model = AKA
        fields = (
            'role',
            'name',
            'language'            
        )

