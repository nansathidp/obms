#!/usr/bin/env bash
branch_name="$(git symbolic-ref HEAD 2>/dev/null)" ||
branch_name="(unnamed branch)"     # detached HEAD
branch_name=${branch_name##refs/heads/}
tag=$(echo $branch_name | tr "/" "-")
docker build --rm -t  subaruqui/bms:$tag .
is_build=$?
if [ $is_build -eq 0 ]; then
  docker push subaruqui/bms:$tag
  echo docker push success
else
  echo failed
fi
