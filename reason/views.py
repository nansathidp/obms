from django.shortcuts import render
from rest_framework import viewsets
from .models import Censor
from .serializers import ReasonSerializer


class ReasonView(viewsets.ReadOnlyModelViewSet):
    queryset = Censor.objects.all()
    serializer_class = ReasonSerializer
    pagination_class = None
