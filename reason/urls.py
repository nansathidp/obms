from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import ReasonView

router = DefaultRouter()

router.register(r'', ReasonView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
