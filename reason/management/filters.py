from django_filters import filters
from django_filters.rest_framework import FilterSet

from ..models import Censor


class CensorFilter(FilterSet):
    censor_description = filters.CharFilter(
        method='get_censor_description',
        field_name='censor_description'
    )

    class Meta:
        model = Censor
        fields = (
            'id',
            'description'            
        )

    def get_censor_description(self, queryset, description, value):
        return queryset.filter(vendor__description=value)
