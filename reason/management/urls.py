from django.urls import include, path
from rest_framework import routers

from .views import ReasonView

router = routers.DefaultRouter()
router.register(r'', ReasonView)

app_name = 'reason'
urlpatterns = [    
    path('', include(router.urls)),
]
