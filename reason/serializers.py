from rest_framework import serializers

from .models import Censor


class ReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Censor
        fields = (
            'id',
            'description',            
        )
