amqp==2.5.2
anyjson==0.3.3
asgiref==3.2.3
beautifulsoup4==4.8.2
billiard==3.6.3.0
cachetools==4.0.0
celery==4.4.2
certifi==2019.11.28
cffi==1.13.2
chardet==3.0.4
coreapi==2.3.3
coreschema==0.0.4
cx-Oracle==7.3.0
Django==3.0
django-celery==3.3.1
django-cors-headers==3.2.1
django-db-prefix==1.0.5
django-extra-fields==2.0.5
django-filter==2.2.0
django-memcache-admin==0.2.1
django-reset-migrations==0.4.0
django-rest-swagger==2.2.0
djangorestframework==3.11.0
djangorestframework-bulk==0.2.1
djangorestframework-simplejwt==4.4.0
drf-yasg==1.17.0
entrypoints==0.3
google==2.0.3
google-api-python-client==1.7.11
google-auth==1.10.0
google-auth-httplib2==0.0.3
google-auth-oauthlib==0.4.1
httplib2==0.15.0
idna==2.8
importlib-metadata==1.6.0
inflection==0.3.1
itypes==1.1.0
Jinja2==2.10.3
kombu==4.6.8
Markdown==3.1.1
MarkupSafe==1.1.1
mysqlclient==1.4.6
Naked==0.1.31
oauthlib==3.1.0
openapi-codec==1.3.2
packaging==20.0
Pillow==7.1.2
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycparser==2.19
pycryptodome==3.9.8
pydot==1.4.1
PyJWT==1.7.1
pylibmc==1.6.1
pyparsing==2.4.6
PyPDF2==1.26.0
python-memcached==1.59
pytz==2019.3
PyYAML==5.3
requests==2.22.0
requests-oauthlib==1.3.0
rsa==4.0
ruamel.yaml==0.16.5
ruamel.yaml.clib==0.2.0
shellescape==3.8.1
simplejson==3.17.0
six==1.13.0
soupsieve==1.9.5
sqlparse==0.3.0
uritemplate==3.0.1
urllib3==1.25.7
uWSGI==2.0.18
vine==1.3.0
websocket-client==0.57.0
zipp==3.1.0
