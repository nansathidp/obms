from .models import Studio, AKA

STUDIO_ITEM = {
    'id': 'SS20200200001',
}

AKA_ITEM_EN = {
    'studio_id': 'SS20200200001',
    'name': 'MARVEL STUDIO',
    'language_id': 'eng',
}
AKA_ITEM_TH = {
    'studio_id': 'SS20200200001',
    'name': 'มาร์เวล สตูดิโอ',
    'language_id': 'tha',
}


def init_studio(studio_id=None):
    if studio_id:
        STUDIO_ITEM.update({'id': studio_id})
        AKA_ITEM_TH.update({'studio_id': studio_id})
        AKA_ITEM_EN.update({'studio_id': studio_id})
    Studio.objects.get_or_create(**STUDIO_ITEM)
    AKA.objects.get_or_create(**AKA_ITEM_EN)
    AKA.objects.get_or_create(**AKA_ITEM_TH)
