from django_filters.rest_framework import FilterSet
from django_filters import filters

from ..models import Studio


class StudioFilter(FilterSet):
    studio_name = filters.CharFilter(
        method='get_studio_name',
        field_name='studio_name'
    )

    class Meta:
        model = Studio
        fields = (
            'id',
            'name'            
        )

    def get_studio_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
