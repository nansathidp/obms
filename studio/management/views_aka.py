
from rest_framework import mixins, viewsets
from rest_framework.exceptions import NotFound

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .serializers_aka import StudioListAKASerializer
from ..models import AKA, Studio


class AKAView(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    queryset = AKA.objects.all()
    pagination_class = None
    serializer_class = StudioListAKASerializer
    app = "studio"
    model = "aka"

    action_serializers = {
        'list': StudioListAKASerializer,        

    }
    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.studio = None

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)
        self.studio = Studio.pull(kwargs.get('studio_id', -1))
        if self.studio is None:
            raise NotFound

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def get_queryset(self):
        return super().get_queryset().filter(studio=self.studio)
