from rest_framework import exceptions, serializers

from language.models import Language
from studio.models import AKA, Studio
from utils.rest_framework.serializer import ContentSerializer


class StudioSerializer(serializers.ModelSerializer):    
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)

    class Meta:
        model = Studio
        fields = (
            'id',
            'name',
            'datetime_created',
            'account_created',
            'create_by'
        )
        read_only_fields = ['id', ]

class StudioAKASerializer(serializers.Serializer):
    name = serializers.CharField(max_length=50, default=None, allow_null=True, allow_blank=True)
    language = serializers.CharField(max_length=3)    

    def validate_language(self, code):
        if Language.objects.filter(code=code, is_aka='Y').exists():
            return code
        raise exceptions.ValidationError()

class StudioCreateSerializer(ContentSerializer, serializers.Serializer):
    name = serializers.CharField(required=True, max_length=50)
    account_created = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)
    aka_list = serializers.ListField(child=StudioAKASerializer())

    fields_map = {
        'name': 'name',        
        'account_created': 'account_created'      
    }

    class Meta:
        model = Studio        

    def validate_account_created(self, account):
        return self.context['request'].user

    def validate_aka_list(self, item_list):
        aka_list = filter(lambda x: x['name'], item_list)
        return list(aka_list)

    def create_relation(self, instance, validated_data, **kwargs):
        self.create_aka(validated_data, instance)

    def create_aka(self,  validated_data, studio):
        for aka in validated_data.get('aka_list', []):
            data = AKA.objects.filter(studio=studio, language=aka['language'])
            if data.exists():
                data.update(name=aka['name'], language=aka['language'])
            else:
                AKA.objects.create(
                    studio=studio,
                    name=aka['name'],
                    language_id=aka['language'],
                    account_created=studio.account_created
                )
        return True

