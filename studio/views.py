from rest_framework import viewsets

from .models import Studio
from .serializers import StudioListSerializer


class StudioView(viewsets.ReadOnlyModelViewSet):
    queryset = Studio.objects.all()
    serializer_class = StudioListSerializer
    pagination_class = None
