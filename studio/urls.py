from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import StudioView

router = DefaultRouter()

# router.register(r'aka', AKAView)
router.register(r'', StudioView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
