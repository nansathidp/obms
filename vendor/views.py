from rest_framework.viewsets import ReadOnlyModelViewSet

from .models import Vendor
from .serializers import VendorListSerializer


class VendorView(ReadOnlyModelViewSet):
    queryset = Vendor.objects.all()
    serializer_class = VendorListSerializer
    pagination_class = None

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()
