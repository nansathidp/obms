from django.conf import settings
from django.db import models

from utils.fields import SaveMixin


class Vendor(SaveMixin, models.Model):
    id = models.CharField(max_length=15, primary_key=True, db_column='VENDOR_ID')
    name = models.CharField(max_length=50, db_column='VENDOR_NAME')
    ftp_user_acc = models.CharField(max_length=50, db_column='FTP_USER_ACC', null=True, blank=True)
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    primary_key = 'id'
    prefix = 'VE'

    class Meta:
        ordering = ['name']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_VENDOR')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '%s (%s)' % (self.id, self.name)

    @staticmethod
    def pull(pk):
        try:
            return Vendor.objects.get(pk=pk)
        except Vendor.DoesNotExist:
            return None


class Studio(models.Model):
    vendor = models.ForeignKey(
        Vendor,
        related_name="vendor_studio_set",
        primary_key=settings.IS_ORA,
        db_column="VENDOR_ID",
        on_delete=models.CASCADE,
    )
    studio = models.ForeignKey(
        'studio.Studio',
        related_name="studio_vendor_set",
        db_column="STUDIO_ID",
        on_delete=models.CASCADE,
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_VENDOR_STUDIO')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '%s_%s' % (self.studio.name, self.vendor_id)