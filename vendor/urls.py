from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from utils.rest_framework.routers import ListDetailRouter
from .view_studio import StudioView
from .views import VendorView

router = DefaultRouter()
router_studio = ListDetailRouter()
router.register(r'', VendorView)
router_studio.register(r'', StudioView)

urlpatterns = [
    path('<str:vendor_id>/studio/', include(router_studio.urls)),
    path('', include(router.urls)),
]
