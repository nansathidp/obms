from rest_framework import serializers

from vendor.models import Vendor


class VendorListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields = (
            'id',
            'name',
            'ftp_user_acc'
        )


class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields = (
            'id',
            'name',
            'ftp_user_acc'
        )
