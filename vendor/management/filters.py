from django_filters.rest_framework import FilterSet
from django_filters import filters

from ..models import Vendor


class VendorFilter(FilterSet):
    vendor_name = filters.CharFilter(
        method='get_vendor_name',
        field_name='vendor_name'
    )

    class Meta:
        model = Vendor
        fields = (
            'id',
            'name'            
        )

    def get_vendor_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
