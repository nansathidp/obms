from django.conf.urls import include, url
from django.urls import path
from rest_framework.routers import DefaultRouter
from utils.rest_framework.routers import ListDetailRouter

from .views import VendorView
from .views_studio import StudioView

router = DefaultRouter()

router.register(r'', VendorView)

router_studio = ListDetailRouter()
router_studio.register(r'', StudioView)

app_name = 'vendor'
urlpatterns = [
	path('<str:vendor_id>/studio/', include(router_studio.urls)),
    url(r'^', include(router.urls)),
]
