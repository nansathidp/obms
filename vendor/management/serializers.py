from rest_framework import exceptions, serializers

from studio.models import Studio as StudioMaster
from utils.rest_framework.serializer import ContentSerializer
from vendor.models import Studio, Vendor


class VendorListSerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)

    class Meta:
        model = Vendor
        fields = (
            'id',
            'name',
            'ftp_user_acc',
            'datetime_created',
            'account_created',
            'create_by'
        )
        read_only_fields = ['id']


class StudioInVendorSerializer(serializers.Serializer):
    studio = serializers.CharField(max_length=15)

    def validate_studio(self, studio):
        if StudioMaster.objects.filter(id=studio).exists():
            return studio
        print(studio)
        raise exceptions.ValidationError()


class VendorSerializer(ContentSerializer, serializers.Serializer):
    account_created = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)    
    name = serializers.CharField(required=True, max_length=50)    
    ftp_user_acc = serializers.CharField(required=False, max_length=50, allow_blank=True, allow_null=True)
    studio = serializers.ListField(child=StudioInVendorSerializer(), write_only=True)

    class Meta:
        model = Vendor

    fields_map = {
        'name': 'name',
        'ftp_user_acc': 'ftp_user_acc',
        'account_created': 'account_created',        
    }

    def validate_name(self, name):
        view = self.context['view']
        if view.action == 'create' and Vendor.objects.filter(name=name).exists():
            raise exceptions.ValidationError('Duplicate %s' % name)
        return name

    def validate_account_created(self, account):
        return self.context['request'].user

    def create_relation(self, instance, validated_data, **kwargs):
        self.create_studio(validated_data, instance)

    def create_studio(self, validated_data, vendor):
        studio_list = validated_data.get('studio', [])
        for row in studio_list:
            if Studio.objects.filter(vendor=vendor, studio=row['studio']):
                continue
            else:
                Studio.objects.create(
                    vendor=vendor,                    
                    studio_id=row['studio'],                    
                )
        Studio.objects.filter(vendor=vendor).exclude(studio__in=list(map(lambda item: item['studio'], studio_list))).delete()                  
        return True


class StudioVendorListSerializer(serializers.ModelSerializer):
    vendor_name = serializers.CharField(source="vendor.name", required=False, read_only=True)
    studio_name = serializers.CharField(source="studio.name", required=False, read_only=True)

    class Meta:
        model = Studio
        fields = (
            'vendor',
            'studio',
            'vendor_name',
            'studio_name'
        )        
