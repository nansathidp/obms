from rest_framework import viewsets
from rest_framework.exceptions import NotFound

from studio.models import Studio
from studio.serializers import StudioListSerializer
from .models import Vendor


class StudioView(viewsets.ReadOnlyModelViewSet):
    queryset = Studio.objects.all()
    serializer_class = StudioListSerializer
    pagination_class = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vender = None

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)
        self.vender = Vendor.pull(kwargs.get('vendor_id', -1))
        if self.vender is None:
            raise NotFound

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        studio_id_list = self.vender.vendor_studio_set.all().values_list('studio_id', flat=True)
        studio_id_list = list(studio_id_list)
        return queryset.filter(id__in=studio_id_list)


