from .models import Vendor

VENDOR_ITEM = {
    'id': 'SV20200200001',
    'name': 'Disney Thailand'
}


def init_vendor(vendor_id=None):
    if vendor_id:
        VENDOR_ITEM.update({'id': vendor_id})
    Vendor.objects.get_or_create(**VENDOR_ITEM)
