from rest_framework import serializers


class TitleRightFromSerializer(serializers.Serializer):
    deal_on = serializers.CharField(required=False, max_length=15, allow_blank=True, allow_null=True)
    start = serializers.DateTimeField(required=False, allow_null=True)
    end = serializers.DateTimeField(required=False, allow_null=True)
    license = serializers.CharField(required=False, max_length=100, allow_blank=True, allow_null=True)
