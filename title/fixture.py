from datetime import timedelta

from django.utils import timezone

EPISODE_ITEM = {
    'episode_name': 'ไอ่หนุ่มพลังช้างสาร',
    # 'episode_id': 'SE202020100001',
    'pac_no': 1,
    'duration': '013002',
}

COST_ITEM = {
    'cost_name': 'subtitle',
    'cost_id': 'SC202002000001',
    'episode_cost': 300,
    'unit_cost': 2,
    'total_cost': 600,
}

LOCALIZATION_ITEM_SUBTITLE = {
    'name': 'SUBTITLE',
    'language_list': [
        {
            'code': 'tha',
            'is_checked': True,
        },
        {
            'code': 'eng',
            'is_checked': False,
        }
    ]
}

LOCALIZATION_ITEM_DUBBING = {
    'name': 'DUBBING',
    'language_list': [
        {
            'code': 'tha',
            'is_checked': True,
        },
        {
            'code': 'kor',
            'is_checked': True,
        },
        {
            'code': 'eng',
            'is_checked': True,
        },
    ]
}

RIGHT_ITEM = {
    'deal_on': 'SD2020020100001',
    'start': timezone.now() + timedelta(days=10),
    'end': timezone.now() + timedelta(days=30),
    'license': 'yhsijjj000124',
}

THUMBNAIL_ITEM = {
    'tag': 'poster',
    'file_name': 'image.jpg',
    'text_source': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPhfDwAChwGA60e6kgAAAABJRU5ErkJggg==',
}


class Request:
    user = None

    def __init__(self):
        from account.models import Account
        self.user = Account.objects.first()


class View:
    action = 'create'


AKA_ITEM = {
    "language": 'tha',
    'name': 'ไอ่หนุ่มพลังช้าง',
    'ep_name': 'ไอ่หนุ่มพลังช้าง',
    'synopsis': 'ตำนานพลังช้างและความแข็งแรงของไอ่หนุ่มพลังช้าง',
}

PEOPLE_ITEM_TH = {
    'id': 'PP202002000015',
    'language': 'tha',
    'name': 'เฉินหลง',
}

PEOPLE_ITEM_EN = {
    'id': 'PP202002000015',
    'language': 'eng',
    'name': 'Jackie Chan',
}

ROLE_NAME_ITEM_EN = {
    'name': 'manager',
    'language': 'eng'
}

ROLE_NAME_ITEM_TH = {
    'name': 'ผู้กำกับ',
    'language': 'tha'
}

CAST_ITEM = {
    'id': 'PR202002000001',
    'name_list': [ROLE_NAME_ITEM_EN, ROLE_NAME_ITEM_TH],
    'people_list': [
        PEOPLE_ITEM_EN,
        PEOPLE_ITEM_TH,
    ]
}

COST_EXTERNAL_ITEM = {
    'cost_name': 'subtitle',
    'cost_id': 'SC202002000001',
    'episode_cost': 300,
    'unit_cost': 2,
    'total_cost': 600,
}

TITLE_ITEM = {
    'name': 'SuperOver',
    'vendor_id': 'VE202002000002',
    'studio_id': 'ST202002000006',
    "country_id": "TH",
    'status': 'Select',
    'title_type': 'SERIES',
    'total_episode': 1,
    'rating_id': 'RT2020022800003',
    'genre_id': 'SG2020022800004',
    'duration': '013020',
    'cost': 600000,
    'release_year': 2007,
    'localize_list': [LOCALIZATION_ITEM_SUBTITLE, LOCALIZATION_ITEM_DUBBING],
    'right_list': [RIGHT_ITEM],
    'thumbnail_list': [THUMBNAIL_ITEM],
    'aka_synopsis_list': [AKA_ITEM],
    'title_cost_list': [COST_ITEM],
    'episode_list': [EPISODE_ITEM],
    'cast_list': [CAST_ITEM]
}
