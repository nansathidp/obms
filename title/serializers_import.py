from rest_framework import serializers

from utils.rest_framework.serializer import CSVBase64File


class FileImportSerializer(serializers.Serializer):
    tmp_file = CSVBase64File()
