from django.core.cache import cache

prefix = 'title'
expire_time = 15

def get_cached(code, id, default=None):
    key = '%s_%s_%s' % (prefix, code, id)
    item = cache.get(key)
    if item:
        return item
    return default

def set_cached(code, id, data):
    key = '%s_%s_%s' % (prefix, code, id)
    cache.set(key, data, expire_time)

def delete_cached(code, id):
    key = '%s_%s_%s' % (prefix, code, id)
    cache.delete(key)