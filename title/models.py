from django.conf import settings
from django.db import models

from deal.models import Linear, Video
from episode.models import Episode
from people.models import People
from role.models import Role
from utils.fields import ObmModel

CURRENT = "CURRENT"
LIBRARY = "LIBRARY"

IS_ACTIVE = 'Y'
IN_ACTIVE = 'N'

TYPE_CHOICES = (
    (CURRENT, CURRENT),
    (LIBRARY, LIBRARY)
)


class Title(ObmModel):
    id = models.CharField(max_length=15, primary_key=True, db_column="TITLE_ID")
    name = models.CharField(max_length=50, db_column="TITLE_NAME")
    vendor = models.ForeignKey(
        'vendor.Vendor',
        related_name="vendor_set",
        null=True,
        blank=True,
        default=None,
        db_column="VENDOR_ID",
        on_delete=models.SET_NULL
    )
    studio = models.ForeignKey(
        'studio.Studio',
        related_name="studio_set",
        null=True,
        blank=True,
        default=None,
        db_column="STUDIO_ID",
        on_delete=models.SET_NULL
    )
    country = models.ForeignKey(
        'country.Country',
        related_name="country_set",
        null=True,
        blank=True,
        default=None,
        db_column="COUNTRY_CODE",
        on_delete=models.SET_NULL
    )
    bu = models.ForeignKey(
        'owner.BusinessOwner',
        null=True,
        blank=True,
        default=None,
        db_column="BU_ID",
        on_delete=models.SET_NULL
    )
    genre = models.ForeignKey(
        'genre.Genre',
        null=True,
        blank=True,
        default=None,
        db_column="GENRE_ID",
        on_delete=models.SET_NULL
    )
    rating = models.ForeignKey(
        'rating.Rating',
        null=True,
        blank=True,
        default=None,
        db_column="RATING_ID",
        on_delete=models.SET_NULL
    )
    other = models.ForeignKey(
        'other_type.Type',
        null=True,
        blank=True,
        default=None,
        db_column="VOD_OTHER_TYPE_ID",
        on_delete=models.SET_NULL,
    )
    amortize_category = models.ForeignKey(
        'amortize.Category',
        null=True, 
        default=None,
        db_column='AMORTIZE_CATEGORY_ID',
        on_delete=models.SET_NULL,
    )

    title_type = models.CharField(max_length=20, db_column="TITLE_TYPE")
    status = models.CharField(max_length=20, db_column="TITLE_STATUS")
    total_episode = models.IntegerField(default=1, db_column="TOTAL_OF_EPS", null=True)
    duration = models.CharField(max_length=6, db_column="DURATION", null=True)
    release_year = models.IntegerField(null=True)
    cost = models.IntegerField(null=True,blank=True, default=None, db_column="TITLE_COST")

    prefix = "ST"
    primary_key = "id"

    class Meta:
        ordering = ['-id']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete','import', 'export')

    def __str__(self):
        return '{}-{}'.format(self.id, self.name)

    def update_total_episode(self):
        from episode.models import Episode
        self.total_episode = Episode.objects.filter(title=self).count()
        self.save(update_fields=['total_episode'])

    def create_episode(self, name, pac_no):
        episode = Episode.objects.filter(name__contains=name).first()
        if episode:
            episode.pac_no = pac_no
            episode.title = self
            episode.duration = self.duration
            episode.save()
        else:
            episode = Episode.objects.create(
                name=name,
                pac_no=pac_no,
                title=self,
                account_created_id=self.account_created_id,
                duration=self.duration
            )
        return episode


class AKA(models.Model):
    title = models.ForeignKey(
        Title,
        primary_key=settings.IS_ORA,
        db_column="TITLE_ID",
        on_delete=models.CASCADE
    )

    language = models.ForeignKey(
        'language.Language',
        related_name="+",
        db_column="LANGUAGE_CODE",
        on_delete=models.CASCADE
    )
    name = models.CharField(max_length=100, db_column="TITLE_NAME")
    synopsis = models.CharField(max_length=2000, db_column="SYNOPSIS")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ('language', 'name', )
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE_AKA')
        default_permissions = ('view', 'add', 'change', 'delete', 'import', 'export')


class Cast(models.Model):
    title = models.ForeignKey(
        Title,
        primary_key=settings.IS_ORA,
        db_column="TITLE_ID",
        on_delete=models.CASCADE
    )
    role = models.ForeignKey(
        'role.Role',
        db_column="PEOPLE_ROLE_ID",
        on_delete=models.CASCADE
    )
    people = models.ForeignKey(
        'people.People',
        db_column="PEOPLE_ID",
        on_delete=models.CASCADE
    )
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE_CAST')
        default_permissions = ('view', 'add', 'change', 'delete',)


class Cost(models.Model):
    title = models.ForeignKey(
        Title,
        primary_key=settings.IS_ORA,
        related_name="title_cost_set",
        db_column="TITLE_ID",
        on_delete=models.CASCADE
    )
    cost = models.ForeignKey(
        'cost.Cost',
        db_column="COST_ID",
        on_delete=models.CASCADE
    )
    episode_cost = models.IntegerField(null=True, db_column="EPISODE_COST")
    unit_cost = models.IntegerField(null=True, db_column="UNIT_COST")
    total_cost = models.IntegerField(null=True, db_column="TOTAL_COST")
    datetime_created = models.DateTimeField(auto_now_add=True, null=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE_COST')
        default_permissions = ('view', 'add', 'change', 'delete',)


class Localization(models.Model):
    STATUS_CHOICES = (
        ('LOCALIZE', 'LOCALIZE'),
        ('RECEIVE', 'RECEIVE'),
    )
    title = models.ForeignKey(
        Title,
        primary_key=settings.IS_ORA,
        db_column="TITLE_ID",
        on_delete=models.CASCADE
    )

    language = models.ForeignKey(
        'language.Language',
        db_column="LANGUAGE_CODE",
        related_name="+",
        on_delete=models.CASCADE
    )
    type = models.CharField(max_length=100, unique=True, db_column="LOCALIZE_TYPE")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='RECEIVE', db_column='IL_STATUS')

    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )


    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE_LOCALIZE')
        default_permissions = ('view', 'add', 'change', 'delete',)


class Thumbnail(models.Model):
    title = models.ForeignKey(
        Title,
        db_column="TITLE_ID",
        primary_key=settings.IS_ORA,
        on_delete=models.CASCADE
    )
    tag = models.CharField(max_length=50, db_column="TAG")
    file = models.ImageField(max_length=100, upload_to="thumbnail/", db_column="FILENAME")
    datetime_created = models.DateTimeField(auto_now_add=True, null=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE_THUMNAILS')
        default_permissions = ('view', 'add', 'change', 'delete',)

    def get_name(self):
        return self.file.name.split('/').pop()


class SubGenre(models.Model):
    title = models.ForeignKey(
        Title,
        db_column="TITLE_ID",
        primary_key=settings.IS_ORA,
        on_delete=models.CASCADE
    )
    sub_genre = models.ForeignKey('sub_genre.SubGenre',related_name="sub_gene_set", on_delete=models.CASCADE, db_column="SUB_GENRE_ID")

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_TITLE_SUB_GENRE')
        default_permissions = ('view', 'add', 'change', 'delete',)
