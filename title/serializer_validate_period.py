from rest_framework import exceptions, serializers

from deal.models import Video


class ValidatePeriodSerializer(serializers.Serializer):
    datetime_start = serializers.DateTimeField()
    datetime_end = serializers.DateTimeField()

    def validate(self, attrs):
        datetime_start = attrs['datetime_start']
        datetime_end = attrs['datetime_end']
        if datetime_start < datetime_end:
            view = self.context['view']
            title = view.get_object()
            video_period_list = Video.objects.filter(title=title)
            is_vod_datetime_start = video_period_list.filter(datetime_right_start__lte=datetime_start).exists()
            is_vod_datetime_end = video_period_list.filter(datetime_right_end__gte=datetime_end).exists()
            if not is_vod_datetime_start:
                raise exceptions.ValidationError('{} title datetime_start is not period'.format(title.id))

            if not is_vod_datetime_end:
                raise exceptions.ValidationError('{} title datetime_end is not period'.format(title.id))
            return attrs
        raise exceptions.ValidationError(
            'Title time period start {} conflict {} is not found '.format(datetime_start, datetime_end)
        )
