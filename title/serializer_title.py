from rest_framework import exceptions, serializers

from amortize.models import Category
from config.models import System
from country.models import Country
from episode.models import Episode
from genre.models import Genre
from rating.models import Rating
from studio.models import Studio
from utils.rest_framework import serializer as utils
from vendor.models import Vendor
from .models import AKA, Cast, Cost, Localization, SubGenre, Thumbnail, Title
from .serializer_cost import TitleCostFromSerializer
from .serializer_episode import TitleEpisodeFromSerializer
from .serializer_localization import TitleLocalizationFromSerializer
from .serializer_right import TitleRightFromSerializer
from .serializer_thumbnail import TitleThumbFromSerializer
from .serializers_aka import TitleAKAFromSerializer
from .serializers_cast import TitleCastFromSerializer
from .serializers_sub_genre import SubGenreTitleFormSerializer


class TitleFromSerializer(utils.ContentSerializer, serializers.Serializer):
    sub_genre_list = serializers.ListField(
        child=SubGenreTitleFormSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )
    localize_list = serializers.ListField(
        child=TitleLocalizationFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )
    right_list = serializers.ListField(
        child=TitleRightFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )
    thumbnail_list = serializers.ListField(
        child=TitleThumbFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )
    aka_synopsis_list = serializers.ListField(
        child=TitleAKAFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )
    title_cost_list = serializers.ListField(
        child=TitleCostFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )
    episode_list = serializers.ListField(
        child=TitleEpisodeFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )
    cast_list = serializers.ListField(
        child=TitleCastFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )
    # require fields
    id = serializers.CharField(required=False)
    name = serializers.CharField(required=True, max_length=50)
    title_type = serializers.CharField(max_length=50, required=True)
    status = serializers.CharField(required=True)

    total_episode = serializers.IntegerField(required=False, default=1)
    vendor_id = serializers.CharField(required=False, max_length=15, allow_null=True, allow_blank=True, default=None)
    country_id = serializers.CharField(required=False, max_length=15, allow_null=True, allow_blank=True, default=None)
    studio_id = serializers.CharField(required=False, max_length=15, allow_null=True, allow_blank=True, default=None)
    rating_id = serializers.CharField(required=False, max_length=15, allow_null=True, allow_blank=True, default=None)
    genre_id = serializers.CharField(required=False, max_length=15, allow_null=True, allow_blank=True, default=None)
    bu_id = serializers.CharField(required=False, max_length=15, allow_null=True, allow_blank=True, default=None)
    release_year = serializers.IntegerField(required=False, min_value=1)
    cost = serializers.IntegerField(required=False, allow_null=True)
    amortize_category_id = serializers.CharField(max_length=15, allow_null=True, allow_blank=True, default=None)
    duration = serializers.CharField(required=False, max_length=6, allow_null=True, allow_blank=True, default=None)
    other_id = serializers.CharField(max_length=15, allow_blank=True, allow_null=True, default=None)

    fields_map = {
        'name': 'name',
        'other_id': 'other_id',
        'country_id': 'country_id',
        'bu_id': 'bu_id',
        'vendor_id': 'vendor_id',
        'studio_id': 'studio_id',
        'rating_id': 'rating_id',
        'genre_id': 'genre_id',
        'amortize_category_id': 'amortize_category_id',
        'status': 'status',
        'total_episode': 'total_episode',
        'title_type': 'title_type',
        'duration': 'duration',
        'release_year': 'release_year',
        'cost': 'cost',
        'account_created': 'account_created',
        'account_updated': 'account_updated',
    }

    class Meta:
        model = Title


    def create_relation(self, instance, validated_data, **kwargs):
        self.create_aka(instance, validated_data, **kwargs)
        self.create_cast(instance, validated_data, **kwargs)
        self.create_cost(instance, validated_data, **kwargs)
        self.create_localization(instance, validated_data, **kwargs)
        self.create_thumbnail(instance, validated_data, **kwargs)
        self.create_episode(instance, validated_data, **kwargs)
        self.create_sub_genre(instance, validated_data, **kwargs)
        instance.update_total_episode()


    def create_aka(self, title, validated_data, **kwargs):
        AKA.objects.filter(title=title).delete()
        for aka_item in validated_data.get('aka_synopsis_list', []):
            name = aka_item.get('name')
            language_id = aka_item.get('language')
            synopsis = aka_item.get('synopsis')
            AKA.objects.create(
                title=title,
                name=name,
                account_created=kwargs.get('account'),
                language_id=language_id,
                synopsis=synopsis,
            )

    def create_cast(self, title, validate_data, **kwargs):
        if validate_data.get('cast_list', None):
            Cast.objects.filter(title=title).delete()
            for cast_item in validate_data.get('cast_list', []):
                role_id = cast_item.get('id')
                people_list = cast_item.get('people_list', [])
                for people in people_list:
                    Cast.objects.get_or_create(
                        title=title,
                        role_id=role_id,
                        account_created=kwargs.get('account'),
                        people_id=people.get('id')
                    )

    def create_localization(self, title, validate_data, **kwargs):
        Localization.objects.filter(title=title).delete()
        for localization in validate_data.get('localize_list', []):
            name = localization.get('name')
            for language in localization.get('language_list'):
                if language.get('is_checked', False):
                    language_id = language.get('code')
                    Localization.objects.create(
                        title=title,
                        account_created=kwargs.get('account'),
                        language_id=language_id,
                        type=name,
                        status=language.get('status', 'RECEIVE')
                    )

    def create_cost(self, title, validate_data, **kwargs):
        tile_cost_list = validate_data.get('title_cost_list', None)
        if isinstance(tile_cost_list, list):
            title.title_cost_set.all().delete()
            for cost_info in tile_cost_list:
                if not Cost.objects.filter(title=title, cost_id=cost_info.get('cost_id')).exists():
                    cost_info.setdefault('account_created', kwargs.get('account'))
                    cost_info.update({'title': title})
                    Cost.objects.create(**cost_info)

    def create_sub_genre(self, title, validate_data, **kwargs):
        sub_genre_list = validate_data.get('sub_genre_list', None)
        sub_genre_list = map(lambda x: x['id'], sub_genre_list)
        sub_genre_list = list(sub_genre_list)
        if isinstance(sub_genre_list, list):
            for sub_genre_id in sub_genre_list:
                 SubGenre.objects.get_or_create(title=title, sub_genre_id=sub_genre_id )
        SubGenre.objects.filter(title=title).exclude(sub_genre_id__in=sub_genre_list).delete()

    def create_thumbnail(self, title, validate_data, **kwargs):
        if title.thumbnail_set.exists():
            title.thumbnail_set.all().delete()

        for item in validate_data.get('thumbnail_list', []):
            Thumbnail.objects.create(
                title=title,
                file=item['text_source'],
                tag=item['tag'],
                account_created=kwargs.get('account')
            )

    def create_episode(self, title, validate_data, **kwargs):
        episode_list = validate_data.get('episode_list', [])
        episode_id_list = list()
        for episode in episode_list:
            episode.update({'title': title})
            episode_id = episode.get('episode_id', None)
            episode_id_list.append(episode_id)
            if episode_id:
                Episode.objects.filter(id=episode_id).update(
                    title=title,
                    name=episode.get('episode_name'),
                    duration=episode.get('duration', '000000'),
                    pac_no=episode.get('pac_no', 1),
                    account_updated=kwargs.get('account')
                )
            else:
                episode_info = {
                    'title': title,
                    'name': episode.get('episode_name'),
                    'duration': episode.get('duration'),
                    'pac_no': episode.get('pac_no', 1),
                    'datetime_expect_receive': episode.get('datetime_expect_receive', None),
                    'account_created': kwargs.get('account'),
                }
                episode = Episode.objects.create(**episode_info)
                episode_id_list.append(episode.id)
        if episode_id_list.__len__() > 0:
            self.clean_episode(episode_id_list, title)

    def clean_episode(self, episode_id_list, title):
        for episode in Episode.objects.filter(title=title).exclude(id__in=episode_id_list):
            try:
                episode.delete()
                print('episode {} delete success'.format(episode.id))
            except Exception as err:
                print('episode {} process'.format(episode.id))

    def validate(self, attrs):
        title_type = attrs.get('title_type', None)
        total_episode = attrs.get('total_episode', -1)
        episode_data_list = attrs.get('episode_list', [])
        if title_type == 'MOVIE':  # validate of title_type is `MOVIE` episode and total is one
            if total_episode != 1 and episode_data_list.__len__() != 1:
                raise exceptions.ValidationError('Type title is not 1')
        view = self.context.get('view', None)
        instance = None
        if view and view.action in ['update', 'partial_update']:
            instance = view.get_object()
        episode_list = Episode.objects.all()
        if instance:
            episode_list = episode_list.exclude(title=instance)
        episode_name_list = map(lambda episode: episode.get('episode_name', None), episode_data_list)
        episode_list_temp = []
        episode_name_list = list(episode_name_list)
        for episode_name in episode_name_list:
            if episode_name in episode_list_temp:
                raise exceptions.ValidationError('{} episode name of title to duplicate'.format(episode_name))
            else:
                episode_list_temp.append(episode_name)

        if episode_list.filter(name__in=episode_list_temp).exists():
            raise exceptions.ValidationError('episode name of title to duplicate')

        return attrs

    def validate_rating_id(self, rating_id):
        if rating_id:
            if Rating.objects.filter(id=rating_id).exists():
                return rating_id
            raise exceptions.ValidationError('{} is not exist.')
        return None

    def validate_studio_id(self, studio_id):
        if studio_id:
            if Studio.objects.filter(pk=studio_id).exists():
                return studio_id
            raise exceptions.ValidationError('{} not found'.format(studio_id))
        return None

    def validate_vendor_id(self, vendor_id):
        if vendor_id:
            if Vendor.objects.filter(pk=vendor_id).exists():
                return vendor_id
            raise exceptions.ValidationError('{} not found'.format(vendor_id))
        return None

    def validate_country_id(self, country_id):
        if country_id:
            if Country.objects.filter(code=country_id).exists():
                return country_id
            raise exceptions.ValidationError('{} not found'.format(country_id))
        return None

    def validate_genre_id(self, genre_id):
        if genre_id:
            if Genre.objects.filter(id=genre_id).exists():
                return genre_id
            raise exceptions.ValidationError('{} is not exists'.format(genre_id))
        return None


    def validate_title_type(self, title_type):
        from config.models import System
        if System.is_exist(title_type, 'TITLE_TYPE'):
            return title_type
        raise exceptions.ValidationError('{} is not exists.'.format(title_type))

    def validate_name(self, name):
        view = self.context.get('view', None)
        instance = None
        if view and view.action in ['update', 'partial_update']:
            instance = view.get_object()

        title_list = Title.objects.filter(name=name)
        if instance:
            title_list = title_list.exclude(id=instance.id)
        if title_list.exists():
            raise exceptions.ValidationError('{} is duplicate'.format(name))
        return name.upper().strip(" ")

    def validate_status(self, status):
        if System.is_exist(status, 'TITLE_STATUS'):
            return status
        raise exceptions.ValidationError('{} is not exists.'.format(status))

    def validate_amortize_category_id(self, amortize_category_id):
        if amortize_category_id:
            if Category.objects.filter(pk=amortize_category_id).exists():
                return amortize_category_id
            raise exceptions.ValidationError('amortize category %s not found' % amortize_category_id)
        else:
            return None
