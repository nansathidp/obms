import base64
import os
from wsgiref.util import FileWrapper

from django.conf import settings
from django.db.models import Q
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import exceptions, filters, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from alert.models import Alert
from deal.models import Deal
from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ExportPermission, \
    ImportPermission, ViewPermission
from .cacheds import delete_cached, get_cached, set_cached
from .filters import TitleFilter
from .models import Title
from .serializer import TitleListSerializer, TitleSerializer
from .serializer_title import TitleFromSerializer
from .serializer_validate_period import ValidatePeriodSerializer
from .serializers_aka import TitleAKAExportSerializer, TitleAKAImportSerializer
from .serializers_import import FileImportSerializer
from .tasks import export_file_aka, import_file, import_file_aka


class TitleView(viewsets.ModelViewSet):
    queryset = Title.objects.all().select_related('studio').select_related('vendor')
    serializer_class = TitleListSerializer
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    filter_class = TitleFilter

    search_fields = (
        'id',
        'name',
        'duration',
        'total_episode',
    )

    ordering_fields = (
        'id',
        'name',
        'duration',
        'total_episode',
    )

    action_serializers = {
        'list': TitleListSerializer,
        'create': TitleFromSerializer,
        'download_aka': TitleAKAExportSerializer,
        'import_aka': TitleAKAImportSerializer,
        'import_file': FileImportSerializer,
        'retrieve': TitleSerializer,
        'destroy': TitleListSerializer,
        'update': TitleFromSerializer,
        'partial_update': TitleFromSerializer,
        'validated_period': ValidatePeriodSerializer
    }
    app = 'title'
    model = 'title'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
        'download_aka': [ExportPermission],
        'import_aka': [ImportPermission],
        'import_file': [ImportPermission],
        'validated_period': [ViewPermission],
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        title = self.perform_create(serializer)
        serializer_response = TitleSerializer(title)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def get_object(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        pk = self.kwargs[lookup_url_kwarg]
        instance = get_cached('object', pk)
        if instance:
            return instance
        instance = super().get_object()
        set_cached('object', instance.id, instance)
        return instance

    def retrieve(self, request, *args, **kwargs):
        title = self.get_object()
        response = get_cached('view', title.id)
        if response:
            return Response(response, status=status.HTTP_201_CREATED)
        return super().retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        delete_cached('object', instance.id)
        self.get_object()
        return Response(serializer.data)

    def perform_destroy(self, instance):
        try:
            super().perform_destroy(instance)
        except Exception as err:
            print(err)
            raise exceptions.ValidationError('Title of process.')

    @action(methods=['POST'], detail=False)
    def download_aka(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        alert = Alert.objects.create(
            percent=10,
            code_name='title.title'
        )
        results = serializer.validated_data
        export_file_aka.delay('title', results['title_list'], alert.id)
        return Response({'id': alert.id}, status=status.HTTP_200_OK)

    @action(methods=['POST'], detail=False)
    def import_aka(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(True)
        text = request.data['tmp_file']
        text_byte = text.encode("UTF-8")
        text_base64 = base64.b64decode(text_byte)
        source = text_base64.decode("UTF-8")
        alert = Alert.objects.create(code_name='title.title')
        import_file_aka.delay('title', source, request.user.id, alert.id)
        return Response({'id':alert.id}, status=status.HTTP_200_OK)

    @action(methods=['POST'], detail=False)
    def import_file(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        text = request.data['tmp_file']
        text_byte = text.encode("UTF-8")
        text_base64 = base64.b64decode(text_byte)
        source = text_base64.decode("UTF-8")
        alert = Alert.objects.create(code_name='title.title')
        import_file.delay(source, request.user.id, alert.id)
        return Response({'id': alert.id}, status=status.HTTP_201_CREATED)

    @action(methods=['GET'], detail=False)
    def download_format(self, request, *args, **kwargs):
        path = os.path.join('%s/title/fixture/title_import_csv.csv' % settings.BASE_DIR)
        wrapper = FileWrapper(open(path, 'rb'))
        response = HttpResponse(wrapper, content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=title_format_import.csv'
        return response

    @action(methods=['PUT'], detail=True)
    def validated_period(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(True)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        title = self.get_object()
        if Deal.objects.filter(Q(linear__title=title) | Q(video__title=title)).exists():
            raise exceptions.ValidationError('{} ({}) is process '.format(title.name, title.id))
        return super().destroy(request, *args, **kwargs)
