from rest_framework import serializers, exceptions

from sub_genre.models import SubGenre


class SubGenreTitleFormSerializer(serializers.Serializer):
    id = serializers.CharField(required=True, max_length=15)
    name = serializers.CharField(required=False, allow_null=True, allow_blank=True, default=None)

    def validate_id(self, pk):
        try:
            SubGenre.objects.get(pk=pk)
            return pk
        except SubGenre.DoesNotExist:
            raise exceptions.ValidationError()