from rest_framework import exceptions, serializers

from config.models import System
from language.models import Language
from .models import Localization


class TitleLanguageSerializer(serializers.Serializer):
    code = serializers.CharField(required=True, max_length=3)
    name = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    is_checked = serializers.BooleanField(default=False)
    status = serializers.ChoiceField(choices=Localization.STATUS_CHOICES)

    def validate_code(self, code):
        if Language.objects.filter(code=code, is_localize=Language.ACTIVE).exists():
            return code
        raise exceptions.ValidationError('{} is not found'.format(code))




class TitleLocalizationFromSerializer(serializers.Serializer):
    name = serializers.CharField(required=True, max_length=100)
    language_list = serializers.ListField(
        child=TitleLanguageSerializer(),
        allow_null=True,
        default=[]
    )

    def validate_name(self, name):
        if System.is_exist(name, "LOCALIZE_TYPE"):
            return name
        raise exceptions.ValidationError('{} not found'.format(name))
