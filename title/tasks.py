import csv
import json
import uuid

from celery import shared_task
from django.conf import settings
from django.utils import timezone

from alert.models import Alert
from country.models import Country
from episode.models import Episode, PublishEpisode, AKAEpisode
from genre.models import Genre
from language.models import Language
from people.models import People
from publish.models import Publish
from rating.models import Rating
from role.models import Role
from title.models import Title, AKA


def convert_datetime(title_right_start, title_right_end, publish_start, publish_end, current_time_zone):
    try:

        title_right_start = timezone.datetime.strptime(title_right_start,
                                                       settings.IMPORT_DATE_TIME_FORMAT)
        title_right_start = timezone.datetime(year=title_right_start.year,
                                              month=title_right_start.month,
                                              day=title_right_start.day,
                                              tzinfo=current_time_zone)
        title_right_end = timezone.datetime.strptime(title_right_end, settings.IMPORT_DATE_TIME_FORMAT)
        title_right_end = timezone.datetime(year=title_right_end.year, month=title_right_end.month,
                                            day=title_right_end.day,
                                            tzinfo=current_time_zone)

        publish_start = timezone.datetime.strptime(publish_start, settings.IMPORT_DATE_TIME_FORMAT)
        publish_start = timezone.datetime(year=publish_start.year, month=publish_start.month,
                                          day=publish_start.day,
                                          tzinfo=current_time_zone)
        publish_end = timezone.datetime.strptime(publish_end, settings.IMPORT_DATE_TIME_FORMAT)
        publish_end = timezone.datetime(year=publish_end.year, month=publish_end.month,
                                        day=publish_end.day,
                                        tzinfo=current_time_zone)
        return title_right_start, title_right_end, publish_start, publish_end
    except Exception as err:
        print(err)
        pass
    try:
        print(title_right_start)
        title_right_start = timezone.datetime.strptime(title_right_start,
                                                       settings.IMPORT_DATE_FORMAT)
        title_right_start = timezone.datetime(year=title_right_start.year,
                                              month=title_right_start.month,
                                              day=title_right_start.day, hour=0, minute=0, second=0,
                                              tzinfo=current_time_zone)
        title_right_end = timezone.datetime.strptime(title_right_end, settings.IMPORT_DATE_FORMAT)
        title_right_end = timezone.datetime(year=title_right_end.year, month=title_right_end.month,
                                            day=title_right_end.day, hour=23, minute=59, second=59,
                                            tzinfo=current_time_zone)

        publish_start = timezone.datetime.strptime(publish_start, settings.IMPORT_DATE_FORMAT)
        publish_start = timezone.datetime(year=publish_start.year, month=publish_start.month,
                                          day=publish_start.day, hour=0, minute=0, second=0,
                                          tzinfo=current_time_zone)
        publish_end = timezone.datetime.strptime(publish_end, settings.IMPORT_DATE_FORMAT)
        publish_end = timezone.datetime(year=publish_end.year, month=publish_end.month,
                                        day=publish_end.day,
                                        hour=23, minute=59, second=59, tzinfo=current_time_zone)
        return title_right_start, title_right_end, publish_start, publish_end
    except Exception as err:
        raise err


def set_format_aka(source, account_id, app):
    data_list = []
    key_app = 'title_id' if app == 'title' else 'episode_id'
    language_code_input_list = []
    language_code_list = Language.objects.filter(is_aka='Y').values_list('code', flat=True)
    language_code_list = list(language_code_list)
    for _index_row, row in enumerate(source.split('\n')):
        data_slot = row.split(',')
        if _index_row == 0:
            language_code_input_list = data_slot
            continue
        content_id = data_slot[0]
        for _index_col, column in enumerate(language_code_input_list):
            try:
                is_empty = data_slot[_index_col] == '' and data_slot[_index_col - 1] == ''
                if _index_col > len(data_slot):
                    break
                if is_empty:
                    continue
            except Exception as err:
                break

            if _index_col == 0:
                continue
            if _index_col == 1:
                continue
            columns = column.split('_')
            if len(columns) < 2:
                continue
            prefix = columns[0]
            code = columns[1]

            # print(language_code_list, code)
            template = {}
            if code in language_code_list:
                if template.get('language_id', None) is None:
                    template.update({'language_id': code})
            else:
                continue
            if template.get(key_app, None) is None:
                template.update({key_app: content_id})
            if template.get('account_created_id', None) is None:
                template.update({'account_created_id': account_id})
            if prefix == 'synopsis':
                template.update({'synopsis': data_slot[_index_col], 'name': data_slot[_index_col-1]})
                data_list.append(template)
            else:
                continue
    return data_list


def set_format_title(title_file, account_id):
    publish_period_list = list()
    title_name_list = list()
    episode_name_list = []
    title_info_list = []
    for index, title_line in enumerate(title_file.split('\n')):
        if index > 0:
            title_col = title_line.split(',')

            if len(title_col) != 39:
                continue
            title_name_en = title_col[1]
            title_name_th = title_col[2]
            title_type = title_col[3]
            synopsis_en = title_col[4]
            synopsis_th = title_col[5]
            release_year = title_col[6]
            rating_name = title_col[7]
            actor_name = title_col[8]
            director_name = title_col[9]
            writer_name = title_col[10]
            duration = title_col[11]
            total_episode = title_col[13]
            title_right_start = title_col[17]
            title_right_end = title_col[18]

            episode_en = title_col[20]
            episode_th = title_col[21]
            episode_synopsis_en = title_col[22]
            episode_synopsis_th = title_col[23]
            episode_pac_no = title_col[24]
            thumb_poster = title_col[25]
            thumb_true_landscape = title_col[26]
            thumb_landscape = title_col[27]
            thumb_portrait = title_col[29]
            thumb_square = title_col[30]
            thumb_ott_landscape = title_col[31]
            thumb_ott_portrait = title_col[32]
            genre_name = title_col[33]
            publish_zone = title_col[34]
            publish_start = title_col[35]
            publish_end = title_col[36]

            thumb_th = title_col[37]
            thumb_en = title_col[38]
            current_time_zone = timezone.get_current_timezone()
            duration = duration if duration.__len__() == 6 else '000000'

            tag_thumb_en = 'EN'
            tag_thumb_local = publish_zone.upper()
            tag_thumb_portrait = 'PORTRAIT'
            tag_thumb_square = 'SQUARE'
            tag_thumb_landscape = 'LANDSCAPE'
            tag_thumb_poster = 'POSTER'
            tag_thumb_true_landscape = 'TRUE_ID_' + tag_thumb_landscape
            tag_thumb_landscape_ott = 'OTT_' + tag_thumb_landscape
            tag_thumb_portrait_ott = 'OTT_' + tag_thumb_portrait

            try:
                release_year = int(release_year)
            except Exception as e:
                release_year = timezone.now().year

            try:
                total_episode = int(total_episode)
            except Exception as e:
                total_episode = 0

            try:
                title_right_start, \
                title_right_end, \
                publish_start, \
                publish_end = convert_datetime(
                    title_right_start,
                    title_right_end,
                    publish_start,
                    publish_end,
                    current_time_zone
                )
            except Exception as err:
                raise err
            publish_period_list.append(
                {
                    'datetime_start': publish_start,
                    'datetime_end': publish_end,
                }
            )
            title_name_list.append(title_name_en.upper())
            episode_name_list.append(episode_en.upper())
            title_info = {
                'name': title_name_en.upper(),
                'title_type': title_type,
                'release_year': release_year,
                'duration': duration,
                'total_episode': total_episode,
                'rating': rating_name,
                'genre': genre_name,
                'datetime_right_start': title_right_start,
                'datetime_right_end': title_right_end,
                'thumb_list': [
                    {
                        'tag': tag_thumb_en,
                        'file_name': thumb_en,
                    },
                    {
                        'tag': tag_thumb_local,
                        'file_name': thumb_th,
                    },
                    {
                        'tag': tag_thumb_poster,
                        'file_name': thumb_poster
                    },
                    {
                        'tag': tag_thumb_portrait,
                        'file_name': thumb_portrait,
                    },
                    {
                        'tag': tag_thumb_landscape,
                        'file_name': thumb_landscape,
                    },
                    {
                        'tag': tag_thumb_square,
                        'file_name': thumb_square,
                    },
                    {
                        'tag': tag_thumb_true_landscape,
                        'file_name': thumb_true_landscape,
                    },
                    {
                        'tag': tag_thumb_portrait_ott,
                        'file_name': thumb_ott_portrait,
                    },
                    {
                        'tag': tag_thumb_landscape_ott,
                        'file_name': thumb_ott_landscape
                    }
                ],
                'cast_list': [
                    {
                        'role_name': 'actor',
                        'people_name': actor_name,
                        'account_created_id': account_id,
                    },
                    {
                        'role_name': 'writer',
                        'people_name': writer_name,
                        'account_created_id': account_id,
                    },
                    {
                        'role_name': 'director',
                        'people_name': director_name,
                        'account_created_id': account_id,
                    }
                ],
                'aka_list': [
                    {
                        'name': title_name_en,
                        'synopsis': synopsis_en,
                        'language_id': 'eng',
                        'account_created_id': account_id,
                    },
                    {
                        'name': title_name_th,
                        'synopsis': synopsis_th,
                        'language_id': publish_zone.lower(),
                        'account_created_id': account_id,
                    }
                ],
                'episode': {
                    'name': episode_en.upper(),
                    'pac_no': episode_pac_no,
                    'duration': duration,
                    'account_created_id': account_id,
                    'publish_zone': publish_zone,
                    'publish_datetime_start': publish_start,
                    'publish_datetime_end': publish_end,
                    'aka_list': [
                        {
                            'name': episode_en,
                            'synopsis': episode_synopsis_en,
                            'language_id': 'eng',
                            'account_created_id': account_id,
                        },
                        {
                            'name': episode_th,
                            'synopsis': episode_synopsis_th,
                            'language_id': publish_zone.lower(),
                            'account_created_id': account_id,
                        }
                    ]
                },
                'publish_country_code': publish_zone,
                'account_created_id': account_id
            }
            title_info_list.append(title_info)

    return {
        'publish_period_list': publish_period_list,
        'title_name_list': title_name_list,
        'episode_name_list': episode_name_list,
        'title_info_list': title_info_list
    }


def get_or_create_title(name, title_type, duration, release_year, total_episode, rating, genre, account_created_id,
                        **kwargs):
    title = Title.objects.filter(name=name).first()

    if title:
        return title
    rating = Rating.objects.filter(name=rating).first()
    if not rating:
        rating = Rating.objects.create(name=rating, account_created_id=account_created_id)
    genre = Genre.objects.filter(name=genre).first()
    if not genre:
        genre = Genre.objects.create(name=genre, account_created_id=account_created_id)
    return Title.objects.create(
        name=name,
        rating=rating,
        genre=genre,
        title_type=title_type,
        status="Select",
        duration=duration,
        release_year=release_year,
        total_episode=total_episode,
        account_created_id=account_created_id
    )


def get_or_create_episode(title, name, pac_no, duration, account_created_id, aka_list, **kwargs):
    episode = Episode.objects.filter(name=name).first()
    if not episode:
        episode = Episode.objects.create(
            name=name,
            title=title,
            pac_no=pac_no,
            duration=duration,
            account_created_id=account_created_id,
        )

    for aka in aka_list:
        language = Language.get_by_code(aka['language_id'])
        if not language:
            continue
        if AKAEpisode.objects.filter(language_id=aka['language_id'], episode=episode).exists():
            continue
        AKAEpisode.objects.create(
            episode=episode,
            language=language,
            name=aka.get('name', episode.name),
            synopsis=aka.get('synopsis', ''),
            account_created_id=account_created_id,
        )

    return episode


def get_or_create_cast_aka(title, aka_list, cast_list, account_created_id, **kwargs):
    for aka in aka_list:
        language = Language.get_by_code(aka['language_id'])
        if not language:
            print(aka)
            continue
        if AKA.objects.filter(title=title, language=language).exists():
            continue
        AKA.objects.create(
            title=title,
            language=language,
            name=aka.get('name', title.name),
            synopsis=aka.get('synopsis', ''),
            account_created_id=account_created_id,
        )

    for cast in cast_list:
        if not (cast['role_name'] and cast['people_name']):
            continue
        role, is_create_role = Role.objects.get_or_create(
            name=cast['role_name'],
            defaults={'account_created_id': cast['account_created_id']}
        )

        people, is_create_people = People.objects.get_or_create(
            name=cast['people_name'],
            defaults={'account_created_id': cast['account_created_id']}
        )
        print(people, role, title.cast_set.filter(role=role, people=people).exists())
        if not title.cast_set.filter(role=role, people=people).exists():
            title.cast_set.create(role=role, people=people, account_created_id=account_created_id)


def is_validate_period_publish(datetime_right_start, datetime_right_end, publish_datetime_start, publish_datetime_end):
    is_datetime_start = datetime_right_start <= publish_datetime_start < datetime_right_end
    is_datetime_end = datetime_right_start < publish_datetime_end <= datetime_right_end
    return is_datetime_start and is_datetime_end and publish_datetime_start < publish_datetime_end


def create_publish_episode(episode_id, publish_datetime_start, publish_datetime_end, account_created_id, publish_zone,
                           **kwargs):
    country = Country.objects.filter(code=publish_zone).first()

    if not country:
        return None
    publish = Publish.objects.filter(country__country=country).first()
    if not publish:
        publish = Publish.objects.create(
            desc='%s (%s)' % (country.name, country.code),
            account_created_id=account_created_id,

        )
        publish.country_set.create(country=country, account_created_id=account_created_id)
    is_datetime_start = PublishEpisode.objects.filter(
        datetime_start__range=(publish_datetime_start, publish_datetime_end)
    )
    is_datetime_end = PublishEpisode.objects.filter(
        datetime_end__range=(publish_datetime_start, publish_datetime_end)
    )
    if is_datetime_start and is_datetime_end:
        return None

    episode_publish = PublishEpisode.objects.filter(
        episode_id=episode_id,
        publish=publish,
        datetime_start__gte=publish_datetime_start
    ).order_by("datetime_start").first()
    sequence_no = episode_publish.sequence_no if episode_publish else 1
    if not episode_publish:
        PublishEpisode.objects.create(
            episode_id=episode_id,
            datetime_start=publish_datetime_start,
            datetime_end=publish_datetime_end,
            sequence_no=sequence_no,
            account_created_id=account_created_id
        )


@shared_task
def import_file(title_file, account_id, alert_id):
    import json
    response = {'message': "Reading file and reformat data ....", 'results': [], 'error': ""}
    try:

        Alert.objects.filter(id=alert_id).update(
            percent=10,
            is_response=True,
            response=json.dumps(response)
        )
        info_format = set_format_title(title_file, account_id)
        response.update({'message': "Set format data success .."})
        Alert.objects.filter(id=alert_id).update(
            percent=15,
            is_response=True,
            response=json.dumps(response)
        )
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(
            percent=10,
            is_error=True,
            backtrack=err
        )
        raise err
    try:
        publish_period_list = info_format.get('publish_datetime_start', [])
        publish_period_list.sort(key=lambda period: period['datetime_start'])
        for index, publish_period in enumerate(publish_period_list):
            assert publish_period['datetime_start'] < publish_period[
                'datetime_end'], 'publish period conflict.'
            publish_period_next = publish_period_list[index + 1]
            assert publish_period_next['datetime_start'] > publish_period[
                'datetime_end'], 'publish period conflict.'
        response.update({'message': "Validate publish conflict"})
        Alert.objects.filter(id=alert_id).update(
            percent=20,
            is_response=True,
            response=json.dumps(response)
        )
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(
            percent=20,
            is_error=True,
            is_response=True,
            backtrack=json.dumps(response)
        )
        raise err
    try:
        response.update({'message': "Process..."})
        Alert.objects.filter(id=alert_id).update(
            percent=50,
            is_response=True,
            response=json.dumps(response)
        )

        title_id_list = []
        title_response_list = []
        for row_of_title, title_item in enumerate(info_format['title_info_list']):
            episode_from = title_item.get('episode', {})
            if not is_validate_period_publish(
                    title_item['datetime_right_start'],
                    title_item['datetime_right_end'],
                    episode_from['publish_datetime_start'],
                    episode_from['publish_datetime_end']):
                print("Publish period is not periods")
                continue
            title = get_or_create_title(**title_item)
            title_item.update({'title': title})
            get_or_create_cast_aka(**title_item)
            title_id_list.append(title.id)
            title_response = {
                'id': title.id,
                'name': title.name,
                'duration': title.duration,
                'title_type': title.title_type,
                'total_episode': title.total_episode,
                'datetime_right_start': title_item['datetime_right_start'].isoformat(),
                'datetime_right_end': title_item['datetime_right_end'].isoformat(),
            }
            title_response_list.append(title_response)
            if episode_from:
                episode_from.update({'title': title})
                episode = get_or_create_episode(**episode_from)
                episode_from.update({'episode_id': episode.id})
                if create_publish_episode(**episode_from):
                    print(" Create publish success")
                else:
                    print(" Publish zone of none")
            else:
                print(" Episode of none")

        response.update({'message': "Input Success ...", 'results': title_response_list})
        Alert.objects.filter(id=alert_id).update(
            percent=100,
            is_response=True,
            is_success=True,
            response=json.dumps(response)
        )
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(
            is_response=True,
            is_error=True,
            backtrack=err
        )
        raise err


@shared_task
def export_file_aka(app_name, content_id_list, alert_id=None):
    response = {'message': '', 'results': ''}
    print(content_id_list, app_name)
    if app_name == 'title':
        content_list = Title.objects.filter(id__in=content_id_list)
        print('ping title ', content_list)
    elif app_name == 'episode':
        content_list = Episode.objects.filter(id__in=content_id_list)
    else:
        content_list = Title.objects.none()
    if not content_list.exists():
        response.update({'message': 'content not found'})
        Alert.objects.filter(pk=alert_id).update(is_error=True, is_response=True, percent=20,
                                                 response=json.dumps(response))
        return 'content not found'
    path_file = '%s/%s/fixture_ignore/title_aka_%s.csv' % (settings.BASE_DIR, app_name, uuid.uuid4().hex)
    language_aka_list = Language.objects.filter(is_aka='Y').values_list('code', flat=True)
    language_aka_list = list(language_aka_list)
    headers = ['id', 'name']
    for code in language_aka_list:
        header_name = 'name_%s' % code
        header_synopsis = 'synopsis_%s' % code
        headers += [header_name, header_synopsis]
    response.update({'message': 'initial columns by languages ...'})
    Alert.objects.filter(pk=alert_id).update(is_error=True, is_response=True, percent=40, response=json.dumps(response))
    with open(path_file, 'w', newline='') as file:
        writer_csv = csv.writer(file)
        writer_csv.writerow(headers)
        response.update({'message': 'make columns by languages ...'})
        Alert.objects.filter(pk=alert_id).update(
            is_error=True,
            is_response=True,
            percent=50,
            response=json.dumps(response)
        )
        for content in content_list:
            name = getattr(content, 'name', '')
            pk = getattr(content, 'id', '')
            writer_csv.writerow([pk, name])
        file.close()
        response.update({'is_file': True, 'path_file': path_file})
        Alert.objects.filter(pk=alert_id).update(
            is_error=False,
            is_response=True,
            is_success=True,
            percent=100,
            response=json.dumps(response)
        )
    return 'path %s ' % path_file

@shared_task
def import_file_aka(app_name, source, account_id, alert_id):
    response = {'message': 'set value of schema', 'results': ''}
    Alert.objects.filter(pk=alert_id).update(
        is_error=False,
        is_response=True,
        percent=35,
        response=json.dumps(response)
    )
    try:
        aka_template_list = set_format_aka(source, account_id, app_name)
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(
            is_response=True,
            is_error=True,
            backtrack=err
        )
        raise err
    response.update({'message': 'set value of schema success.'})
    Alert.objects.filter(pk=alert_id).update(
        is_error=False,
        is_response=True,
        percent=40,
        response=json.dumps(response)
    )
    from .models import AKA
    from episode.models import AKAEpisode
    try:
        assert app_name in ['title', 'episode'], 'content not support'
        parent_key, model = ('title_id', AKA) if app_name == 'title' else ('episode_id', AKAEpisode)

    except Exception as err:
        Alert.objects.filter(pk=alert_id).update(
            is_error=True,
            is_response=True,
            percent=45,
            backtrack=err
        )
        raise err
    try:
        assert len(aka_template_list) > 0,'content is empty.'

    except Exception as err:
        response.update({'message': 'set value of schema success.', 'results': []})
        Alert.objects.filter(pk=alert_id).update(
            is_success=True,
            is_response=True,
            percent=100,
            response=json.dumps(response),
            backtrack=err
        )
        raise err
    try:
        response.update(
            {'message': 'Wait aka update... ', 'results': {'app': app_name, 'items_list': aka_template_list}})
        Alert.objects.filter(pk=alert_id).update(
            is_error=False,
            is_response=True,
            percent=60,
            response=json.dumps(response)
        )
        for aka_template in aka_template_list:

            assert aka_template.get(parent_key, False), "%s is None" % parent_key
            query = { 'language_id': aka_template['language_id'], parent_key: aka_template[parent_key] }
            model.objects.filter(**query).delete()
            model.objects.create(**aka_template)
            print('AKA %s to %s create success' % (app_name, aka_template))



        response.update({'message': 'Validate data to database', 'results': {'app': app_name, 'items_list': aka_template_list}})
        Alert.objects.filter(pk=alert_id).update(
            is_error=False,
            is_response=True,
            percent=80,
            response=json.dumps(response)
        )
        for aka_item in aka_template_list:
            assert aka_item.get(parent_key, False), "%s is None" % parent_key
            query = {'language_id': aka_item['language_id'], parent_key: aka_item[parent_key]}
            aka = model.objects.filter(**query).first()
            assert aka is not None, "AKA %s language is not found" % app_name
            name = getattr(aka, 'name', None)
            synopsis = getattr(aka, 'synopsis', None)
            assert name == aka_item['name'], 'AKA %s of language %s to name not update' % (app_name, aka_item['language_id'])
            assert synopsis == aka_item['synopsis'] , 'AKA %s of language %s to synopsis not update' % (app_name, aka_item['language_id'])

        response.update({'message': 'Import AKA finish.', 'results': {'app': app_name, 'items_list': aka_template_list}})
        Alert.objects.filter(pk=alert_id).update(
            is_error=False,
            is_response=True,
            is_success=True,
            percent=100,
            response=json.dumps(response)
        )
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(
            is_response=True,
            is_error=True,
            backtrack=err
        )
        raise err


