from django.conf import settings
from rest_framework import serializers

from config.models import System
from deal.models import Linear, Video
from episode.models import Episode
from language.models import Language
from people.models import AKA as PeopleAKA
from people.serializers import PeopleMiniSerializer
from role.models import AKA as RoleAKA
from role.serializers import AKASerializer as RoleAKASerializer
from role.serializers import RoleMiniSerializer
from title.models import AKA, Cast, Cost, SubGenre, Thumbnail, Title


class TitleListSerializer(serializers.ModelSerializer):
    studio_name = serializers.CharField(source='studio.name', default="")
    vendor_name = serializers.CharField(source='vendor.name', default="")

    class Meta:
        model = Title
        fields = (
            'id',
            'name',
            'duration',
            'title_type',
            'total_episode',
            'studio_id',
            'studio_name',
            'vendor_id',
            'vendor_name',
        )


class TitleSerializer(serializers.ModelSerializer):
    aka_synopsis_list = serializers.SerializerMethodField()
    localize_list = serializers.SerializerMethodField(method_name="get_localization")
    cast_list = serializers.SerializerMethodField()
    title_cost_list = serializers.SerializerMethodField(method_name='get_cost')
    episode_list = serializers.SerializerMethodField(method_name='get_episode')
    right_list = serializers.SerializerMethodField()
    thumbnail_list = serializers.SerializerMethodField()
    sub_genre_list = serializers.SerializerMethodField()

    class Meta:
        model = Title
        fields = (
            'id',
            'name',
            'vendor_id',
            'country_id',
            'studio_id',
            'genre_id',
            'rating_id',
            'bu_id',
            'other_id',
            'amortize_category_id',
            'status',
            'title_type',
            'total_episode',
            'release_year',
            'cost',
            'duration',
            'aka_synopsis_list',
            'localize_list',
            'episode_list',
            'cast_list',
            'title_cost_list',
            'right_list',
            'thumbnail_list',
            'sub_genre_list',
        )

    def get_aka_synopsis_list(self, title):
        language_code_list = Language.objects.filter(
            is_aka=Language.ACTIVE
        ).values_list(
            'code',
            flat=True
        )
        language_code_list = list(language_code_list)
        aka_list = title.aka_set.filter(language_id__in=language_code_list)
        if aka_list.exists() and aka_list.count() == language_code_list.__len__():
            serializer_aka_list = AKATitleSerializer(aka_list, many=True)
            return serializer_aka_list.data
        else:
            aka_list = []
            for language_code in language_code_list:
                aka = title.aka_set.filter(language_id=language_code).first()

                if aka:
                    serializer_aka = AKATitleSerializer(aka)
                    aka_list.append(serializer_aka.data)
                else:
                    item = {
                        'name': title.name,
                        'synopsis': '',
                        'language': language_code,
                    }
                    aka_list.append(item)
            return aka_list

    def get_cast_list(self, title):
        cast_list = title.cast_set.all().order_by('-role_id')
        role_list = CastMiniSerializer(cast_list, many=True).data
        temp_list = dict()
        for item in role_list:
            key = 'role_{}'.format(item.get('role_id'))
            temp = temp_list.get(key, None)
            people = item.get('people')
            role = item.get('role')
            if temp:
                people_list = temp.get('people_list', [])
                people_list.append(people)
            else:
                temp = {
                    'id': role['id'],
                    'name': role['name'],
                    'people_list': [people]
                }
                temp_list.update({key: temp})
        response_list = []
        for k, v in temp_list.items():
            response_list.append(v)
        return response_list

    def get_cost(self, title):
        cost_list = Cost.objects.filter(title=title)
        serializer_cost_list = CostTitleSerializer(cost_list, many=True)
        return serializer_cost_list.data

    def get_localization(self, title):
        language_list = Language.objects.filter(is_localize=Language.ACTIVE)
        system_value_list = []
        for config_type in System.objects.filter(group_name='LOCALIZE_TYPE'):
            localization_list = title.localization_set.filter(type=config_type.value)
            serializer_language_list = LocalizationLanguage(
                language_list,
                context={'localization_list': localization_list},
                many=True,
            )
            system_value = {
                'name': config_type.value,
                'language_list': serializer_language_list.data,
            }
            system_value_list.append(system_value)
        return system_value_list

    def get_episode(self, title):
        episode_list = Episode.objects.filter(title=title).order_by('pac_no')
        serializer_episode_list = EpisodeSerializer(episode_list, many=True)
        return serializer_episode_list.data

    def get_right_list(self, title):
        video_list = Video.objects.filter(title=title).select_related('deal')
        linear_list = Linear.objects.filter(title=title).select_related('deal')
        item_list = RightLinearSerializer(linear_list, many=True).data + \
                    RightVideoSerializer(video_list, many=True).data
        item_list.sort(key=lambda item: item['datetime_right_start'])
        return item_list

    def get_thumbnail_list(self, title):
        return ThumbnailSerializer(title.thumbnail_set.all(), many=True).data

    def get_sub_genre_list(self,title):
        return SubGenreTitleSerializer(title.subgenre_set.all(), many=True).data


class ThumbnailSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField()
    text_source = serializers.SerializerMethodField()

    class Meta:
        model = Thumbnail
        fields = ('tag', 'uri', 'text_source')

    def get_uri(self, thumb):
        return '%s%s' % (settings.MEDIA_URL, thumb.get_name())

    def get_text_source(self, thumb):
        return ""


class RolAKACastSerializer(RoleAKASerializer):

    def get_language(self, aka):
        return aka.language.code


class RightLinearSerializer(serializers.ModelSerializer):
    deal_no = serializers.CharField(source="deal_id", default="")
    remark = serializers.CharField(source='deal.description', default="")

    class Meta:
        model = Linear
        fields = (
            'datetime_right_start',
            'datetime_right_end',
            'deal_no',
            'remark',
            'type',
        )


class RightVideoSerializer(serializers.ModelSerializer):
    deal_no = serializers.CharField(source="deal_id")
    remark = serializers.SerializerMethodField()

    class Meta:
        model = Video
        fields = (
            'datetime_right_start',
            'datetime_right_end',
            'deal_no',
            'remark',
            'type',
        )

    def get_remark(self, video):

        config = System.objects.filter(group_name='VOD_TYPE', value__icontains=video.type).first()

        if not config:
            return video.deal.description
        vod_type = config.get_vod_type_display()

        if vod_type:
            messages = []
            if vod_type['is_mg']:
                messages.append('MG(%s)' % video.deem_price)
            if vod_type['is_deem_price']:
                messages.append('Deem Price(%s)' % video.mg)
            if vod_type['is_rev_share']:
                messages.append('Revenue shear(%s)' % video.rev_share)
            return ",".join(messages)
        return video.deal.description


class PeopleCastSerializer(serializers.ModelSerializer):
    id = serializers.CharField(source='people_id')
    language = serializers.CharField(source='language_id')

    class Meta:
        model = PeopleAKA
        fields = (
            'id',
            'name',
            'language',
        )


class CastSerializerView(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    name_list = serializers.SerializerMethodField()
    people_list = serializers.SerializerMethodField()

    class Meta:
        model = Cast
        fields = ('id', 'name_list', 'people_list',)

    def get_id(self, cast):
        return cast.role_id

    def get_name_list(self, cast):
        role_aka_serializer = RolAKACastSerializer(RoleAKA.objects.filter(role=cast.role), many=True)
        return role_aka_serializer.data

    def get_people_list(self, cast):
        people_aka_list = cast.people.aka_set.all()
        serializer_people_aka = PeopleCastSerializer(people_aka_list, many=True)
        return serializer_people_aka.data


class CostTitleSerializer(serializers.ModelSerializer):
    cost_name = serializers.CharField(source='cost.name', default="")

    class Meta:
        model = Cost
        fields = (
            'cost_id',
            'episode_cost',
            'cost_name',
            'unit_cost',
            'total_cost',
        )


class AKATitleSerializer(serializers.ModelSerializer):
    language = serializers.CharField(source='language_id')

    class Meta:
        model = AKA
        fields = (
            'language',
            'name',
            'synopsis',
        )


class LocalizationLanguage(serializers.ModelSerializer):
    is_checked = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()

    class Meta:
        model = Language
        fields = (
            'code',
            'name',
            'status',
            'is_checked',
        )

    def get_is_checked(self, language):
        localization = self.context['localization_list']
        return localization.filter(language=language).exists()

    def get_status(self, language):
        localization = self.context['localization_list']
        local = localization.filter(language=language).first()
        status = getattr(local, 'status', 'RECEIVE')
        return status if status else 'RECEIVE'

class EpisodeSerializer(serializers.ModelSerializer):
    episode_id = serializers.CharField(source="id")
    episode_name = serializers.CharField(source="name")

    class Meta:
        model = Episode
        fields = (
            'episode_id',
            'episode_name',
            'pac_no',
            'duration'
        )


class CastMiniSerializer(serializers.ModelSerializer):
    role = RoleMiniSerializer(read_only=True, many=False)
    people = PeopleMiniSerializer(read_only=True, many=False)

    class Meta:
        model = Cast
        fields = (
            'role_id',
            'role',
            'people',
        )

class SubGenreTitleSerializer(serializers.ModelSerializer):
    id = serializers.CharField(source='sub_genre.id', default="")
    name = serializers.CharField(source='sub_genre.name', default="")
    class Meta:
        model = SubGenre
        fields = (
            'id',
            'name'
        )