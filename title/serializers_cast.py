from rest_framework import exceptions, serializers

from language.models import Language
from people.models import People
from role.models import Role


class TitleCastRoleNameFromSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=50)
    language = serializers.CharField(max_length=3, required=True)

    def validate_language(self, language):
        if Language.objects.filter(code=language).exists():
            return language
        raise exceptions.ValidationError('{} not found.'.format(language))


class TitleCasePeopleFromSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=15, required=True)
    name = serializers.CharField(max_length=50, required=False, allow_null=True, allow_blank=True)

    def validate_id(self, pk):
        if People.objects.filter(pk=pk).exists():
            return pk
        raise exceptions.ValidationError('{} is not found'.format(pk))


class TitleCastFromSerializer(serializers.Serializer):
    id = serializers.CharField(required=True, max_length=15)
    name = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    people_list = serializers.ListField(
        child=TitleCasePeopleFromSerializer(),
        allow_null=True
    )

    def validate_id(self, pk):
        if Role.objects.filter(pk=pk).exists():
            return pk
        raise exceptions.ValidationError('role `{}` nof found'.format(pk))
