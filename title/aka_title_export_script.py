import csv
from title.models import AKA, Title
from episode.models import AKAEpisode, Episode
from language.models import Language


class AkaNameExport(object):
    def __init__(self, _title_id= None,_fields_list=[]):
        self.title_id = _title_id
        self.fields_list = _fields_list
        self.title = self._find_title_obj()
        self.aka_list = self._find_language_obj()

    def _get_data(self):
        results = []
        aka_title_all = AKA.objects.all()
        for aka_title in aka_title_all:
            data = dict()
            data['title_id'] = aka_title.title.id
            data['title_name'] = aka_title.title.name
            data['title_synopsis'] = aka_title.title.synopsis
            data['language'] = aka_title.language.code
            episode = dict()

            results.append(data)
        return results

    def _prepare_ep_data(self, ep):
        results = list()
        return results

    def _check_langua_is_active(self, lang):
        return True

    def _find_ep_objects(self):

        return None

    def _get_episode(self):
        episode_list = Episode.objects.get(title=self.title)
        ep_id = []
        ep_name = []
        ep_sysnopsis = []
        for ep in episode_list:
            ep_id.append(ep.id)
            ep_name.append(ep.name)
        self.ep_id = ep_id
        self.ep_name = ep_name
        self.ep_sysnopsis = ep_sysnopsis

    def export_csv(self):
        import pandas as pd
        self._get_episode()
        cars = {'ID': [ self.title_id ],
                'AKA Title Name': [],
                'AKA Title Sysnopsis': [],
                'Title Name': [self.title.name],
                'Episode ID': self.ep_id,
                'Episode Name': self.ep_name,
                'Sysnopsis': self.ep_sysnopsis,
                'AKA Episode Name': [],
                'AKA Episode Sysnopsis': []
                }

        df = pd.DataFrame(cars, columns=['ID', 'AKA Title Name','AKA Title Sysnopsis', 'Title Name', 'Episode ID', 'Episode Name', 'Sysnopsis', 'AKA Episode Name','AKA Episode Sysnopsis'])
        df.to_csv('aka_title.csv', sep='\t')
        return 0

    def _create_csv_file(self, data):
        return

    def _find_title_obj(self):
        try:
           return Title.objects.get(id=self.title_id)
        except:
            return None

    def _find_language_obj(self):
        try:
            return AKA.objects.get(title=self.title)
        except:
            return None

    def prepare_data_from_db(self):
        return


class AkaNameImport(object):
    def __init__(self, _title_id= None, _fields_list=[]):
        self.title_id = _title_id
        self.fields_list = _fields_list

    def _find_language_list(self):
        language_list = []
        aka_list = AKA.objects.filter(title_id=self.title_id)
        for aka in aka_list:
            language_list.append(aka.language.code)
        return language_list

    def _find_title_obj(self):
        try:
           return Title.objects.get(id=self.title_id)
        except:
            return None

    def _find_ep_objects(self, ep_id):
        try:
            return Episode.object.get(id=ep_id)
        except:
            return None

    def _split_name(self, item_original):
        result = dict()
        item_original_array = item_original.split(",,")
        for item in item_original_array:
            data = item.split("::")
            lang = data[0]
            value = data[1]
            result[lang] = value
        return result

    def read_csv(self):
        try:
            with open('fixture/aka_title.csv') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                for row in csv_reader:
                    if line_count == 0:
                        line_count += 1
                    else:
                        title = self._find_title_obj(row[0])
                        aka_name_dict = self._split_name(row[1])
                        aka_synopsis_dict = self._split_name(row[2])
                        aka_item_list = AKA.objects.filter(title=title)
                        for aka_item in aka_item_list:
                            lang_code = aka_item.language.code
                            aka_item.name = aka_name_dict[lang_code]
                            aka_item.synopsis = aka_synopsis_dict[lang_code]
                            aka_item.save()

                        ep = self._find_ep_objects(row[4])
                        aka_ep_list = AKAEpisode.objects.filter(episode=ep)
                        aka_name_dict = self._split_name(row[7])
                        aka_synopsis_dict = self._split_name(row[8])
                        for aka_ep in aka_ep_list:
                            lang_code = aka_ep.language.code
                            aka_ep.name = aka_name_dict[lang_code]
                            aka_ep.synopsis = aka_synopsis_dict[lang_code]
                            aka_ep.save()
                        line_count += 1
        except:
            line_count = 0
        return line_count


def views_export_aka_csv():
    from django.conf import settings
    import os
    from wsgiref.util import FileWrapper
    from django.http import HttpResponse
    path = os.path.join('%s/title/fixture/aka_title.csv' % settings.BASE_DIR)
    wrapper = FileWrapper(open(path, 'rb'))
    response = HttpResponse(wrapper, content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename=title_format_import.csv'
    return response
