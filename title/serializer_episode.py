from rest_framework import exceptions, serializers

from episode.models import Episode


class TitleEpisodeFromSerializer(serializers.Serializer):
    episode_name = serializers.CharField(required=True, max_length=50)
    episode_id = serializers.CharField(required=False, max_length=15)
    pac_no = serializers.IntegerField(required=False)
    duration = serializers.CharField(required=False, max_length=6)
    datetime_expect_receive = serializers.DateTimeField(required=False, allow_null=True, default=None)

    def validate_episode_id(self, episode_id):
        if Episode.objects.filter(id=episode_id).exists():
            return episode_id
        raise exceptions.ValidationError('{} not found.'.format(episode_id))

    def validate_name(self, name):
        view = self.context['view']
        episode_list = Episode.objects.all()
        if view.action in ['update', 'partial_update']:
            episode_list = episode_list.filter(title=view.get_object())
        if episode_list.filter(name=name).exists():
            raise exceptions.ValidationError('%s is duplicate' % name)
        return name.upper()
