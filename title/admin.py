from django.contrib import admin

from .models import Title, AKA, Cast, Localization, Cost, SubGenre


@admin.register(Title)
class TitleModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


@admin.register(AKA)
class AKAModelAdmin(admin.ModelAdmin):
    list_display = ('title_id', 'name')


@admin.register(Cast)
class CastModelAdmin(admin.ModelAdmin):
    list_display = ('title_id', 'role_id', 'people_id')


@admin.register(Localization)
class Localization(admin.ModelAdmin):
    list_display = ('type', 'title_id', 'language_id')


@admin.register(Cost)
class CostModelAdmin(admin.ModelAdmin):
    list_display = ('cost_id', 'episode_cost', 'unit_cost', 'title_id')

@admin.register(SubGenre)
class SubGeneModelAdmin(admin.ModelAdmin):
    list_display = ('title_id', 'sub_genre_id', )