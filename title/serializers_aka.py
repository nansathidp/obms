from rest_framework import exceptions, serializers

from language.models import Language
from title.models import Title
from utils.rest_framework.serializer import CSVBase64File


class TitleAKAFromSerializer(serializers.Serializer):
    language = serializers.CharField(required=True, max_length=5)
    name = serializers.CharField(required=True, max_length=100)
    synopsis = serializers.CharField(required=False,  allow_blank=True, allow_null=True, default=None, max_length=2000)

    def validate_language(self, language):
        if Language.objects.filter(code=language, is_aka='Y').exists():
            return language
        raise exceptions.ValidationError('language `{}` is not found.'.format(language))


class TitleAKAExportSerializer(serializers.Serializer):
    title_list = serializers.ListField(
        child=serializers.CharField(max_length=15)
    )

    def validate_title_list(self, title_list):
        queryset = Title.objects.filter(id__in=title_list)
        if queryset.count() == len(title_list):
            return title_list
        raise exceptions.ValidationError()




class TitleAKAImportSerializer(serializers.Serializer):
    tmp_file = CSVBase64File()
