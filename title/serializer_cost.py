from rest_framework import serializers, exceptions


class TitleCostFromSerializer(serializers.Serializer):
    cost_id = serializers.CharField(required=False, default=None)
    episode_cost = serializers.IntegerField(required=True)
    unit_cost = serializers.IntegerField(required=True)
    total_cost = serializers.IntegerField(required=True)
    cost = None

    def validate(self, attrs):
        name = attrs.get('cost_name', None)
        pk = attrs.get('cost_id', None)
        if self.is_cost(id=pk) or self.is_cost(name=name):
            return super().validate(attrs)
        else:
            raise exceptions.ValidationError('Validate cost_id {cost_id}'.format(**attrs))

    def is_cost(self, **kwargs):
        from cost.models import Cost
        items = Cost.objects.filter(**kwargs)
        if items.exists():
            self.cost = items.first()
            self.context.update({'cost': self.cost})
        return items.exists()
