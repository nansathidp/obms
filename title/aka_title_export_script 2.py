import csv
from title.models import AKA, Title
from episode.models import AKAEpisode, Episode
from language.models import Language


class AkaNameExport(object):
    def __init__(self, _title_id= None):
        self.title_id = _title_id
        self.title = self._find_title_obj()
        self.aka_list = self._find_language_obj()

    def _get_data(self):
        results = []
        aka_title_all = AKA.objects.all()
        for aka_title in aka_title_all:
            data = dict()
            data['title_id'] = aka_title.title.id
            data['title_name'] = aka_title.title.name
            data['title_synopsis'] = aka_title.title.synopsis
            data['language'] = aka_title.language.code
            episode = dict()

            results.append(data)
        return results

    def _prepare_ep_data(self, ep):
        results = list()
        return results

    def _check_langua_is_active(self, lang):
        return True

    def export_csv(self):
        csv.DictWriter(file, 'sheet.csv')
        data_export = self._get_data()
        self._create_csv_file(data_export)
        with open('sheet.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    title = self._find_title_obj(row[0])
                    aka_item = AKA.objects.filter(title=title)[0]
                    aka_item.name = row[1]
                    aka_item.synopsis = row[2]
                    aka_item.save()
                    ep = self._find_ep_objects(row[4])
                    aka_ep = AKAEpisode.objects.filter(episode=ep)
                    aka_ep.name = row[7]
                    aka_ep.synopsis = row[8]
                    aka_ep.save()
                    line_count += 1
            print('Processed {line_count} lines.')
        return

    def _create_csv_file(self, data):
        return

    def _find_title_obj(self):
        try:
           return Title.objects.get(id=self.title_id)
        except:
            return None

    def _find_language_obj(self):
        try:
            return AKA.objects.get(title=self.title)
        except:
            return None

    def prepare_data_from_db(self):
        return


class AkaNameImport(object):
    def __init__(self, _title_id= None):
        self.title_id = _title_id

    def _find_title_obj(self):
        try:
           return Title.objects.get(id=self.title_id)
        except:
            return None

    def _find_ep_objects(self, ep_id):
        try:
            return Episode.object.get(id=ep_id)
        except:
            return None

    def read_csv(self):
        with open('sheet.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    title = self._find_title_obj(row[0])
                    aka_item = AKA.objects.filter(title=title)[0]
                    aka_item.name = row[1]
                    aka_item.synopsis = row[2]
                    aka_item.save()
                    ep = self._find_ep_objects(row[4])
                    aka_ep = AKAEpisode.objects.filter(episode=ep)
                    aka_ep.name = row[7]
                    aka_ep.synopsis = row[8]
                    aka_ep.save()
                    line_count += 1
            print('Processed {line_count} lines.')


def views_export_aka_csv():
    return ''
