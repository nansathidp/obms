from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import TitleView
from .views_aka_manage import AkaExportView, AkaImportView

router = DefaultRouter()

router.register(r'akaimport', AkaImportView)
router.register(r'akaexport', AkaExportView)
router.register(r'', TitleView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
