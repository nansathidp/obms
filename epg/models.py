from django.db import models


class Schedule(models.Model):
    class Meta:
        ordering = ['datetime_start']
        managed = False
        db_table = "SCH_BMS"
        unique_together = (('bms_code', 'schedule_date', 'schedule_time', 'episode_id'),)
        default_permissions = ('view', )

    datetime_start = models.DateTimeField(db_column="START_DATE")
    datetime_end = models.DateTimeField(db_column="END_DATE")
    bms_code = models.CharField(max_length=10, db_column="CHANNEL_CODE")
    schedule_date = models.CharField(max_length=10, db_column="SCHEDULE_DATE", primary_key=True)
    schedule_time = models.CharField(max_length=8, db_column="SCHEDULE_TIME")
    title_eng = models.CharField(max_length=100, db_column="TITLE_NAME_ENG")
    title_tha = models.CharField(max_length=100, db_column="TITLE_NAME_THAI")
    episode_name = models.CharField(max_length=100, db_column="EPISODE_NAME")
    title_id = models.CharField(max_length=12, db_column="TITLE_ID")
    episode_id = models.CharField(max_length=12, db_column="EPISODE_ID")
    end_time = models.CharField(max_length=8, db_column="END_TIME")
    telecast_type = models.CharField(max_length=20, db_column="TELECAST_TYPE")
    title_type = models.CharField(max_length=20, db_column="TITLE_TYPE")
    synopsis_eng = models.CharField(max_length=4000, db_column="SYNOPSIS_ENG")
    synopsis_tha = models.CharField(max_length=4000, db_column="SYNOPSIS_THA")
    digest_eng = models.CharField(max_length=4000, db_column="DIGEST_ENG")
    digest_tha = models.CharField(max_length=4000, db_column="DIGEST_THA")
    is_run_first = models.CharField(max_length=1, db_column="FIRST_RUN_FLAG")
    home_team_id = models.CharField(max_length=12, db_column="HOME_TEAM_ID")
    home_pvr_code = models.CharField(max_length=30, db_column="HOME_PVR_CODE")
    home_team_name = models.CharField(max_length=100, db_column="HOME_TEAM_NAME")
    away_team_id = models.CharField(max_length=12, db_column="AWAY_TEAM_ID")
    away_pvr_code = models.CharField(max_length=30, db_column="AWAY_PVR_CODE")
    away_team_name = models.CharField(max_length=100, db_column="AWAY_TEAM_NAME")
    digital_code = models.CharField(max_length=50, db_column="DIGITAL_CODE")
    pac_eps_no = models.IntegerField(db_column="PAC_EPS_NO")
    category_group_code = models.CharField(max_length=10, db_column="CATEGORY_GROUP_CODE")



