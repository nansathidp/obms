from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import exceptions, mixins, viewsets
from rest_framework.filters import SearchFilter

from .filters import EpgFilter
from .models import Schedule
from .serializers import EpgSerializer


class EpgScheduleView(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = Schedule.objects.all()
    serializer_class = EpgSerializer
    pagination_class = None
    filter_class = EpgFilter
    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
    )
    search_fields = (
        'datetime_start',
        'datetime_end',
        'episode_id',
        'episode_name',
        'title_id',
        'title_eng',
        'title_tha',
    )

    def filter_queryset(self, queryset):
        if self.request.query_params.get('bms_code') or self.request.query_params.get('search'):
            return super().filter_queryset(queryset)
        raise exceptions.ValidationError('required `bms_code` or `search` ')




