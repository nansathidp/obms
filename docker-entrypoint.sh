#!/usr/bin/env bash

pre_start() {
python scripts/create_admin.py
}

make_path() {
  mkdir -p /app/title/fixture_ignore/
  mkdir -p /app/episode/fixture_ignore/
  echo "init path fixture_ignore success"
  chmod 777 /app/title/fixture_ignore/ -R
  chmod 777 /app/episode/fixture_ignore/ -R
  echo "grant permission path fixture_ignore success"
}

start_server () {
  python manage.py runserver 0.0.0.0:8000
}

start_woker() {
  celery -A bms worker -l info
}


make_path
pre_start

is_build=$?
if [ $is_build -eq 0 ]; then
 if [ "$1" != "worker" ]; then
  start_server
else
  start_woker
fi
else
  echo error
fi


