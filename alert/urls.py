from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import AlertView

router = DefaultRouter()
router.register(r'', AlertView)

urlpatterns = [
    url(r'^', include(router.urls)),
]