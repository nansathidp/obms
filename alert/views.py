import os
from wsgiref.util import FileWrapper

from django.http import HttpResponse
from rest_framework import exceptions, mixins, viewsets
from rest_framework.decorators import action

from .models import Alert
from .serializers import AlertSerializer


class AlertView(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    queryset = Alert.objects.all()
    serializer_class = AlertSerializer

    @action(methods=['GET'], detail=True)
    def download(self,  request, *args, **kwargs):
        instance = self.get_object()
        result = getattr(instance, 'results', {})
        if result:
            if 'is_file' in result and 'path_file' in result:
                if not os.path.exists(result['path_file']):
                    raise exceptions.ValidationError('Path file is not found.')
                path = os.path.join(result['path_file'])
                wrapper = FileWrapper(open(path, 'rb'))
                response = HttpResponse(wrapper, content_type="text/csv")
                response['Content-Disposition'] = 'attachment; filename=title_aka_import.csv'
                return response
        raise exceptions.NotFound('file not found.')


