from rest_framework import serializers

from ..models import Cost


class CostSerializer(serializers.ModelSerializer):
    account_created = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)

    class Meta:
        model = Cost
        fields = (            
            'name',            
            'account_created'            
        )

    def validate_account_created(self, account):
        return self.context['request'].user

class CostListSerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)

    class Meta:
        model = Cost
        fields = (
            'id',
            'name',            
            'datetime_created',
            'account_created',
            'create_by',            
        )
