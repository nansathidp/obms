from django_filters.rest_framework import FilterSet
from django_filters import filters

from ..models import Cost


class CostFilter(FilterSet):
    cost_name = filters.CharFilter(
        method='get_cost_name',
        field_name='cost_name'
    )

    class Meta:
        model = Cost
        fields = (
            'id',
            'name'            
        )

    def get_cost_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
