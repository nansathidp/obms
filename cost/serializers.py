from rest_framework import serializers

from .models import Cost


class CostListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cost
        fields = (
            'id',
            'name',
        )
