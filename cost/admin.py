from django.contrib import admin

from .models import Cost


@admin.register(Cost)
class CostModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
