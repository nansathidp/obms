COST_ITEM = {
    'name': 'subtitle',
    'id': 'ST2020020100001',
}


def init_cost():
    from .models import Cost
    return Cost.objects.get_or_create(**COST_ITEM)


def remove_cost():
    from .models import Cost
    return Cost.objects.filter(**COST_ITEM).delete()
