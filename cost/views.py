from rest_framework import viewsets

from .models import Cost
from .serializers import CostListSerializer


class CostView(viewsets.ReadOnlyModelViewSet):
    queryset = Cost.objects.all()
    serializer_class = CostListSerializer
    pagination_class = None

