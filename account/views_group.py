from django.contrib.auth.models import Group
from rest_framework import mixins, viewsets

from .models import Account
from .serializers_group import GroupPermissionSerializer


class GroupView(viewsets.ReadOnlyModelViewSet):
    queryset = Group.objects.all().order_by('id')
    serializer_class = GroupPermissionSerializer
    pagination_class = None

    def filter_queryset(self, queryset):
        account = self.request.user
        return account.groups.all()
        # queryset = super().filter_queryset(queryset)



