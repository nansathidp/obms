from django.contrib.auth import login
from rest_framework import exceptions, mixins, viewsets
from rest_framework import permissions, status
from rest_framework.response import Response

from utils.google import GoogleAuthenticate
from .cacheds import set_account, set_account_by_token, set_account_permission, set_token_info, set_token_by_account_id
from .models import Account, Token
from .serializers_token import GoogleTokenSerializer, TokenAccountSerializer


class GoogleAuthenticateView(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    pagination_class = None
    serializer_class = GoogleTokenSerializer
    queryset = Token.objects.none()
    permission_classes = [
        permissions.AllowAny,
    ]
    google = GoogleAuthenticate()

    def list(self, request, *args, **kwargs):
        response = {
            'redirect_uri': self.google.get_url_authenticate(),
            'response_type': self.google.get_response_type()
        }
        return Response(response, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = Account.objects.get(email=serializer.data.get('email'))
        if not user:
            raise exceptions.AuthenticationFailed()
        login(request, user)
        token = Token.objects.filter(account=user).first()
        if not token:
            token = Token.objects.create(account=user)
        headers = self.get_success_headers(serializer.data)
        set_account_permission(user.id, user)
        set_account(user.id, user)
        set_account_by_token(token.key, user)
        set_token_info(token.key, token)
        set_token_by_account_id(user.id, token)
        serializer_response = TokenAccountSerializer(instance=token)

        return Response(serializer_response.data, status=status.HTTP_201_CREATED, headers=headers)
