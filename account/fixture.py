from account.models import Account

ACCOUNT = {
    'employee_id': "000000",
    "email": "admin@local.com",
    "password": "password123"
}


def create_user():
    return Account.objects.filter(is_superuser=False).first()


def create_supper_admin():
    return Account.objects.filter(is_superuser=True).first()
