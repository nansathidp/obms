from django.conf.urls import include, url
from django.urls import path
from rest_framework.routers import DefaultRouter
from utils.rest_framework.routers import ListDetailRouter

from .views_google_authenticate import GoogleAuthenticateView
from .views_group import GroupView
from .views_logout import LogoutView
from .views_profile import ProfileView
from .view_permssion import PermissionView
router = DefaultRouter()
router_permission = ListDetailRouter()
router_permission.register(r'', PermissionView)
router.register(r'group', GroupView)
router.register(r'profile', ProfileView)
router.register(r'authenticate/google', GoogleAuthenticateView)

urlpatterns = [
    path('<str:permission_code>/allow', include(router_permission.urls)),
    path('logout/', LogoutView.as_view()),
    url(r'^', include(router.urls)),
]
