from django.conf import settings
from django.contrib.auth.models import Permission
from rest_framework import mixins, viewsets
from rest_framework.exceptions import NotFound

from .serializers_permission import PermissionSerializer


class PermissionView(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = PermissionSerializer
    queryset = Permission.objects.all()
    pagination_class = None

    def __init__(self, *args, **kwargs):
        self.permission_code = None
        self.account = None
        super().__init__(*args, **kwargs)


    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)
        self.permission_code = kwargs.get('permission_code', None)
        self.account = request.user

    def filter_queryset(self, queryset):
        if self.permission_code and self.account:
            if 'app' in self.permission_code:
                content_type = settings.CONTENT_TYPE(self.permission_code.replace("app.", ""))
                if not content_type:
                    raise NotFound("Application permission code app  %s is not support" % self.permission_code)
                return self.account.user_permissions.filter(content_type=content_type)
            elif 'group' in self.permission_code:
                group_name = self.permission_code.replace('group.', '')
                groups = self.account.groups.filter(name=group_name)
                if groups.exists():
                    group = groups.first()
                    return group.permissions.all()
                raise NotFound("Group permission code app  %s is not support" % self.permission_code)

            else:
                raise NotFound("Permission code %s is not support" % self.permission_code)
        else:
            raise NotFound("Permission code %s is not support" % self.permission_code)

