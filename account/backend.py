from django.contrib.auth.backends import ModelBackend
from rest_framework import exceptions

from account.models import Account


class NonePasswordAuthenticate(ModelBackend):

    def authenticate(self, request, username=None, password=None, **kwargs):
        # if username is None:
            # username = kwargs.get(UserModel.USERNAME_FIELD)
        # if username is None or password is None:
        #     return
        try:
            user = Account.objects.get_by_natural_key(username)
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            UserModel().set_password(password)
        else:
            if self.user_can_authenticate(user):
               return user
