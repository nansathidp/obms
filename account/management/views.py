from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import exceptions, filters, status, viewsets
from rest_framework.response import Response

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .filters import AccountFilter
from .serializers import AccountListSerializer, AccountSerializer
from ..models import Account


class AccountView(viewsets.ModelViewSet):
    queryset = Account.objects.filter(is_active=True)
    serializer_class = AccountListSerializer
    app = 'account'
    model = 'account'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    action_serializers = {
        'list': AccountListSerializer,
        'create': AccountSerializer,
        'update': AccountSerializer,
        'partial_update': AccountSerializer,
        'destroy': AccountListSerializer,
    }

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    filter_class = AccountFilter

    search_fields = (
        'employee_id',
        'first_name',
        'last_name'
    )

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):        
        request.data['password'] = 'password123'
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        account = self.perform_create(serializer)
        serializer_response = AccountListSerializer(account)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):        
        partial = kwargs.pop('partial', False)
        instance = self.get_object()        
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        serializer_response = AccountListSerializer(instance)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()

    def perform_destroy(self, instance):
        try:
            instance.is_active = False
            instance.save()
        except Exception as err:
            raise exceptions.ValidationError()