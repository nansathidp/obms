from django_filters.rest_framework import FilterSet
from django_filters import filters

from account.models import Account


class AccountFilter(FilterSet):
    account_employee_id = filters.CharFilter(
        method='get_account_employee_id',
        field_name='account_employee_id'
    )

    account_first_name = filters.CharFilter(
        method='get_account_first_name',
        field_name='account_first_name'
    )

    account_last_name = filters.CharFilter(
        method='get_account_last_name',
        field_name='account_last_name'
    )

    account_email = filters.CharFilter(
        method='get_account_email',
        field_name='account_email'
    )

    class Meta:
        model = Account
        fields = (
            'employee_id',
            'first_name',
            'last_name',
            'email'      
        )

    def get_account_employee_id(self, queryset, employee_id, value):
        return queryset.filter(vendor__employee_id=value)

    def get_account_first_name(self, queryset, first_name, value):
        return queryset.filter(vendor__first_name=value)

    def get_account_last_name(self, queryset, last_name, value):
        return queryset.filter(vendor__last_name=value)

    def get_account_email(self, queryset, email, value):
        return queryset.filter(vendor__email=value)
