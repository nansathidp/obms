from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from rest_framework import exceptions, serializers
from django.conf import settings
from utils.rest_framework.serializer import ContentSerializer

from account.models import Account

class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = (
            'id',
            'app_label',
            'model',
        )

class PermissionSerializer(serializers.ModelSerializer):
    content_info = serializers.SerializerMethodField(read_only=True)
    content_type_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Permission
        fields = (
            'id',
            'codename',
            'content_info',
            'content_type_id',
        )

    def validate_content_type_id(self, content_type_id):        
        if settings.CONTENT_TYPE_ID(content_type_id):
            return content_type_id
        raise exceptions.ValidationError()

    def get_content_info(self, permission):
        return ContentTypeSerializer(permission.content_type).data

class AccountSerializer(ContentSerializer, serializers.Serializer):
    email = serializers.CharField(required=True, max_length=255)
    password = serializers.CharField(required=False, max_length=50)
    employee_id = serializers.CharField(required=True, max_length=120)
    first_name = serializers.CharField(required=True, max_length=120)
    last_name = serializers.CharField(required=True, max_length=120)
    form = serializers.ListField(child=PermissionSerializer())

    class Meta:
        model = Account

    fields_map = {
        'email': 'email',
        'password': 'password',
        'employee_id': 'employee_id',
        'first_name': 'first_name',
        'last_name': 'last_name'
    }                    

    def create_relation(self, instance, validated_data, **kwargs):
        self.create_permission(validated_data, instance)

    def create_permission(self,  validated_data, account):
        forms = validated_data.get('form', [])
        if forms:
            transaction.set_autocommit(False)
            try:
                user_permission_ids = []
                for form_permission in forms:
                    data = account.user_permissions.filter(content_type_id=form_permission['content_type_id'],
                                                                 codename=form_permission['codename']).first()                    
                    if data:
                        user_permission_ids.append(data.id)                        
                        continue
                    permission = Permission.objects.filter(content_type_id=form_permission['content_type_id'], codename=form_permission['codename']).first()                                   
                    if permission:                        
                        user_permission_ids.append(permission.id)
                        account.user_permissions.add(permission)
                account.user_permissions.exclude(id__in=user_permission_ids).delete()
                transaction.commit()
            except Exception as err:
                transaction.rollback()
                print(err)
        return True

class AccountListSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Account
        fields = (
            "id",
            "email",
            "employee_id",
            "first_name",
            "last_name",
            
        )
