from django.contrib.auth.models import Permission
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .serializers_permission import PermissionListSerializer

from account.models import Account
import json, itertools
from operator import itemgetter

class PermissionView(viewsets.ModelViewSet):
    queryset = Permission.objects.all()
    serializer_class = PermissionListSerializer
    pagination_class = None

    app = 'account'
    model = 'account'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    action_serializers = {
        'list': PermissionListSerializer,
        # 'create': PermissionCreateSerializer,
        # 'update': PermissionCreateSerializer,
        # 'partial_update': PermissionCreateSerializer,
        # 'destroy': PermissionSerializer,
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):        
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)        
        return Response(True, status=status.HTTP_201_CREATED)

    def list(self, request, *args, **kwargs):        
        permission = PermissionListSerializer(Permission.objects.all(), many=True)
        data = json.loads(json.dumps([permission.data]))[0]  
        col = []
        result = []        
        for row in data: 
            codename = self.getcolumn(row['codename'])
            if codename not in col:
                col.append(codename)
        
        for key, value in itertools.groupby(data, key=itemgetter('content_type_id')):
            row = {}      
            column = col.copy()          
            for rows in value:   
                row['model'] = rows['content_info']['model']
                row['app_label'] = rows['content_info']['app_label']
                row['content_type_id'] = rows['content_type_id']
                row['selected'] = False
                codename = self.getcolumn(rows['codename'])
                if codename in column:
                    row[codename] = False
                    row['status_' + codename] = True
                    column.remove(codename)

            for rows in column:
                row[rows] = False
                row['status_' + rows] = False

            result.append(row)        
        data = {'data': result, 'column': col}            
        return Response(data, status=status.HTTP_201_CREATED)   
    
    def getcolumn(self, codename):
        if len(codename.split('_')) > 2:
            return codename.split('_')[1]        
        return codename.split('_')[0]

    @action(methods=['GET'], detail=False, url_path='(?P<account_id>\d+)')
    def getdata(self, request, account_id):        
        account = Account.pull(account_id)
        permission = PermissionListSerializer(Permission.objects.all(), many=True)
        data = json.loads(json.dumps([permission.data]))[0]  
        col = []
        result = []
        for row in data: 
            codename = self.getcolumn(row['codename'])                       
            if codename not in col:
                col.append(codename)
        
        for key, value in itertools.groupby(data, key=itemgetter('content_type_id')):
            row = {}      
            column = col.copy()
            selected = 0       
            for rows in value:   
                row['model'] = rows['content_info']['model']
                row['app_label'] = rows['content_info']['app_label']
                row['content_type_id'] = rows['content_type_id']                
                codename = self.getcolumn(rows['codename'])
                if codename in column:                    
                    check = account.user_permissions.filter(content_type_id=rows['content_type_id'],codename=rows['codename']).first()
                    if check:
                        row[codename] = True
                        selected += 1
                    else:
                        row[codename] = False

                    row['status_' + codename] = True
                    column.remove(codename)

            if (len(col) - len(column)) == selected:
                row['selected'] = True
            else:
                row['selected'] = False

            for rows in column:
                row[rows] = False
                row['status_' + rows] = False

            result.append(row)        
        data = {'data': result, 'column': col}            
        return Response(data, status=status.HTTP_201_CREATED)   
        
