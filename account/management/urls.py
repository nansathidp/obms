from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from utils.rest_framework.routers import ListDetailRouter
from .views import AccountView
from .views_permission import PermissionView

router = DefaultRouter()
router.register(r'', AccountView)

router_permission = ListDetailRouter()
router_permission.register(r'', PermissionView)

app_name = 'account'

urlpatterns = [
    path('permission/', include(router_permission.urls)),
    path(r'', include(router.urls)),
]
