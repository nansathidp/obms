from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from rest_framework import serializers

from account.models import Account


class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = (
            'id',
            'app_label',
            'model',
        )

class PermissionListSerializer(serializers.ModelSerializer):
    content_info = serializers.SerializerMethodField()

    class Meta:
        model = Permission
        fields = (
            'id',
            'codename',
            'content_type_id',
            'content_info'
        )

    def get_content_info(self, permission):
        return ContentTypeSerializer(permission.content_type).data
        