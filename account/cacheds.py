from django.core.cache import cache

expire_time = 60 * 60 * 24

prefix_account = 'account_account_'
prefix_permission  = 'account_permission_'


def get_account(id):
    key = '%s_%s' % (prefix_account, id)
    return cache.get(key)


def set_account(id, account):
    key = '%s_%s' % (prefix_account, id)
    cache.set(key, account, expire_time)
    return account

def get_account_by_token(token):
    return cache.get(token)


def set_account_by_token(token, account):
    cache.set(token, account, expire_time)
    cache.set('account_token_%s' % account.id, token)
    return account

def set_token_by_account_id(id, token):
    cache.set('account_token_%s' %id, token)

def get_token_by_account_id(id):
    return cache.get('account_token_%s' % id)

def set_account_permission(id, account):
    key = '%s_%s' % (prefix_permission, id)
    cache.set(key, list(account.get_all_permissions()), expire_time)
    return account

def get_account_permission(id):
    key = '%s_%s' % (prefix_permission, id)
    return cache.get(key)

def set_token_info(key_token, token):
    key = 'token_%s' % key_token
    cache.set(key, token, expire_time)

def get_token_info(key_token):
    key = 'token_%s' % key_token
    return cache.get(key)


def delete_account(id):
    token = get_token_by_account_id(id)
    cache.delete(key='token_%s' % token.key)
    cache.delete(token.key)
    cache.delete(key='%s_%s' % (prefix_permission, id))
    cache.delete(key='%s_%s' % (prefix_account, id))
    cache.delete('account_token_%s' % id)
