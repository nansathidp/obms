from rest_framework import serializers

from .cacheds import get_account_permission
from .models import Account


class AccountSerializer(serializers.ModelSerializer):
    has_permission = serializers.SerializerMethodField()
    class Meta:
        model = Account
        fields = (
            "id",
            "email",
            "employee_id",
            "first_name",
            "last_name",
            'has_permission'
        )


    def get_has_permission(self, account):
        permission = get_account_permission(account.id)
        return permission if permission else []

