from django.contrib import admin

from .models import Account, Token


@admin.register(Account)
class AccountModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'email')
    actions = [
        'grant_private_permission',
        'grant_publish_permission',
        'grant_master_permission',
        'grant_business_permission',
        'grant_system_management',
        'grant_aka_permission',
        'drop_private_permission',
        'drop_publish_permission',
        'drop_master_permission',
        'drop_business_permission',
    ]

    def grant_private_permission(self, request, queryset):
        for user in queryset:
            user.grate_permission_private()

    def grant_publish_permission(self, request, queryset):
        for user in queryset:
            user.grant_permission_publish()

    def grant_master_permission(self, request, queryset):
        for user in queryset:
            user.grant_permission_master()

    def grant_business_permission(self, request, queryset):
        for user in queryset:
            user.grant_permission_business()

    def grant_system_management(self, request, queryset):
        for user in queryset:
            user.grant_management_permission()

    def grant_aka_permission(self, request, queryset):
        for user in queryset:
            user.grant_aka_permission()

    def drop_private_permission(self, request, queryset):
        for user in queryset:
            user.drop_permission_private()

    def drop_publish_permission(self, request, queryset):
        for user in queryset:
            user.drop_permission_publish()

    def drop_master_permission(self, request, queryset):
        for user in queryset:
            user.drop_permission_master()

    def drop_business_permission(self, request, queryset):
        for user in queryset:
            user.drop_permission_business()


@admin.register(Token)
class TokenModelAdmin(admin.ModelAdmin):
    list_display = ('key', 'account', 'type', 'datetime_created')
