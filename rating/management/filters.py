from django_filters.rest_framework import FilterSet
from django_filters import filters

from ..models import Rating


class RatingFilter(FilterSet):
    rating_name = filters.CharFilter(
        method='get_rating_name',
        field_name='rating_name'
    )

    class Meta:
        model = Rating
        fields = (
            'id',
            'name'            
        )

    def get_rating_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
