from rest_framework import viewsets

from .models import Rating
from .serializers import RatingSerializer


class RatingView(viewsets.ReadOnlyModelViewSet):
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer
    pagination_class = None
