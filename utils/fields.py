from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone

from account.models import Account
from prefix.models import Prefix


class SaveMixin(object):
    primary_key = 'id'
    prefix = None

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        print("[created] is ", force_insert)

        if force_insert and self.prefix and self.primary_key:
            content_type = ContentType.objects.get_for_model(self)
            if not getattr(self, self.primary_key):
                account = getattr(self, 'account_created',  Account.objects.last())
                generate_key = Prefix.get_generate_id(content_type, self.prefix, account)
                setattr(self, self.primary_key, generate_key)
                print('[{}] System to generate of key.'.format(content_type.app_label, content_type.model))
            else:
                print('user_in_put_data {}'.format(getattr(self, self.primary_key)))
            if content_type:
                model_class = content_type.model_class()
                pk = getattr(self, self.primary_key)
                is_exists = model_class.objects.filter(pk=pk)
                assert not is_exists, '{}.{} pk {} is exists'.format(content_type.app_label, content_type.model, pk)

        return super().save(force_insert, force_update
                            , using, update_fields)


class ObmModel(SaveMixin, models.Model):
    account_updated = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        default=None,
        db_column="MODIFIED_BY",
        on_delete=models.SET_NULL
    )
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    datetime_updated = models.DateTimeField(null=True, auto_now=True, db_column="MODIFIED_DATE")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")

    class Meta:
        abstract = True
        ordering = ['-datetime_updated']
