import io

import PyPDF2
from django.db import transaction
from drf_extra_fields.fields import Base64FileField
from drf_yasg.codecs import logger
from rest_framework import serializers
from rest_framework.fields import SkipField, empty


class ContentSerializer(object):
    fields_map = {}


    def update(self, instance, validated_data, **kwargs):
        transaction.set_autocommit(False)
        try:
            fields = []
            for key, value in validated_data.items():
                field = self.fields_map.get(key, None)
                if field and value:
                    fields.append(field)
                    setattr(instance, field, value)
            instance.save(update_fields=fields)
            self.create_relation(instance, validated_data, **kwargs)
            transaction.commit()
            return instance
        except Exception as err:
            transaction.rollback()
            raise err

    def create(self, validated_data, **kwargs):
        transaction.set_autocommit(False)
        try:
            fields = dict()
            for key, value in validated_data.items():
                field = self.fields_map.get(key, None)
                if field and value:
                    fields.update({field: value})
            instance = self.Meta.model.objects.create(**fields)
            self.create_relation(instance, validated_data, **kwargs)
            transaction.commit()
            return instance
        except Exception as err:
            transaction.rollback()
            raise err


    def save(self, **kwargs):
        validated_data = dict(self.validated_data)
        request = self.context['request']
        account = request.user
        kwargs.update({'account': account})
        if self.instance is not None:
            validated_data.update({'account_updated': account})
            return self.update(self.instance, validated_data, **kwargs)
        else:
            validated_data.update({'account_created': account})
            return self.create(validated_data, **kwargs)


    def create_relation(self, instance, validated_data, **kwargs):
        raise NotImplementedError('`create_relation()` must be implemented.')


class PDFBase64File(Base64FileField):
    ALLOWED_TYPES = ['pdf']

    def get_file_extension(self, filename, decoded_file):
        try:
            PyPDF2.PdfFileReader(io.BytesIO(decoded_file))
        except PyPDF2.utils.PdfReadError as e:
            logger.warning(e)
        else:
            return 'pdf'


class CSVBase64File(Base64FileField):
    ALLOWED_TYPES = ['csv']

    def get_file_extension(self, filename, decoded_file):
        return 'csv'


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-restframework-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension,)

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr
        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension
        if extension is None:
            extension = 'jpg'
        return extension

    def validate_empty_values(self, data):
        if data is empty:
            if getattr(self.root, 'partial', False):
                raise SkipField()

        if self.allow_empty_file and isinstance(data, str) and len(data) == 0:
            return True, None

        if self.allow_empty_file and data is empty:
            return True, None
        elif self.allow_null and data is None:
            return True, None
        else:
            return False, data
