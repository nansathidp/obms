from django.conf import settings

class ViewPermission(object):

    def has_permission(self, request, view):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.view_{}'.format(view.app, view.model)
        group_codename = 'view_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename, content_type=content_type)

    def has_object_permission(self, request, view, obj):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.view_detail_{}'.format(view.app, view.model)
        group_codename = 'view_detail_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename,content_type=content_type)



class ChangePermission(object):

    def has_permission(self, request, view):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.change_{}'.format(view.app, view.model)
        group_codename = 'change_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename, content_type=content_type)

    def has_object_permission(self, request, view, obj):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.change_{}'.format(view.app, view.model)
        group_codename = 'change_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename, content_type=content_type)


class AddPermission(object):

    def has_permission(self, request, view):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.add_{}'.format(view.app, view.model)
        group_codename = 'add_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename,content_type=content_type)

    def has_object_permission(self, request, view, obj):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.add_{}'.format(view.app, view.model)
        group_codename = 'add_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename, content_type=content_type)


class DeletePermission(object):
    def has_permission(self, request, view):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.delete_{}'.format(view.app, view.model)
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        group_codename = 'delete_%s' % view.model
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename, content_type=content_type)

    def has_object_permission(self, request, view, obj):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.delete_{}'.format(view.app, view.model)
        group_codename = 'delete_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename,content_type=content_type)


class ExportPermission(object):

    def has_permission(self, request, view):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.export_{}'.format(view.app, view.model)
        group_codename = 'export_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename,
                                                                               content_type=content_type)

    def has_object_permission(self, request, view, obj):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.export_{}'.format(view.app, view.model)
        group_codename = 'export_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename,
                                                                               content_type=content_type)


class ImportPermission(object):

    def has_permission(self, request, view):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.import_{}'.format(view.app, view.model)
        group_codename = 'export_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename,
                                                                               content_type=content_type)

    def has_object_permission(self, request, view, obj):
        if request.user is None or not request.user.is_authenticated:
            return False
        codename = '{}.import_{}'.format(view.app, view.model)
        group_codename = 'export_%s' % view.model
        content_type = settings.CONTENT_TYPE('{}.{}'.format(view.app, view.model))
        return request.user.has_perm(codename) or request.user.hash_prem_group(codename=group_codename,
                                                                               content_type=content_type)
