def is_period_conflict(datetime_right_start, datetime_right_end, queryset): # Queryset of deal
    return queryset.filter(
        datetime_right_end__range=(
            datetime_right_start,
            datetime_right_end)
    ).exists() or queryset.filter(
        datetime_right_start__range=(
            datetime_right_start,
            datetime_right_end,
        )
    )