from reset_migrations.management.commands import reset_migrations
from django.conf import settings


class Command(reset_migrations.Command):

    def handle(self, *args, **options):
        apps = options.get('apps', None)
        print('handle')
        if apps == 'all':
            apps = settings.PUBLISH_APPS
            apps += settings.PRIVATE_APPS
        super().handle(*args, **options)
