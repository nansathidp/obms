import os

import requests
from django.conf import settings


class GoogleAuthenticate:
    def __init__(self):
        self._config = settings.GOOGLE_CONFIG
        self._token_uri = settings.GOOGLE_TOKEN_INFO_URI
        self.data = {}
        self.proxy = {}

    def get_url_authenticate(self):
        self._config['redirect_uris'] = "".join(self._config["redirect_uris"])
        return "{auth_uri}?redirect_uri={redirect_uris}&state={state}&scope={scope}&client_id={client_id}&response_type={response_type}".format(
            **self._config)

    def get_response_type(self):
        return self._config['response_type']

    def get_uri_token(self, token):
        return '{}?access_token={}'.format(self._token_uri, token)

    def is_valid_token(self, token):
        if settings.TESTING:
            from account.fixture import create_user
            account = create_user()
            self.data = account.__dict__
            return True
        else:
            session = requests.Session()
            response = session.get(
                url=self.get_uri_token(token=token),
                headers={'content-type': 'application/json'},
                proxies=self.proxy
            )
            if response.ok:
                self.data = response.json()
            return response.ok
