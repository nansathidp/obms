from django.core.cache import cache

CODE = 'LANGUAGE'


def get_cached(key):
    return cache.get('{}.{}'.format(CODE, key))


def fetch(code):
    from .models import Language
    value = Language.objects.filter(code=code).first()
    if value:
        set_cached(code, value)
    return value


def set_cached(key, value):
    cache.set('{}.{}'.format(CODE, key), value)
