from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import LanguageView

router = DefaultRouter()

router.register(r'', LanguageView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
