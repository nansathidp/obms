LANGUAGE_ITEMS = [
    {
        'code': 'tha',
        'name': 'Thai',
        'alpha2': 'th'
    },
    {
        'code': 'eng',
        'name': 'English',
        'alpha2': 'en'
    },
    {
        'code': 'kor',
        'name': 'Korea',
        'alpha2': 'ko'
    },

]


def init_language(**kwargs):
    from .models import Language
    return Language.objects.get_or_create(**kwargs)


def init_languages():
    for language in LANGUAGE_ITEMS:
        init_language(**language)
