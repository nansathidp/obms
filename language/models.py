from django.conf import settings
from django.db import models

from utils.fields import ObmModel


class Language(ObmModel):
    ACTIVE = 'Y'
    IN_ACTIVE = 'N'

    ACTIVE_CHOICES = (
        (ACTIVE, True),
        (IN_ACTIVE, False)
    )

    code = models.CharField(primary_key=True, unique=True, max_length=20, db_column='LANGUAGE_CODE')
    name = models.CharField(max_length=100, db_column="LANGUAGE_NAME")
    alpha2 = models.CharField(max_length=20, null=True, blank=True, db_column="ALPHA2")
    is_localize = models.CharField(max_length=1, choices=ACTIVE_CHOICES, default=ACTIVE, db_column="LOCALIZE_FLAG")
    is_aka = models.CharField(max_length=1, choices=ACTIVE_CHOICES, default=ACTIVE, db_column="AKA_FLAG")

    primary_key = 'code'

    class Meta:
        ordering = ['name']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_LANGUAGE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '{}-{}'.format(self.code, self.name)

    @staticmethod
    def get_by_code(code):
        return Language.objects.filter(code=code).first()
