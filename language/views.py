from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from .models import Language
from .serializers import LanguageSerializer


class LanguageView(viewsets.ReadOnlyModelViewSet):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer
    pagination_class = None
    filter_backends = (DjangoFilterBackend, )
    filter_fields = (
        'is_aka',
        'is_localize',
    )
