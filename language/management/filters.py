from django_filters import filters
from django_filters.rest_framework import FilterSet

from ..models import Language


class LanguageFilter(FilterSet):
    language_name = filters.CharFilter(
        method='get_language_name',
        field_name='language_name'
    )

    class Meta:
        model = Language
        fields = (
            'code',
            'name'            
        )

    def get_language_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
