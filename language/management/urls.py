from django.urls import include, path
from rest_framework import routers

from .views import LanguageView

router = routers.DefaultRouter()
router.register(r'', LanguageView)

app_name = 'language'
urlpatterns = [    
    path('', include(router.urls)),
]
