from django_filters import filters
from django_filters.rest_framework import FilterSet

from ..models import Type


class TypeFilter(FilterSet):
    type_name = filters.CharFilter(
        method='get_type_name',
        field_name='type_name'
    )

    class Meta:
        model = Type
        fields = (
            'id',
            'name'            
        )

    def get_rating_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
