from django.urls import include, path
from rest_framework import routers

from .views import OtherView

router = routers.DefaultRouter()
router.register(r'', OtherView)

app_name = 'type'
urlpatterns = [    
    path('', include(router.urls)),
]
