from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import OtherView

router = DefaultRouter()

router.register(r'', OtherView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
