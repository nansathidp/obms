from rest_framework import viewsets

from .models import Type
from .serializers import OtherTypeSerializer


class OtherView(viewsets.ReadOnlyModelViewSet):
    queryset = Type.objects.all()
    serializer_class = OtherTypeSerializer
    pagination_class = None