from django.conf import settings
from django.db import models

from utils.fields import SaveMixin

IN_ACTIVE = 'N'
IS_ACTIVE = 'Y'
ACTIVE_CHOICES = (
    (IN_ACTIVE, False),
    (IS_ACTIVE, True)
)


class Type(SaveMixin, models.Model):
    id = models.CharField(max_length=15, primary_key=True, db_column="VOD_OTHER_TYPE_ID")
    name = models.CharField(max_length=100, db_column="OTHER_TYPE_NAME")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    primary_key = 'id'
    prefix = "VOT"

    class Meta:
        ordering = ['name']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_VOD_OTHER_TYPE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '%s (%s)' % (self.id, self.name)
