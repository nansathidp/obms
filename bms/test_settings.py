from django.test.runner import DiscoverRunner

#
# class UnManagedModelTestRunner(DiscoverRunner):
#
#     def setup_test_environment(self, *args, **kwargs):
#         from django.apps import apps
#         get_models = apps.get_models
#         self.unmanaged_models = [m for m in get_models() if not m._meta.managed]
#         for m in self.unmanaged_models:
#             m._meta.managed = True
#         super(UnManagedModelTestRunner, self).setup_test_environment(*args, **kwargs)
#
#     def teardown_test_environment(self, *args, **kwargs):
#         super(UnManagedModelTestRunner, self).teardown_test_environment(*args, **kwargs)
#         for m in self.unmanaged_models:
#             m._meta.managed = False
#

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "obms",
        'USER': 'root',
        'HOST': '127.0.0.1',
        'PASSWORD': 'password'

    }
}
ACCOUNT_USER = {
    'employee_id': "000000",
    "email": "admin@local.com",
    "password": "password123"
}
APP_MODE = "simple"
TESTING = True

# Set Django's test runner to the custom class defined above
# TEST_RUNNER = 'bms.test_settings.UnManagedModelTestRunner'