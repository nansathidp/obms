"""bms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

urlpatterns_api = [
    path('api/account/', include('account.urls')),
    path('api/alert/', include('alert.urls')),
    path('api/amortize/', include('amortize.urls')),
    path('api/asset/', include('asset.urls')),
    path('api/channel/', include('channel.urls')),
    path('api/config/', include('config.urls')),
    path('api/cost/', include('cost.urls')),
    path('api/deal/', include('deal.urls')),
    path('api/episode/', include('episode.urls')),
    path('api/country/', include('country.urls')),
    path('api/epg/', include('epg.urls')),
    path('api/episode/', include('episode.urls')),
    path('api/genre/', include('genre.urls')),
    path('api/language/', include('language.urls')),
    path('api/other-type/', include('other_type.urls')),
    path('api/owner/', include('owner.urls')),
    path('api/people/', include('people.urls')),
    path('api/publish/', include('publish.urls')),
    path('api/rating/', include('rating.urls')),
    path('api/role/', include('role.urls')),
    path('api/studio/', include('studio.urls')),
    path('api/sub-genre/', include('sub_genre.urls')),
    path('api/title/', include('title.urls')),
    path('api/vendor/', include('vendor.urls')),
    path('api/reason/', include('reason.urls')),

]

urlpatterns_api_management = [
    path('api/management/cost/', include('cost.management.urls')),
    path('api/management/people/', include('people.management.urls')),
    path('api/management/role/', include('role.management.urls')),
    path('api/management/studio/', include('studio.management.urls')),
    path('api/management/language/', include('language.management.urls')),
    path('api/management/vendor/', include('vendor.management.urls')),
    path('api/management/rating/', include('rating.management.urls')),
    path('api/management/publish/', include('publish.management.urls')),
    path('api/management/country/', include('country.management.urls')),
    path('api/management/genre/', include('genre.management.urls')),
    path('api/management/sub-genra/', include('sub_genre.management.urls')),
    path('api/management/title-group/', include('title_group.management.urls')),
    path('api/management/other-type/', include('other_type.management.urls')),
    path('api/management/sub-genre/', include('sub_genre.management.urls')),
    path('api/management/owner/', include('owner.management.urls')),
    path('api/management/account/', include('account.management.urls')),
    path('api/management/channel/', include('channel.management.urls')),
    path('api/management/amortize/', include('amortize.management.urls')),
    path('api/management/reason/', include('reason.management.urls')),

]

schema_view = get_schema_view(
    openapi.Info(
        title="Docs API",
        default_version='v1',
        description="API Version 1",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    patterns=urlpatterns_api,
    permission_classes=(permissions.AllowAny,),
)
schema_management_view = get_schema_view(
    openapi.Info(
        title="Docs API Management",
        default_version='v1',
        description="API Version 1",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    patterns=urlpatterns_api_management,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('api/', schema_view.with_ui('swagger', cache_timeout=0)),
    path('api/management/', schema_management_view.with_ui('swagger', cache_timeout=0)),
    path('admin/', admin.site.urls),
]

urlpatterns += urlpatterns_api_management
urlpatterns += urlpatterns_api
