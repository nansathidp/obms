from django.urls import include, path
from rest_framework import routers

from .views import OwnerView

router = routers.DefaultRouter()
router.register(r'', OwnerView)

app_name = 'owner'
urlpatterns = [    
    path('', include(router.urls)),
]
