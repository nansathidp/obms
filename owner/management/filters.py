from django_filters.rest_framework import FilterSet
from django_filters import filters

from ..models import BusinessOwner


class OwnerFilter(FilterSet):
    owner_name = filters.CharFilter(
        method='get_owner_name',
        field_name='owner_name'
    )

    class Meta:
        model = BusinessOwner
        fields = (
            'id',
            'name'            
        )

    def get_owner_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
