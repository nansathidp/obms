from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import BusinessOwnerView

router = DefaultRouter()

router.register(r'', BusinessOwnerView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
