from django.conf import settings
from django.db import models

from utils.fields import ObmModel


class BusinessOwner(ObmModel):
    id = models.CharField(max_length=15, primary_key=True, db_column='BU_ID')
    name = models.CharField(max_length=20, db_column='BU_NAME')
    cost_center = models.CharField(max_length=20, null=True, blank=True, db_column="COST_CENTER")

    prefix = 'BU'
    primary_key = 'id'

    class Meta:
        ordering = ['name']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_BUSINESS_OWNER')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '%s (%s)' % (self.id, self.name)
