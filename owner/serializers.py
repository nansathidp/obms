from rest_framework import serializers

from .models import BusinessOwner


class BusinessOwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessOwner
        fields = (
            'id',
            'name'
        )
        read_only_fields = ['id', ]
