from rest_framework import exceptions, serializers
from rest_framework.exceptions import ValidationError

from episode.models import Episode
from language.models import Language
from utils.rest_framework.serializer import CSVBase64File


class EpisodeAKAFromSerializer(serializers.Serializer):
    language_id = serializers.CharField(max_length=10)
    name = serializers.CharField(max_length=100)
    synopsis = serializers.CharField(max_length=2000, required=False, allow_null=True, allow_blank=True, default=None)

    def validate_language_id(self, code):
        if Language.objects.filter(code=code).exists():
            return code
        raise ValidationError('Language {} is not found'.format(code))


class EpisodeAKAExportSerializer(serializers.Serializer):
    episode_list = serializers.ListField(
        child=serializers.CharField(max_length=15)
    )

    def validate_episode_list(self, episode_list):
        queryset = Episode.objects.filter(id__in=episode_list)
        if queryset.count() == len(episode_list):
            return episode_list
        raise exceptions.ValidationError()


class EpisodeAKAImportSerializer(serializers.Serializer):
    tmp_file = CSVBase64File()