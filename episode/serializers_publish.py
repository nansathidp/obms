from rest_framework import exceptions, serializers

from publish.models import Publish


class PeriodSerializer(serializers.Serializer):
    datetime_start = serializers.DateTimeField(required=True)
    datetime_end = serializers.DateTimeField(required=True)
    sequence_no = serializers.IntegerField(required=False, allow_null=True, default=0)


class EpisodePublishFromSerializer(serializers.Serializer):
    publish_id = serializers.CharField(required=True, max_length=15)
    desc = serializers.CharField(required=False, max_length=255)
    period_list = serializers.ListField(
        required=True,
        child=PeriodSerializer(),
    )

    def validate_publish_id(self, id):
        if Publish.objects.filter(id=id).exists():
            return id
        raise exceptions.ValidationError('Publish zone not found.')

    def validate_period_list(self, periods_list):
        periods_list.sort(key=lambda p: p['datetime_start'])
        for index, period in enumerate(periods_list):
            period.update({'sequence_no': index + 1})
            if period['datetime_end'] <= period['datetime_start']:
                raise exceptions.ValidationError('Validate datetime_end  not letter datetime_start')
            if len(periods_list) - 1 == index:
                break
            period_next = periods_list[index + 1]
            if period_next:
                if period_next['datetime_start'] <= period['datetime_end']:
                    raise exceptions.ValidationError('Validate datetime_start conflict datetime_end')
        return periods_list
