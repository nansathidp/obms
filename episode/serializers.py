from django.conf import settings
from rest_framework import serializers

from language.models import Language
from publish.models import Publish
from publish.serializers import PublishSerializer
from title.serializer import TitleListSerializer
from .models import AKAEpisode, Episode, PublishEpisode, Segment, ThumbnailEpisode


class ListEpisodeSerializer(serializers.ModelSerializer):
    title_info = TitleListSerializer(source='title', many=False, read_only=True, default=None)

    class Meta:
        model = Episode
        fields = (
            'id',
            'name',
            'duration',
            'title_id',
            'title_info',
            'pac_no',
            'use_run',
            'datetime_expect_receive',
            'datetime_receive',
        )


class EpisodeSerializer(serializers.ModelSerializer):
    segment_list = serializers.SerializerMethodField()
    publish_list = serializers.SerializerMethodField()
    aka_list = serializers.SerializerMethodField()
    thumbnail_list = serializers.SerializerMethodField()
    title_info = TitleListSerializer(source='title', many=False, read_only=True, default=None)

    class Meta:
        model = Episode
        fields = (
            'id',
            'title_id',
            'asset_id',
            'title_info',
            'name',
            'pac_no',
            'use_run',
            'duration',
            'datetime_expect_receive',
            'datetime_receive',
            'publish_list',
            'aka_list',
            'segment_list',
            'thumbnail_list'
        )

    def get_thumbnail_list(self, episode):
        return ThumbnailSerializer(episode.thumbnailepisode_set.all(), many=True).data

    def get_segment_list(self, episode):
        return SegmentEpisodeSerializer(episode.segment_set.all(), many=True).data

    def get_publish_list(self, episode):
        response_list = []
        for publish in Publish.objects.all():
            publish_response = PublishSerializer(publish).data
            period_list = PeriodEpisodeSerializer(
                episode.publishepisode_set.filter(publish=publish).order_by('sequence_no'), many=True).data
            if period_list.__len__() > 0:
                publish_response.update({'period_list': period_list})
                response_list.append(publish_response)
        return response_list

    def get_aka_list(self, episode):
        language_list = Language.objects.filter(is_aka='Y')
        response_list = []
        for language in language_list:
            aka_list = episode.akaepisode_set.filter(language=language)
            if aka_list.exists():
                item = AKAEpisodeSerializer(aka_list.first()).data
            else:
                item = {
                    'name': "",
                    'synopsis': "",
                    'language_id': language.code
                }
            response_list.append(item)
        return response_list


class AKAEpisodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AKAEpisode
        fields = (
            'language_id',
            'name',
            'synopsis',
        )


class PeriodEpisodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublishEpisode
        fields = (
            'datetime_start',
            'datetime_end',
        )


class SegmentEpisodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Segment
        fields = (
            'segment_no',
            'duration',
        )


class ThumbnailSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField()
    text_source = serializers.SerializerMethodField()

    class Meta:
        model = ThumbnailEpisode
        fields = ('tag', 'uri', 'text_source')

    def get_uri(self, thumb):
        return '%s%s' % (settings.MEDIA_URL, thumb.get_name())

    def get_text_source(self, thumbnail):
        return ""
