from rest_framework import serializers


class EpisodeAssetFromSerializer(serializers.Serializer):
    asset_id = serializers.CharField(max_length=15, required=True)
    is_main = serializers.BooleanField(default=False)
