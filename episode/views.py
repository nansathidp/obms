import base64

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import exceptions, filters, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from alert.models import Alert
from title.tasks import export_file_aka, import_file_aka
from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ExportPermission, \
    ImportPermission, ViewPermission
from .filters import EpisodeFilter
from .models import Episode
from .serializers import EpisodeSerializer, ListEpisodeSerializer
from .serializers_aka import EpisodeAKAExportSerializer, EpisodeAKAImportSerializer
from .serializers_episode import EpisodeFromSerializer


class EpisodeView(viewsets.ModelViewSet):
    queryset = Episode.objects.all().select_related('title')
    serializer_class = ListEpisodeSerializer
    filter_backends = (
        DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    filter_class = EpisodeFilter
    search_fields = (
        'id',
        'name',
        'title__id',
        'title__name',
        'duration',
        'pac_no',
        'use_run',
    )

    ordering_fields = (
        'id',
        'name',
        'duration',
        'pac_no',
        'use_run',
    )

    action_serializers = {
        'list': ListEpisodeSerializer,
        'retrieve': EpisodeSerializer,
        'create': EpisodeFromSerializer,
        'download_aka': EpisodeAKAExportSerializer,
        'import_aka': EpisodeAKAImportSerializer,
        'update': EpisodeFromSerializer,
        'partial_update': EpisodeFromSerializer,
        'destroy': EpisodeFromSerializer,

    }

    app = 'episode'
    model = 'episode'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
        'import_aka': ImportPermission,
        'download_aka': [ExportPermission],
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    @action(methods=['POST'], detail=False)
    def download_aka(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        alert = Alert.objects.create(
            percent=10,
            code_name='episode.aka'
        )
        results = serializer.validated_data
        export_file_aka.delay('episode', results['episode_list'], alert.id)
        return Response({'id': alert.id}, status=status.HTTP_200_OK)

    @action(methods=['POST'], detail=False)
    def import_aka(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(True)
        text = request.data['tmp_file']
        text_byte = text.encode("UTF-8")
        text_base64 = base64.b64decode(text_byte)
        source = text_base64.decode("UTF-8")
        alert = Alert.objects.create(code_name='episode.aka', percent=10)
        import_file_aka.delay('episode', source, request.user.id, alert.id)
        return Response({'id': alert.id}, status=status.HTTP_200_OK)

    def perform_destroy(self, instance):
        try:
            super().perform_destroy(instance)
        except Exception as err:
            print(instance.id, 'Process')
            raise exceptions.ValidationError('Episode %s is process' % instance.id)
