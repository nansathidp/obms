from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import EpisodeView

router = DefaultRouter()

router.register(r'', EpisodeView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
