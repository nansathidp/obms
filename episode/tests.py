from django.core.management import call_command
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from account.fixture import create_supper_admin
from account.models import Token
from .fixture import get_form_episode
from .models import Episode


class EpisodeTestCase(APITestCase):
    def setUp(self) -> None:
        call_command('loaddata', 'account/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'country/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'vendor/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'owner/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'studio/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'people/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'genre/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'rating/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'amortize/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'config/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'other_type/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'title/fixtures/initial_data.json', verbosity=0)

        account_admin = create_supper_admin()
        self.token, is_token = Token.objects.get_or_create(account=account_admin)
        self.client = APIClient()
        self.client = APIClient()
        self.client.force_authenticate(account_admin, token=self.token)
        self.path = '/api/episode/'

    def test_created_episode(self):
        data = get_form_episode()
        Episode.objects.delete(name=data['name'])
        response = self.client.post(self.path, data=data, json=True)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(Episode.objects.filter(name=data['name']).exists(), True)
        episode = Episode.objects.get(name=data['name'])
        self.assertEqual(getattr(episode, 'name', None), data['name'])
        self.assertIsNotNone(getattr(episode, 'datetime_expect_receive', None))
