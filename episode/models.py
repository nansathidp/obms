from django.conf import settings
from django.db import models

from config.models import System
from utils.fields import ObmModel


class Episode(ObmModel):
    id = models.CharField(max_length=15, primary_key=True, db_column="EPISODE_ID")
    title = models.ForeignKey(
        'title.Title',
        db_column="TITLE_ID",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=50, db_column="EPISODE_NAME")
    duration = models.CharField(max_length=6, db_column="DURATION")
    pac_no = models.IntegerField(null=True, default=None, db_column="PAC_NO")
    use_run = models.IntegerField(null=True, default=0, db_column="USE_RUN")
    asset_id = models.CharField(
        max_length=15,
        null=True,
        blank=True,
        default=None,
        db_column="MAIN_ASSET_ID",
    )
    datetime_expect_receive = models.DateTimeField(null=True,  db_column="EXPECT_RECEIVE_DATE")
    datetime_receive = models.DateTimeField(null=True, db_column='RECEIVE_DATE')

    prefix = 'SE'
    primary_key = 'id'

    class Meta:
        ordering = ['-id']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_EPISODE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete', 'import', 'export')

    def __str__(self):
        return '{}'.format(self.id)

    def update_total_episode(self):
        self.title.update_total_episode()

    @staticmethod
    def is_period_use(title_id, datetime_start, datetime_end):
        publise_list = PublishEpisode.objects.filter(episode__title_id=title_id)
        if publise_list.filter(datetime_start__lt=datetime_start).exists() or publise_list.filter(datetime_end__gt=datetime_end).exists():
            return False
        return True


class AKAEpisode(models.Model):
    episode = models.ForeignKey(
        Episode,
        primary_key=settings.IS_ORA,
        db_column="EPISODE_ID",
        on_delete=models.CASCADE,
    )
    language = models.ForeignKey(
        'language.Language',
        related_name="aka_episode_language_set",
        db_column="LANGUAGE_CODE",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=200, null=True, default=None, db_column="EPISODE_NAME")
    synopsis = models.CharField(max_length=200, null=True, default=None, db_column="SYNOPSIS")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")

    class Meta:
        ordering = ('language', 'name')
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_EPISODE_AKA')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete', 'import', 'export')

    def __str__(self):
        return '{}-{} ({})'.format(self.episode_id, self.name, self.language_id)


class PublishEpisode(models.Model):
    episode = models.ForeignKey(
        Episode,
        primary_key=settings.IS_ORA,
        db_column="EPISODE_ID",
        on_delete=models.CASCADE,
    )
    publish = models.ForeignKey(
        'publish.Publish',
        related_name="publish_set",
        db_column="PUBLISH_ZONE_ID",
        on_delete=models.CASCADE,
    )
    sequence_no = models.IntegerField(db_column="SEQ_NO")
    datetime_start = models.DateTimeField(null=True, default=None, db_column="PUBLISH_START")
    datetime_end = models.DateTimeField(null=True, default=None, db_column='PUBLISH_END')
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")

    class Meta:
        ordering = (
            '-episode_id',
        )
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_EPISODE_PUBLISH')
        default_permissions = ('view', 'add', 'change', 'delete',)

    def __str__(self):
        return '{} {} ({})'.format(self.episode_id, self.publish_id, self.publish.desc)


class ThumbnailEpisode(models.Model):
    episode = models.ForeignKey(
        Episode,
        primary_key=settings.IS_ORA,
        on_delete=models.CASCADE,
        db_column="EPISODE_ID"
    )
    tag = models.CharField(max_length=200, db_column="TAG")
    file = models.FileField(upload_to='thumbnail/', db_column="FILENAME")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_EPISODE_THUMNAILS')
        default_permissions = ('view', 'add', 'change', 'delete',)

    def get_name(self):
        return self.file.name.split('/').pop()


class Segment(models.Model):
    episode = models.ForeignKey(
        Episode,
        primary_key=settings.IS_ORA,
        db_column="EPISODE_ID",
        on_delete=models.CASCADE,
    )
    segment_no = models.IntegerField(db_column="SEGMENT_NO")
    duration = models.CharField(max_length=6, db_column="DURATION")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_EPISODE_SEGMENT')
        default_permissions = ('view', 'add', 'change', 'delete',)
