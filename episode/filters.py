from django_filters import filters
from django_filters.rest_framework import FilterSet

from episode.models import Episode


class EpisodeFilter(FilterSet):
    title_name = filters.CharFilter(
        method="get_title_name",
        field_name="title_name"
    )

    class Meta:
        model = Episode
        fields = (
            'title_name',
            'title_id'
        )

    def get_title_name(self, queryset, name, value):
        return queryset.filter(title__name__icontains=value)
