from datetime import timedelta

from django.utils import timezone

from .models import Episode

AKA_ITEM = {
    'language': 'tha',
    'name': 'ไอ่หนุมพลังช้าง',
    'synopsis': 'ตำนานพลังช้างและความแข็งแรงของไอ่หนุ่มพลังช้าง'
}

PUBLISH_ITEM = {
    'publish_zone_id': '0',
    'start': timezone.now(),
    'end': timezone.now() + timedelta(days=200)
}

SEGMENT = {
    'segment_on': 1,
    'duration': '010000',
}

EPISODE_ITEM = {
    'id': 'SE202020100001',
    'name': 'superman',
    'pac_no': 1,
    'duration': '010000',
    'use_run': 1,
    'available_run': 1,
    'publish': [PUBLISH_ITEM],
    'aka': [AKA_ITEM],
    'segment': [SEGMENT],
}


def init_episode(**kwargs):
    Episode.objects.get_or_create(**kwargs)


def get_form_episode():
    return {
        "account_updated": None,
        "account_created": "1",
        "datetime_updated": None,
        "datetime_created": "2020-06-17T19:41:32Z",
        "title": "PGS201006000058",
        "name": "\u0e04\u0e23\u0e39\u0e1e\u0e34\u0e40\u0e28\u0e29\u0e08\u0e2d\u0e21\u0e1b\u0e48\u0e27\u0e19 \u0e1b\u0e35 2 \u0e15\u0e2d\u0e19\u0e17\u0e35\u0e48 12",
        "duration": "002400",
        "pac_no": 12,
        "use_run": 0,
        "asset_id": "ASS202004003270",
        "datetime_expect_receive": "2020-06-17T19:41:32Z",
    }