from rest_framework import exceptions, serializers

from deal.models import Video
from publish.models import Country
from title.models import Title
from utils.deal import queryset as util_queryset
from utils.rest_framework import serializer as utils
from .models import Episode
from .serializer_thumbnail import EpisodeThumbFromSerializer
from .serializers_aka import EpisodeAKAFromSerializer
from .serializers_publish import EpisodePublishFromSerializer
from .serializers_segment import EpisodeSegmentFromSerializer


class EpisodeFromSerializer(utils.ContentSerializer, serializers.Serializer, ):
    id = serializers.CharField(required=False, max_length=15, allow_blank=True, allow_null=True)
    title_id = serializers.CharField(max_length=15)
    name = serializers.CharField(required=True, max_length=50)
    pac_no = serializers.IntegerField(required=True, min_value=1)
    duration = serializers.CharField(max_length=6, allow_null=True, allow_blank=True)
    use_run = serializers.IntegerField(default=0)
    datetime_expect_receive = serializers.DateTimeField(default=None, allow_null=True)

    publish_list = serializers.ListField(
        child=EpisodePublishFromSerializer(),
        required=False,
        allow_null=True,
        default=[]
    )

    aka_list = serializers.ListField(
        child=EpisodeAKAFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )

    segment_list = serializers.ListField(
        child=EpisodeSegmentFromSerializer(),
        allow_null=True,
        default=[]
    )
    thumbnail_list = serializers.ListField(
        child=EpisodeThumbFromSerializer(),
        allow_null=True,
        default=[]
    )

    fields_map = {
        'title_id': 'title_id',
        'name': 'name',
        'pac_no': 'pac_no',
        'duration': 'duration',
        'datetime_expect_receive': 'datetime_expect_receive',
        'account_created': 'account_created',
        'account_updated': 'account_updated',
    }

    class Meta:
        model = Episode

    def create_relation(self, instance, validated_data, **kwargs):
        self.create_aka(instance, validated_data, **kwargs)
        self.create_publish(instance, validated_data, **kwargs)
        self.create_segment(instance, validated_data, **kwargs)
        self.create_thumbnail(instance, validated_data, **kwargs)
        instance.update_total_episode()

    def create_thumbnail(self, episode, validated_data, **kwargs):
        thumbnail_list = validated_data.get('thumbnail_list')
        if episode.thumbnailepisode_set.exists():
            episode.thumbnailepisode_set.all().delete()
        if thumbnail_list:
            for thumbnail in thumbnail_list:
                episode.thumbnailepisode_set.create(
                    file=thumbnail['text_source'],
                    tag=thumbnail['tag'],
                    account_created=kwargs.get('account'),
                )

    def create_aka(self, episode, validate_data, **kwargs):
        aka_synopsis_list = validate_data.get('aka_list', None)
        if aka_synopsis_list:
            episode.akaepisode_set.all().delete()
            for aka_synopsis in aka_synopsis_list:
                aka_synopsis.update({'account_created': kwargs.get('account')})
                episode.akaepisode_set.create(**aka_synopsis)

    def create_publish(self, episode, validate_data, **kwargs):
        publish_list = validate_data.get('publish_list', [])
        if len(publish_list) > 0:
            episode.publishepisode_set.all().delete()
            for publish_info in publish_list:
                for period in publish_info.get('period_list', []):
                    period.update(
                        {'account_created': kwargs.get('account'), 'publish_id': publish_info.get('publish_id')})
                    try:
                        episode.publishepisode_set.create(**period)
                    except Exception as err:
                        print(err)
                        raise exceptions.ValidationError('Please validate periods')

    def create_segment(self, episode, validate_data, **kwargs):
        segment_list = validate_data.get('segment_list', None)
        if segment_list:
            episode.segment_set.all().delete()
            for segment in segment_list:
                segment.update({'account_created': kwargs.get('account')})
                episode.segment_set.create(**segment)

    def validate_name(self, name):
        view = self.context.get('view', None)
        instance = None
        if view and view.action in ['update', 'partial_update']:
            instance = view.get_object()

        episode_list = Episode.objects.filter(name=name)
        if instance:
            episode_list = episode_list.exclude(id=instance.id)
        if episode_list.exists():
            raise exceptions.ValidationError('Episode name duplicate'.format(name))
        return name.upper().strip(" ")

    def validate_title_id(self, title_id):
        title = Title.objects.filter(id=title_id).first()
        if title:
            view = self.context.get('view', None)
            if view.action == 'create' and title.title_type == 'NONE-SERIES':
                return exceptions.ValidationError('Create series only.')
            return title_id
        raise exceptions.ValidationError('Title is not found'.format(title_id))

    def validate(self, attr):

        publish_item_list = attr.get('publish_list', [])
        if publish_item_list:
            title_id = attr['title_id']
            video_deal_list = Video.objects.filter(title_id=title_id).select_related('deal')
            if not video_deal_list.exists():
                raise exceptions.ValidationError('Title id  {} is deal right not found'.format(title_id))
        for publish_item in publish_item_list:
            country_code_publish_list = Country.objects.filter(
                publish_id=publish_item['publish_id']
            ).values_list(
                'country_id',
                flat=True
            )
            if len(country_code_publish_list) == 0:
                raise exceptions.ValidationError(
                    'Country of publish_id `{}` is not found.'.format(publish_item['publish_id']))
            video_deal_list = video_deal_list.filter(
                deal__territory__country_id__in=country_code_publish_list
            )

            if not video_deal_list.exists():
                raise exceptions.ValidationError('Title id {} of country is deal right not found'.format(title_id))
            for periods_item in publish_item.get('periods_list', []):
                video_deal_exists = util_queryset.is_period_conflict(periods_item['datetime_start'],
                                                                     periods_item['datetime_end'], video_deal_list)

                if video_deal_exists:
                    continue
                raise exceptions.ValidationError(
                    'Publish {} period form {} to {} is conflict '.format(
                        publish_item['publish_id'],
                        periods_item['datetime_start'],
                        periods_item['datetime_end']
                    )
                )

        return attr
