from django.contrib import admin

from .models import Deal, Linear, Video


@admin.register(Deal)
class DealModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'ref')


@admin.register(Video)
class VideoModelAdmin(admin.ModelAdmin):
    list_display = ('no', 'title_id', 'deal_id', 'datetime_right_start', 'datetime_right_end', 'type')
    list_filter = ('type',)


@admin.register(Linear)
class LinearModelAdmin(admin.ModelAdmin):
    list_display = ('no', 'title_id', 'deal_id', 'datetime_right_start', 'datetime_right_end',)
