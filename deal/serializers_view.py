from rest_framework import serializers

from config.models import System
from deal.models import Deal, Linear, Territory, Video


class LinearListSerializer(serializers.ModelSerializer):
    title_name = serializers.CharField(source='title.name', default="")
    title_type = serializers.CharField(source='title.title_type', default="")
    total_episode = serializers.CharField(source='title.total_episode', default="")
    duration = serializers.CharField(source='title.duration', default="")

    class Meta:
        model = Linear
        fields = (
            'title_id',
            'title_name',
            'title_type',
            'duration',
            'max_run',
            'total_episode',
            'datetime_right_start',
            'datetime_right_end',
        )


class VideoListSerializer(serializers.ModelSerializer):
    title_name = serializers.CharField(source='title.name', default="")
    title_type = serializers.CharField(source='title.title_type', default="")
    total_episode = serializers.CharField(source='title.total_episode', default="")
    duration = serializers.CharField(source='title.duration', default="")

    class Meta:
        model = Video
        fields = (
            'title_id',
            'title_name',
            'title_type',
            'duration',
            'total_episode',
            'mg',
            'deem_price',
            'rev_share',
            'datetime_right_start',
            'datetime_right_end',
        )


class TerritorySerializer(serializers.ModelSerializer):
    country_code = serializers.CharField(source="country.code")
    country_name = serializers.CharField(source="country.name")

    class Meta:
        model = Territory
        fields = (
            'country_code',
            'country_name',
        )


class DealDetailSerializer(serializers.ModelSerializer):
    deal_no = serializers.CharField(source='id')
    ref_no = serializers.CharField(source='ref', default="")
    remark = serializers.CharField(source="description", default="")
    datetime_transaction = serializers.DateTimeField(source='transaction', default="")
    linear_list = serializers.SerializerMethodField()
    other_list = serializers.SerializerMethodField()
    territory_list = serializers.SerializerMethodField()

    class Meta:
        model = Deal
        fields = (
            'deal_no',
            'ref_no',
            'status',
            'remark',
            'attach',
            'studio_id',
            'vendor_id',
            'bu_id',
            'linear_list',
            'other_list',
            'territory_list',
            'datetime_transaction',
        )

    def get_other_list(self, deal):
        response_list = []
        for system in System.pull_vod_type():
            vod_type = system.get_vod_type_display()
            if vod_type.get('is_enable', False):
                response = {
                    'label': vod_type.get('type'),
                    'items': VideoListSerializer(deal.video_set.filter(type=vod_type.get('type')), many=True).data
                }
                response_list.append(response)
        return response_list

    def get_linear_list(self, deal):
        return LinearListSerializer(deal.linear_set.all(), many=True).data

    def get_territory_list(self, deal):
        return TerritorySerializer(deal.territory_set.all(), many=True).data


class DealListSerializer(serializers.ModelSerializer):
    deal_no = serializers.CharField(source='id')
    ref_no = serializers.CharField(source='ref', default="")
    datetime_transaction = serializers.DateTimeField(source='transaction')
    studio_name = serializers.CharField(source='studio.name', default="")
    vendor_name = serializers.CharField(source='vendor.name', default="")
    datetime_right_start = serializers.SerializerMethodField()
    datetime_right_end = serializers.SerializerMethodField()

    class Meta:
        model = Deal
        fields = (
            'deal_no',
            'ref_no',
            'status',
            'datetime_transaction',
            'studio_id',
            'studio_name',
            'vendor_id',
            'vendor_name',
            'datetime_right_start',
            'datetime_right_end',
        )

    def get_datetime_right_start(self, deal):
        linear = deal.linear_set.all().order_by('datetime_right_start').first()
        video = deal.video_set.all().order_by('datetime_right_start').first()
        if linear and video:
            if linear.datetime_right_start < video.datetime_right_start:
                return linear.datetime_right_start
            else:
                return video.datetime_right_start
        elif linear:
            return linear.datetime_right_start
        elif video:
            return video.datetime_right_start
        else:
            return None

    def get_datetime_right_end(self, deal):
        linear = deal.linear_set.all().order_by('-datetime_right_end').first()
        video = deal.video_set.all().order_by('-datetime_right_end').first()
        if linear and video:
            if linear.datetime_right_end > video.datetime_right_end:
                return linear.datetime_right_end
            else:
                return video.datetime_right_end
        elif linear:
            return linear.datetime_right_end
        elif video:
            return video.datetime_right_end
        else:
            return None
