from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status, viewsets
from rest_framework.response import Response

from .filters import DealFilter
from .models import Deal
from .serializers_from import DealFromSerializer
from .serializers_view import DealDetailSerializer, DealListSerializer


class DealView(viewsets.ModelViewSet):
    queryset = Deal.objects.all().select_related('vendor').select_related('studio')
    serializer_class = DealListSerializer
    filter_backends = (
        filters.SearchFilter,
        DjangoFilterBackend,
    )
    filter_class = DealFilter

    search_fields = (
        'id',
        'ref',
        'status',
        'transaction',
    )

    action_serializers = {
        'create': DealFromSerializer,
        'destroy': DealFromSerializer,
        'update': DealFromSerializer,
        'partial_update': DealFromSerializer,
        'list': DealListSerializer,
        'retrieve': DealDetailSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        response = DealDetailSerializer(instance).data
        headers = self.get_success_headers(response)
        return Response(response, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()
