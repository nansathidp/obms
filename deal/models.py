from django.conf import settings
from django.db import models

from config.models import System
from utils.deal.queryset import is_period_conflict
from utils.fields import ObmModel


class Deal(ObmModel):
    id = models.CharField(max_length=15, primary_key=True, db_column='DEAL_ID')

    ref = models.CharField(max_length=50, null=True, blank=True, db_column='REF_NO')
    status = models.CharField(max_length=20, null=True, blank=True, db_column='DEAL_STATUS')
    description = models.CharField(max_length=20, null=True, blank=True, db_column='DESCRIPTION')
    attach = models.FileField(upload_to='attach/', null=True, blank=True, db_column='CONTRACT_ATTACH')
    vendor = models.ForeignKey(
        'vendor.Vendor',
        related_name='+',
        null=True,
        blank=True,
        db_column='VENDOR_ID',
        on_delete=models.SET_NULL
    )
    studio = models.ForeignKey(
        'studio.Studio',
        related_name='+',
        null=True,
        blank=True,
        db_column='STUDIO_ID',
        on_delete=models.SET_NULL
    )
    bu = models.ForeignKey(
        'owner.BusinessOwner',
        related_name='+',
        null=True,
        blank=True,
        db_column='BU_ID',
        on_delete=models.SET_NULL
    )
    transaction = models.DateTimeField(null=True, blank=True, db_column='TRANSACTION_DATE')
    prefix = 'DL'

    class Meta:
        ordering = ['-id']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_DEAL')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete', 'import', 'export')

    def __str__(self):
        return self.id

    def title_publish(self, title, datetime_start, datetime_end, vod_type):
        assert System.is_vod_type(vod_type), 'VOD_TYPE is not found'
        if vod_type == 'LINEAR':
            queryset = Linear.objects.filter(
                title=title,
            )
            assert not is_period_conflict(datetime_start, datetime_end,
                                          queryset), 'Linear title of `{}` is period conflict'.format(title.name)
            linear = queryset.filter(deal=self).last()
            index = linear.no + 1 if linear else 1
            Linear.objects.create(
                deal=self,
                title=title,
                no=index,
                datetime_right_start=datetime_start,
                datetime_right_end=datetime_end,
            )
        else:
            queryset = Video.objects.filter(
                title=title,
            )
            assert not is_period_conflict(datetime_start, datetime_end,
                                          queryset), '{} title of `{}` is period conflict'.format(vod_type, title.name)
            video = queryset.filter(deal=self).last()
            index = video.no + 1 if video else 1
            Video.objects.create(
                deal=self,
                title=title,
                no=index,
                datetime_right_start=datetime_start,
                datetime_right_end=datetime_end,
            )


class Linear(models.Model):
    deal = models.ForeignKey(
        Deal,
        db_column='DEAL_ID',
        on_delete=models.CASCADE
    )
    title = models.ForeignKey(
        'title.Title',
        db_column='TITLE_ID',
        on_delete=models.CASCADE,
    )
    no = models.IntegerField(primary_key=settings.IS_ORA, db_column='SEQ_NO')
    datetime_right_start = models.DateTimeField(db_column='RIGHTS_START')
    datetime_right_end = models.DateTimeField(db_column='RIGHTS_END')
    max_run = models.IntegerField(db_column='MAX_RUN', default=0)
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='+',
        db_column='CREATED_BY',
        on_delete=models.CASCADE,
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column='CREATED_DATE')

    class Meta:
        ordering = ['-no']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_DEAL_TITLE_RIGHTS_LINEAR')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '{} ({}) ({})'.format(self.title_id, self.deal_id, self.no)

    @staticmethod
    def pull_by_title_id(title_id):
        return Linear.objects.filter(title_id=title_id)

    @property
    def type(self):
        return 'linear'.upper()


class Video(models.Model):
    title = models.ForeignKey(
        'title.Title',
        db_column='TITLE_ID',
        on_delete=models.CASCADE
    )
    deal = models.ForeignKey(
        Deal,
        db_column='DEAL_ID',
        on_delete=models.CASCADE
    )
    type = models.CharField(max_length=20, db_column="VOD_TYPE")
    no = models.IntegerField(db_column='SEQ_NO', primary_key=settings.IS_ORA)
    mg = models.IntegerField(null=True, blank=True, db_column='MG')
    deem_price = models.IntegerField(null=True, blank=True, db_column='DEEM_PRICE')
    rev_share = models.IntegerField(null=True, blank=True, db_column='REV_SHARE')

    datetime_right_start = models.DateTimeField(db_column='RIGHTS_START')
    datetime_right_end = models.DateTimeField(db_column='RIGHTS_END')
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name='+',
        db_column='CREATED_BY',
        on_delete=models.SET_NULL
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column='CREATED_DATE')

    class Meta:
        ordering = ['-no']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_DEAL_TITLE_RIGHTS_VOD')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '{}-{}({})'.format(self.title_id, self.deal_id, self.no)

    @staticmethod
    def pull_title_id(title_id):
        return Video.objects.filter(title_id=title_id)

    @staticmethod
    def pull_by_title_id_and_type(title_id, vod_type):
        return Video.pull_title_id(title_id).filter(type=vod_type)


class Territory(models.Model):
    deal = models.ForeignKey(
        Deal,
        primary_key=settings.IS_ORA,
        db_column='DEAL_ID',
        on_delete=models.CASCADE
    )
    country = models.ForeignKey(
        'country.Country',
        related_name="country_territory_set",
        null=True,
        db_column="COUNTRY_CODE",
        on_delete=models.SET_NULL
    )
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        related_name='+',
        db_column='CREATED_BY',
        on_delete=models.SET_NULL
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column='CREATED_DATE')

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_DEAL_TERRITERY')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

