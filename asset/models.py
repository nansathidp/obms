from django.db import models

from bms import settings
from utils.fields import SaveMixin


class Asset(SaveMixin, models.Model):
    id = models.CharField(max_length=15, primary_key=True, unique=True, db_column="ASSET_ID")
    episode = models.ForeignKey('episode.Episode', related_name="episode_set", null=True, on_delete=models.SET_NULL, db_column="EPISODE_ID")
    file_name = models.CharField(max_length=50, db_column="FILE_NAME")
    status = models.CharField(max_length=20, db_column="STATUS")
    duration = models.IntegerField(db_column="DURATION")
    media_info = models.CharField(max_length=1000, db_column="MEDIA_INFO")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    prefix = "AS"
    primary_key = "id"

    class Meta:
        ordering = ['-datetime_created', '-id']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_ASSET')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)
