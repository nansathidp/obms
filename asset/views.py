from rest_framework import viewsets

from .models import Asset
from .serializers import AssetListSerializer, AssetSerializer


class AssetView(viewsets.ModelViewSet):
    queryset = Asset.objects.all()
    serializer_class = AssetListSerializer
    action_serializers = {
        'list': AssetListSerializer,
        'create': AssetSerializer,
        'update': AssetSerializer,
        'retrieve': AssetSerializer,
        'destroy': AssetSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()
