from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import AssetView

router = DefaultRouter()
router.register(r'', AssetView)

urlpatterns = [
    url(r'^', include(router.urls)),
]