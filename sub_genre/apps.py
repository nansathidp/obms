from django.apps import AppConfig


class SubGenreConfig(AppConfig):
    name = 'sub_genre'
