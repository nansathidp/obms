from rest_framework import viewsets

from .models import SubGenre
from .serializers import SubGenreSerializer


class SubGenreView(viewsets.ReadOnlyModelViewSet):
    queryset = SubGenre.objects.all()
    serializer_class = SubGenreSerializer
    pagination_class = None

    app = 'sub_genre'
    model = 'subgenre'



