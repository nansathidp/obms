from rest_framework import serializers

from .models import SubGenre


class SubGenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubGenre
        fields = (
            'id',
            'name'
        )