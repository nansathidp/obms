from django.contrib import admin

from .models import SubGenre


@admin.register(SubGenre)
class SubGenreModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )
    search_fields =  ['id', 'name']