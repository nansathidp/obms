from django_filters.rest_framework import FilterSet
from django_filters import filters
from ..models import SubGenre


class SubGenreFilter(FilterSet):
    sub_genre_name = filters.CharFilter(
        method='get_sub_genre_name',
        field_name='sub_genre_name'
    )

    class Meta:
        model = SubGenre
        fields = (
            'id',
            'name'
        )

    def get_sub_genre_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
