from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from sub_genre.management.views import SubGenreView

router = DefaultRouter()
router.register(r'', SubGenreView)

app_name = 'sub_genre'
urlpatterns = [
    url(r'^', include(router.urls)),
]