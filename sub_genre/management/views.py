from rest_framework import exceptions, status, viewsets, filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .serializers import SubGenreSerializer, SubGenreListSerializer
from ..models import SubGenre
from .filters import SubGenreFilter

class SubGenreView(viewsets.ModelViewSet):
    queryset = SubGenre.objects.all()
    serializer_class = SubGenreSerializer

    app = 'sub_genre'
    model = 'subgenre'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    action_serializers = {
        'list': SubGenreListSerializer,
        'create': SubGenreSerializer,
        'update': SubGenreSerializer,
        'partial_update': SubGenreSerializer,
        'destroy': SubGenreListSerializer,
    }

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    filter_class = SubGenreFilter

    search_fields = (
        'id',
        'name'        
    )

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        vendor = self.perform_create(serializer)
        serializer_response = SubGenreListSerializer(vendor)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()

    def perform_destroy(self, instance):
        try:
            super().perform_destroy(instance)
        except Exception as err:
            print(err)
            raise exceptions.ValidationError()
