from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import SubGenreView

router = DefaultRouter()
router.register(r'', SubGenreView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
