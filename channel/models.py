from django.conf import settings
from django.db import models

from utils.fields import ObmModel


class Channel(ObmModel):
    ACTIVE_CHOICE = (
        ('Y', 'Yes'),
        ('N', 'No')
    )
    DEFINITIONS_TYPE_CHOICES = (
        ('SD', 'SD'),
        ('HD', 'HD'),
        ('UHD', 'UHD')
    )
    SOURCE_TYPE_CHOICES = (
        ('TVG', 'TVG'),
        ('INTERNET', 'INTERNET')
    )
    code = models.CharField(max_length=20, primary_key=True, db_column="STREAM_CODE")
    name = models.CharField(max_length=20, null=True, blank=True, db_column="STREAM_NAME")
    bms_code = models.CharField(max_length=5, null=True, blank=True, db_column="BMS_CH_CODE")
    definition_type = models.CharField(max_length=10, choices=DEFINITIONS_TYPE_CHOICES, default='SD',
                                       db_column="DIFINATION_TYPE")
    source = models.CharField(max_length=20, choices=SOURCE_TYPE_CHOICES, default='TVG', db_column="SOURCE")

    is_active = models.CharField(max_length=1, choices=ACTIVE_CHOICE, default='Y', db_column="ACTIVE_FLAG")
    is_catchup = models.CharField(max_length=1, choices=ACTIVE_CHOICE, default='N', db_column="CATCHUP_FLAG")
    is_live_record = models.CharField(max_length=1, choices=ACTIVE_CHOICE, default='N', db_column='LIVE_TO_VOD_FLAG')
    is_blackout = models.CharField(max_length=1, choices=ACTIVE_CHOICE, default='N', db_column="BLACK_OUT_FLAG")

    class Meta:
        ordering = ['code']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_LIVE_CHANNEL')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    @staticmethod
    def pull(code):
        from .cacheds import get_by_code, set_by_code
        instance = get_by_code(code)
        if instance:
            return instance
        try:
            instance = Channel.objects.get(code=code)
            set_by_code(code, instance)
            return instance
        except Channel.DoesNotExist as err:
            return None

    @staticmethod
    def pull_bms(bms_code):
        from .cacheds import get_by_bms, set_by_bms
        instances = get_by_bms(bms_code)
        if instances:
            return instances
        instances = Channel.objects.filter(bms_code=bms_code)
        set_by_bms(bms_code, instances)
        return instances


