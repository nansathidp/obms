from django.core.cache import cache
expire_time = 60 * 60 * 24

namespace = 'channel'

def get_key(prefix, key):
    return '%s_%s_%s' % (namespace, prefix, key)

def get_by_code(code):
    key = get_key('code', code)
    return cache.get(key)

def get_by_bms(bms_code):
    key = get_key('bms_code', bms_code)
    return cache.get(key)

def set_by_code(code, instance):
    key = get_key('code', code)
    cache.set(key, instance)

def set_by_bms(bms_code, instance):
    key = get_key('bms_code', bms_code)
    cache.set(key, instance)