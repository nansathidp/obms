from django.core.management import call_command
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from account.fixture import create_supper_admin
from account.models import Token


class ChannelSelectTestCase(APITestCase):
    def setUp(self) -> None:
        call_command('loaddata', 'account/fixtures/initial_data.json', verbosity=0)
        call_command('loaddata', 'channel/fixtures/initial_data.json', verbosity=0)
        account_admin = create_supper_admin()
        self.token, is_token = Token.objects.get_or_create(account=account_admin)
        self.client = APIClient()
        self.client.force_authenticate(account_admin, token=self.token)
        self.path = '/api/channel/'

    def test_get_list(self):
        response = self.client.get(self.path)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

