from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import SearchFilter

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .serializers import ChannelManagementSerializer
from ..models import Channel


class ChannelView(viewsets.ModelViewSet):
    queryset = Channel.objects.all()
    serializer_class = ChannelManagementSerializer

    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
    )

    app = 'channel'
    model = 'channel'

    search_fields = (
        'code',
        'name',
        'bms_code',
        'definition_type'
    )

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]
