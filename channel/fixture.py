from .models import Channel


def create_channel(
        account_created,
        code='c03',
        name="Channel 3 HD",
        bms_code="N30",
        definition_type='HD',
        source='internet',
        is_active='Y',
        is_catchup='N',
        is_live_record='N',
        is_blackout='Y'
):
    return Channel.objects.get_or_create(
        code=code,
        name=name,
        bms_code=bms_code,
        definition_type=definition_type,
        source=source,
        is_active=is_active,
        is_catchup=is_catchup,
        is_live_record=is_live_record,
        is_blackout=is_blackout,
        account_created=account_created
    )