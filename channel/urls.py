from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import ChannelView

router = DefaultRouter()
router.register(r'', ChannelView)

urlpatterns = [
    url(r'^', include(router.urls)),
]