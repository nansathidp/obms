from django.contrib import admin

from .models import Channel


@admin.register(Channel)
class ChannelModelAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'bms_code', 'is_active', 'is_catchup', 'is_live_record', 'is_blackout', "definition_type")
    list_filter = ['is_active', 'is_catchup', 'is_live_record', 'is_blackout', 'definition_type']
    search_fields = ['code', 'name', 'bms_code',]
    ordering = ['code', 'name', 'bms_code',]