from rest_framework import serializers

from channel.models import Channel


class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = (
            'code',
            'name',
            'bms_code',
        )