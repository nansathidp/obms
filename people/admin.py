from django.contrib import admin

from .models import AKA, People


@admin.register(People)
class PeopleModelAdmin(admin.ModelAdmin):
    list_display = ('id',)


@admin.register(AKA)
class AKAModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'people', 'language')
