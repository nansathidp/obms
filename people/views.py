from rest_framework import viewsets

from .models import People
from .serializers import PeopleListSerializer


class PeopleView(viewsets.ReadOnlyModelViewSet):
    queryset = People.objects.all()
    serializer_class = PeopleListSerializer
    pagination_class = None

    app = 'people'
    model = 'people'