from django_filters import filters
from django_filters.rest_framework import FilterSet

from ..models import People


class PeopleFilter(FilterSet):
    people_name = filters.CharFilter(
        method='get_people_name',
        field_name='people_name'
    )

    class Meta:
        model = People
        fields = (
            'id',
            'name',
            'datetime_created',
            'account_created',            
        )

    def get_people_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
