from rest_framework import serializers

from language.serializers import LanguageSerializer
from .models import AKA, People


class PeopleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = People
        fields = (
            'id',
            'name'            
        )


class PeopleMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = People
        fields = (
            'id',
            'name'            
        )


class AKASerializer(serializers.ModelSerializer):

    class Meta:
        model = AKA
        fields = (
            'people',
            'name',
            'language'            
        )


class AKAListSerializer(serializers.ModelSerializer):    
    language = serializers.SerializerMethodField()

    class Meta:
        model = AKA
        fields = (
            'people',
            'name',
            'language',            
        )

    def get_language(self, aka):
        return LanguageSerializer(aka.language).data
