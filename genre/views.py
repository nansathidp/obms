from rest_framework import viewsets

from .models import Genre
from .serializer import GenreSerializer


class GenreView(viewsets.ReadOnlyModelViewSet):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer
    pagination_class = None
