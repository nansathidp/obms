from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import exceptions, filters, status, viewsets
from rest_framework.response import Response

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .filters import GenreFilter
from .serializers import GenreListSerializer, GenreSerializer
from ..models import Genre


class GenreView(viewsets.ModelViewSet):
    queryset = Genre.objects.all()
    serializer_class = GenreListSerializer
    app = 'genre'
    model = 'genre'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    action_serializers = {
        'list': GenreListSerializer,
        'create': GenreSerializer,
        'update': GenreSerializer,
        'partial_update': GenreSerializer,
        'destroy': GenreSerializer,
    }

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    filter_class = GenreFilter

    search_fields = (
        'id',
        'name'        
    )

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        vendor = self.perform_create(serializer)
        serializer_response = GenreSerializer(vendor)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()

    def perform_destroy(self, instance):
        try:
            super().perform_destroy(instance)
        except Exception as err:
            print(err)
            raise exceptions.ValidationError()
