from django_filters import filters
from django_filters.rest_framework import FilterSet

from ..models import Genre


class GenreFilter(FilterSet):
    genre_name = filters.CharFilter(
        method='get_genre_name',
        field_name='genre_name'
    )

    class Meta:
        model = Genre
        fields = (
            'id',
            'name'            
        )

    def get_genre_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
