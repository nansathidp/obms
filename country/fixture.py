from .models import Country


def init_country(code):
    return Country.objects.get_or_create(code=code, name='mock-country')