from django.urls import include, path
from rest_framework import routers

from .views import CountryView

router = routers.DefaultRouter()
router.register(r'', CountryView)

app_name = 'country'
urlpatterns = [    
    path('', include(router.urls)),
]
