from django_filters import filters
from django_filters.rest_framework import FilterSet

from ..models import Country


class CountryFilter(FilterSet):
    country_name = filters.CharFilter(
        method='get_country_name',
        field_name='country_name'
    )

    class Meta:
        model = Country
        fields = (
            'code',
            'name'            
        )

    def get_country_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
