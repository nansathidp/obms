from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import CountryView

router = DefaultRouter()

router.register(r'', CountryView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
