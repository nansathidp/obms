FROM python:3.7.6-slim-stretch as base
ADD pkg pkg
RUN apt update -y && apt install -y  \
    alien \
    apt-utils \
    nano \
    libssl-dev \
    python3-mysqldb \
    build-essential \
    python3-dev \
    default-libmysqlclient-dev \
    libffi-dev \
    uwsgi \
    mysql-client && \
    alien -i pkg/oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm && \
    alien -i pkg/oracle-instantclient12.2-sqlplus-12.2.0.1.0-1.x86_64.rpm && \
    sh -c 'echo "/usr/lib/oracle/12.2/client64/lib/" > /etc/ld.so.conf.d/oracle.conf' && \
    ldconfig && \
    ln -s /usr/bin/sqlplus64 /usr/bin/sqlplus

ADD requirements.txt  requirements.txt
RUN pip install -r requirements.txt

FROM base
RUN mkdir -p /app
ADD . /app
WORKDIR /app/
RUN mkdir -p media/thumbnail
EXPOSE 8000
RUN chmod +x docker-entrypoint.sh
RUN cp -f bms/stg_settings.py  bms/local_settings.py
ENTRYPOINT ./docker-entrypoint.sh
