import os
import sys

import django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bms.settings")
django.setup()

from django.conf import settings
from django.contrib.auth.models import Group, Permission

COMPONENT = [
    'episode',
    'title',
    'deal',
]

group_name = 'Product Admin'
COMPONENT += settings.MASTER_APPS


def grant_product_admin():
    print('init group {} '.format(group_name))
    group, is_created = Group.objects.get_or_create(name=group_name)
    permission_list = Permission.objects.filter(content_type__app_label__in=COMPONENT)
    for permission in permission_list:
        if not group.permissions.filter(codename=permission.codename).exists():
            group.permissions.add(permission)
        print('grant permission {} success'.format(permission.name))
    group.save()


if __name__ == '__main__':
    grant_product_admin()
