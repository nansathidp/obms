from django.utils import timezone

from account.models import Account
from config.models import System
from deal.models import Deal
from studio.models import Studio
from title.models import Title
from vendor.models import Vendor

PERIOD_DAYS = 30


def get_title_json_display(title, datetime_start, datetime_end):
    return {
        'title_id': title.id,
        'title_name': title.name,
        'total_episode': title.total_episode,
        'title_type': title.title_type,
        'datetime_right_start': datetime_start,
        'datetime_right_end': datetime_end,
        'duration': title.duration,
    }


def get_vod_type_json_display():
    title = get_title()
    datetime_start = timezone.now() + timezone.timedelta(days=20)
    datetime_end = datetime_start + timezone.timedelta(days=PERIOD_DAYS)
    title_json = get_title_json_display(title, datetime_start, datetime_end)
    title_json.update(
        {
            'mg': 200,
            'rev_share': 200,
            'deem_price': 100,
        }
    )
    return {
        'label': 'SVOD',
        'items': [title_json],
    }


def get_vod_linear_display():
    title = Title.objects.all().order_by('?').last()
    datetime_start = timezone.now() + timezone.timedelta(days=7)
    datetime_end = datetime_start + timezone.timedelta(days=PERIOD_DAYS)
    return get_title_json_display(title, datetime_start, datetime_end)


def get_title():
    return Title.objects.all().order_by('?').first()


def get_account():
    return Account.objects.all().order_by('?').last()


def get_deal_json():
    vendor = Vendor.objects.all().order_by('?').last()
    studio = Studio.objects.all().order_by('?').last()
    config = System.objects.get(group_name='DEAL_STATUS')
    datetime_right_start = timezone.now() + timezone.timedelta(days=12)
    datetime_right_end = datetime_right_start + timezone.timedelta(days=30)
    return {
        'ref_no': 'STS00020121',
        'remark': 'bY sony.',
        'status': config.value,
        'studio_id': studio.id,
        'vendor_id': vendor.id,
        'other_list': [get_vod_type_json_display()],
        'linear_list': [get_title_json_display(get_title(), datetime_right_start, datetime_right_end)]
    }


class Request:
    user = None

    def __init__(self):
        self.user = get_account()


class View:
    action = 'create',

    def get_object(self):
        if self.action == 'create':
            return None
        else:
            return Deal.objects.all().order_by('?').last()


def run():
    from deal.serializers_from import DealFromSerializer, LinerItemSerializer
    data = get_deal_json()
    request = Request()
    view = View()
    context = {
        'request': request,
        'view': view,
    }
    # print(data)
    serializer_liner = LinerItemSerializer(data=data['linear_list'][0])
    serializer_liner.is_valid(True)
    print(serializer_liner.data)
    serializer = DealFromSerializer(data=data, context=context)
    serializer.is_valid(True)
    serializer.save()
