import os
import sys

import django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bms.settings")
django.setup()

from django.conf import settings
from django.contrib.auth.models import Permission

CODENAME_LIST = ['view', 'view_detail', 'add', 'change', 'delete']


def generate():
    for codename in CODENAME_LIST:
        for master in settings.MASTER_APPS:
            content_type = settings.CONTENT_TYPE('{}.{}'.format(master, master))
            if content_type:
                perm, is_created = Permission.objects.get_or_create(
                    content_type=content_type,
                    codename='{}_{}'.format(codename, master),
                )
                # perm.delete()
                if is_created:
                    perm.name = 'Can {} {}'.format(codename, master)
                    perm.save(update_fields=['name'])
                    print('init {} {} success'.format(master, codename))


if __name__ == '__main__':
    generate()
