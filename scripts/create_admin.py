import os
import sys

import django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bms.settings")
django.setup()

from account.models import Account

USERS = [
    {
        'email': 'nansathidp@gmail.com',
        'employee_id': '010E0915',
        'password': 'password123'
    },
    {
        'email': 'wasagorn.sri@gmail.com',
        'employee_id': '010E6673',
        'password': 'password123'
    },
]

if __name__ == '__main__':
    try:
        for USER in USERS:
            user = Account.objects.filter(email=USER['email'])
            print(user.exists())
            if not user.exists():
                Account.objects.create_superuser(**USER)
            print(USER.get('email'), ' created.')
    except Exception as err:
        print(err)
        sys.exit(1)
