from django.conf import settings
from django.db import models

from utils.fields import ObmModel

IS_MERGE = 'Y'
NO_MERGE = 'N'

MERGE_CHOICES = (
    (IS_MERGE, True),
    (NO_MERGE, False)
)


class Publish(ObmModel):
    id = models.CharField(max_length=15, primary_key=True, db_column="PUBLISH_ZONE_ID")
    desc = models.CharField(max_length=50, null=True, default=None, db_column="DESCRIPTION")
    is_merge = models.CharField(max_length=1, choices=MERGE_CHOICES, default=IS_MERGE, db_column="MERGE_FLAG")

    primary_key = "id"
    prefix = "PZ"

    class Meta:
        ordering = ('desc',)
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_PUBLISH_ZONE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '%s (%s)' % (self.id, self.desc)

    @staticmethod
    def pull(pk):
        try:
            return Publish.objects.get(pk=pk)
        except Publish.DoesNotExist:
            return None


class Audio(models.Model):
    publish = models.ForeignKey(
        Publish,
        primary_key=settings.IS_ORA,
        db_column="PUBLISH_ZONE_ID",
        on_delete=models.CASCADE,
    )
    language = models.ForeignKey(
        'language.Language',
        related_name="publish_audio_language_set",
        db_column="LANGUAGE_CODE",
        on_delete=models.CASCADE,
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATEE_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_PUBLISH_ZONE_AUDIO')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)


class SubTitle(models.Model):
    publish = models.ForeignKey(
        Publish,
        primary_key=settings.IS_ORA,
        db_column="PUBLISH_ZONE_ID",
        on_delete=models.CASCADE,
    )
    language = models.ForeignKey(
        'language.Language',
        related_name="publish_subtitle_language_set",
        db_column="LANGUAGE_CODE",
        on_delete=models.CASCADE,
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_PUBLISH_ZONE_SUBTITLE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)


class Video(models.Model):
    publish = models.ForeignKey(
        Publish,
        primary_key=settings.IS_ORA,
        db_column="PUBLISH_ZONE_ID",
        on_delete=models.CASCADE,
    )

    codec = models.CharField(max_length=50, db_column="CODEC")
    resolution = models.CharField(max_length=50, db_column="RESOLUTION")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_PUBLISH_ZONE_VIDEO')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)


class Country(models.Model):
    publish = models.ForeignKey(
        Publish,
        primary_key=settings.IS_ORA,
        db_column="PUBLISH_ZONE_ID",
        on_delete=models.CASCADE,
    )
    country = models.ForeignKey(
        'country.Country',
        related_name="publish_country_set",
        db_column="COUNTRY_CODE",
        null=True,
        default=None,
        on_delete=models.SET_NULL,
    )
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_PUBLISH_ZONE_COUNTRY')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete')


class Directory(models.Model):
    publish = models.ForeignKey(
        Publish,
        primary_key=settings.IS_ORA,
        db_column="PUBLISH_ZONE_ID",
        on_delete=models.CASCADE,
    )
    sequence_no = models.IntegerField(db_column="SEQ_NO")
    remote_ip = models.GenericIPAddressField(protocol='both', unpack_ipv4=True, db_column="REMOTE_ADDRESS", max_length=20)
    username = models.CharField(max_length=20, null=True, blank=True, default=None, db_column="USER_NAME")
    password = models.CharField(max_length=100, null=True, blank=True, default=None, db_column='PWD')
    directory = models.CharField(max_length=20, null=True, blank=True, default=None, db_column="DIRECTORY")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_PUBLISH_ZONE_DIRECTORIES')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete')
