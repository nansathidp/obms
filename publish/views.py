from rest_framework import viewsets

from .models import Publish
from .serializers import ListPublishSerializer


class PublishView(viewsets.ReadOnlyModelViewSet):
    queryset = Publish.objects.all()
    serializer_class = ListPublishSerializer
