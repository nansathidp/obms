import json

from rest_framework import mixins, status, viewsets
from rest_framework.response import Response

from alert.models import Alert
from utils.rest_framework.permission import AddPermission
from .models import Publish
from .serializers_schedule import ScheduleSerializer
from .task import generate_episode_schedule


class ScheduleView(viewsets.GenericViewSet, mixins.CreateModelMixin):
    queryset = Publish.objects.none()
    serializer_class = ScheduleSerializer
    pagination_class = None

    permission_classes_action = {
        'create': [AddPermission],
    }

    app = 'episode'
    model = 'publishepisode'

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        alert = Alert.objects.create(
            percent=5,
            is_response=True,
            response=json.dumps(
                {'message': "Receive data ", 'results': [], 'error': ""}
            )
        )

        generate_episode_schedule.delay(request.data, alert.id, request.user.id)
        return Response({"id": alert.id}, status=status.HTTP_201_CREATED)