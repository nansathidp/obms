from django.contrib import admin

from .models import Country, Publish


@admin.register(Publish)
class PublishModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'is_merge',)
    list_filter = ('is_merge',)


@admin.register(Country)
class CountryModelAdmin(admin.ModelAdmin):
    list_display = ('publish_id', 'country_id',)
