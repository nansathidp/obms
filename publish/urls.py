from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import PublishView
from .views_schedule import ScheduleView
from .views_tools import ToolsView

router = DefaultRouter()

router.register(r'schedule', ScheduleView)
router.register(r'tools', ToolsView)
router.register(r'', PublishView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
