from datetime import timedelta,datetime

from rest_framework import exceptions, serializers

from deal.models import Territory, Video
from episode.models import Episode
from title.models import Title
from .models import Country, Publish

WEEK_DAYS = [0, 1, 2, 3, 4, 5, 6]


class ScheduleSerializer(serializers.Serializer):
    title_id = serializers.CharField(required=True)
    start_pac_no = serializers.IntegerField(min_value=1)
    end_pac_no = serializers.IntegerField(min_value=1)
    datetime_start = serializers.DateTimeField()
    time_expire = serializers.TimeField()
    days_expire = serializers.IntegerField(min_value=0, default=0, required=False)
    datetime_expire = serializers.DateTimeField(allow_null=True, default=None, required=False)
    select_of_weekdays = serializers.ListField(
        child=serializers.IntegerField(min_value=0, max_value=6),
        allow_empty=True,
        allow_null=True,
        default=WEEK_DAYS,
    )
    shift_days = serializers.IntegerField(default=0, min_value=0)
    publish_zone_ids = serializers.ListField(
        child=serializers.CharField(max_length=15)
    )

    def validate(self, attrs):

        title_id = attrs['title_id']
        start_pac_no = attrs['start_pac_no']
        end_pac_no = attrs['end_pac_no']
        publish_zone_ids = attrs['publish_zone_ids']
        time_expire = attrs['time_expire']
        days_expire = attrs['days_expire']
        datetime_expire = attrs['datetime_expire']
        if days_expire == 0 and datetime_expire == None:
            raise exceptions.ValidationError(
                detail={
                    'days_expire': ['require days_expire'],
                    'datetime_expire': ['require datetime_expire']
                }
            )

        country_publish_list = Country.objects.filter(publish_id__in=publish_zone_ids)
        if not country_publish_list.exists():
            raise exceptions.ValidationError({
                'publish_zone_ids': ['Publish is country empty']
            })

        if not Episode.objects.filter(
                title_id=title_id,
                pac_no__range=(start_pac_no, end_pac_no)
        ).exists():
            raise exceptions.ValidationError(
                detail={
                    'fields': {
                        'title_id': 'Title {} pac no period is not found'.format(title_id),
                        'start_pac_no': ['Start pac {} no not found.'.format(start_pac_no)],
                        'end_pac_no': ['End pac no {} not found.'.format(end_pac_no)]
                    }
                }
            )

        datetime_start = attrs['datetime_start']
        datetime_start = datetime_start + timedelta(
            hours=datetime_start.hour,
            minutes=datetime_start.minute,
            seconds=datetime_start.second,
            microseconds=datetime_start.microsecond,
        )
        datetime_expire = datetime_expire if datetime_expire else datetime_start + timedelta(days=days_expire)
        datetime_expire = datetime_expire + timedelta(
            hours=time_expire.hour,
            minutes=time_expire.minute,
            seconds=time_expire.second,
            microseconds=time_expire.microsecond,
        )

        video_title_right_start_periods = Video.objects.filter(
            title_id=title_id,
            datetime_right_start__lte=datetime_start,
            datetime_right_end__gte=datetime_start
        )
        video_title_right_expire_periods = Video.objects.filter(
            title_id=title_id,
            datetime_right_start__lte=datetime_expire,
            datetime_right_end__gte=datetime_expire
        )

        if not (video_title_right_start_periods.exists() and video_title_right_expire_periods.exists()):
            raise exceptions.ValidationError(
                detail={
                    'fields': ['datetime_start'],
                    'message': 'Title periods from  {} to {} is not fiend'.format(datetime_start.strftime("%c"),
                                                                                  datetime_expire.strftime("%c"))
                }
            )
        country_id_list = country_publish_list.values_list('country_id', flat=True)
        country_id_list = list(country_id_list)
        title_territories = Territory.objects.filter(deal__video__title_id=title_id, country_id__in=country_id_list)

        if not title_territories.exists():
            raise exceptions.ValidationError(
                detail={
                    'title_id': ['{} title territory is not found.'.format(title_id)]
                }
            )
        return attrs

    def validate_title_id(self, title_id):
        if Title.objects.filter(id=title_id).exists():
            return title_id
        raise exceptions.ValidationError('{} title not found'.format(title_id))

    def validate_publish_zone_ids(self, publish_zone_ids):
        for publish_zone_id in publish_zone_ids:
            if Publish.objects.filter(id=publish_zone_id).exists():
                continue
            raise exceptions.ValidationError('{} publish_zone_id is not found.'.format(publish_zone_id))
        return publish_zone_ids
