from rest_framework import serializers

from publish.models import Publish


class ListPublishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publish
        fields = (
            'id',
            'is_merge',
            'desc',
        )


class PublishSerializer(serializers.ModelSerializer):
    publish_id = serializers.CharField(source='id')

    class Meta:
        model = Publish
        fields = (
            'id',
            'publish_id',
            'desc',
        )
