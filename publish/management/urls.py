from django.urls import include, path
from rest_framework import routers

from utils.rest_framework.routers import ListDetailRouter
from .views import PublishView
from .views_audio import AudioView
from .views_country import CountryView
from .views_directory import DirectoryView
from .views_subtitle import SubTitleView
from .views_video import VideoView

router = routers.DefaultRouter()
router.register(r'', PublishView)

router_audio = ListDetailRouter()
router_audio.register(r'', AudioView)

router_subtitle = ListDetailRouter()
router_subtitle.register(r'', SubTitleView)

router_video = ListDetailRouter()
router_video.register(r'', VideoView)

router_country = ListDetailRouter()
router_country.register(r'', CountryView)

router_directory = ListDetailRouter()
router_directory.register(r'', DirectoryView)

app_name = 'publish'
urlpatterns = [ 
	path('<str:publish_id>/audio/', include(router_audio.urls)),
	path('<str:publish_id>/subtitle/', include(router_subtitle.urls)),
	path('<str:publish_id>/video/', include(router_video.urls)),
	path('<str:publish_id>/country/', include(router_country.urls)),
	path('<str:publish_id>/directory/', include(router_directory.urls)),
    path('', include(router.urls)),
]
