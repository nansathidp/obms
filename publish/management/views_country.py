from rest_framework import mixins

from .serializers import PublishCountryListSerializer
from .views_detail_generic import DetailGenericView
from ..models import Country


class CountryView(DetailGenericView, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = Country.objects.all().order_by('-country_id')
    serializer_class = PublishCountryListSerializer
    pagination_class = None
    model = 'country'
