from rest_framework import mixins

from .serializers import SubTitleListSerializer
from .views_detail_generic import DetailGenericView
from ..models import SubTitle


class SubTitleView(DetailGenericView, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = SubTitle.objects.all().order_by('-language_id')
    serializer_class = SubTitleListSerializer
    pagination_class = None
    model = 'subtitle'
