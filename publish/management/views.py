from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import exceptions, filters, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .filters import PublishFilter
from .serializers import PublishCreateSerializer, PublishListSerializer, PublishSerializer
from ..models import Publish


class PublishView(viewsets.ModelViewSet):
    queryset = Publish.objects.all()
    serializer_class = PublishSerializer
    app = 'publish'
    model = 'publish'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    action_serializers = {
        'list': PublishSerializer,
        'getbyid': PublishListSerializer,
        'create': PublishCreateSerializer,
        'update': PublishCreateSerializer,
        'partial_update': PublishCreateSerializer,
        'destroy': PublishSerializer,
    }

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    filter_class = PublishFilter

    search_fields = (
        'id',
        'desc'        
    )


    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):        
        serializer = self.get_serializer(data=request.data)        
        serializer.is_valid(raise_exception=True)
        publish = self.perform_create(serializer)
        serializer_response = PublishSerializer(publish)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):        
        partial = kwargs.pop('partial', False)
        instance = self.get_object()        
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        serializer_response = PublishSerializer(instance)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()

    def perform_destroy(self, instance):
        try:
            super().perform_destroy(instance)
        except Exception as err:
            print(err)
            raise exceptions.ValidationError()

    @action(methods=['GET'], detail=False)
    def getbyid(self, request):
        import json
        publish_id = request.GET['0']
        publish = PublishListSerializer(Publish.objects.filter(id=publish_id), many=True)
        result = json.loads(json.dumps([publish.data]))[0][0]           
        return Response(result, status=status.HTTP_201_CREATED)