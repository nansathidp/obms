from django_filters.rest_framework import FilterSet
from django_filters import filters

from ..models import Publish


class PublishFilter(FilterSet):
    publish_name = filters.CharFilter(
        method='get_publish_name',
        field_name='publish_name'
    )

    class Meta:
        model = Publish
        fields = (
            'id',
            'desc'            
        )

    def get_publish_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)
