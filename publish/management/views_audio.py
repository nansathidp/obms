from rest_framework import mixins

from .serializers import AudioListSerializer
from .views_detail_generic import DetailGenericView
from ..models import Audio


class AudioView(DetailGenericView, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = Audio.objects.all().order_by('-language_id')
    serializer_class = AudioListSerializer
    pagination_class = None
    model = 'audio'
