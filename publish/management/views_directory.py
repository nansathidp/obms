from rest_framework import mixins

from .serializers import DirectoryListSerializer
from .views_detail_generic import DetailGenericView
from ..models import Directory


class DirectoryView(DetailGenericView, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = Directory.objects.all().order_by('-sequence_no')
    serializer_class = DirectoryListSerializer
    pagination_class = None
    model = 'directory'
